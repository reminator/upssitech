import pybullet as p
import pybullet_data
import numpy as np
import matplotlib.pyplot as plt
import time

"""
####     Initialisation     ####
"""

# Position de départ du robot
startPos = [0, 0, 0]
startOrientation = p.getQuaternionFromEuler([0, 0, 0])

# Configuration de PyBullet
physicsClient = p.connect(p.GUI)
p.setAdditionalSearchPath(pybullet_data.getDataPath())
p.setGravity(0, 0, -10)
timestep = 1. / 240.
p.setTimeStep(timestep)


# Import des fichiers URDF
# Sol
planeId = p.loadURDF("plane.urdf")
# Robot
robot2RId = p.loadURDF("2R.urdf", startPos, startOrientation, useFixedBase=1)

# Récupération du nombre de liaisons
nbJoint = p.getNumJoints(robot2RId)

# Variables pour l'affichage
hasPrevPose = False
t = 0

# Récupération des informations sur l'organe terminal
orgTermInfos = p.getLinkState(robot2RId, 2)
posOrgTerm = orgTermInfos[0]
# Récupération de la position Z de l'organe terminal
coordZ = posOrgTerm[2]
idOT = p.getNumJoints(robot2RId) - 1


"""
####     Fonctions utiles     ####
"""


def ControlPosition(robotId, joint, targetPosition):
    """
    Permet de déplacer une liaison à une position désirée
    :param robotId: Identifiant du robot
    :param joint: Identifiant de la liaison
    :param targetPosition: Position à atteindre
    :return: None
    """
    mode = p.POSITION_CONTROL
    p.setJointMotorControl2(bodyUniqueId=robotId,
                            jointIndex=joint,
                            controlMode=mode,
                            targetPosition=targetPosition,
                            )


def getMGDMGI(robotId, idOT, targetPosition):
    """
    Permet de récupérer les positions des liaisons à atteindre en fonction de la position de l'organe terminal à atteindre
    :param robotId: Identifiant du robot
    :param idOT: Identifiant de l'organe terminal
    :param targetPosition: Position à atteindre
    :return: Positions des liaisons correspondantes à la position de l'organe terminal
    """
    jointpos = p.calculateInverseKinematics(robotId, idOT, targetPosition)
    return jointpos


def goToPoint(robotId, position):
    """
    Permet de déplacer le robot à une position désirée
    :param robotId: Identifiant du robot
    :param position: Position à atteindre
    :return: None
    """
    nbLiaisonMoinsUn = p.getNumJoints(robotId) - 1
    jointpos = getMGDMGI(robotId, nbLiaisonMoinsUn, position)
    for i in range(nbLiaisonMoinsUn):
        ControlPosition(robotId, i, jointpos[i])


def resetPosition(robotId, position):
    """
    Permet de change la position du robot instantanément
    :param robotId: Identifiant du robot
    :param position: Position à atteindre
    :return: None
    """
    nbLiaisonMoinsUn = p.getNumJoints(robotId) - 1
    jointpos = getMGDMGI(robotId, nbLiaisonMoinsUn, position)
    for i in range(nbLiaisonMoinsUn):
        p.resetJointState(robot2RId, i, jointpos[i])


def getDatas(robotId, q0pos, q0vel, q0acc, q1pos, q1vel, q1acc):
    """
    Permet d'ajouter à la liste des positions / vitesses / accélérations précédentes les nouvelles données
    :param robotId: Identifiant du robot
    :param q0pos: Liste des positions de la liaison 0
    :param q0vel: Liste des vitesses de la liaison 0
    :param q0acc: Liste des accélérations de la liaison 0
    :param q1pos: Liste des positions de la liaison 1
    :param q1vel: Liste des vitesses de la liaison 1
    :param q1acc: Liste des accélérations de la liaison 1
    :return: q0pos, q0vel, q0acc, q1pos, q1vel, q1acc
    """
    nbJoint = p.getNumJoints(robotId)
    for i in range(nbJoint - 1):
        state = p.getJointState(robotId, i)
        if i == 0:
            q0pos.append(state[0])
            if len(q0vel) > 1:
                q0acc.append(state[1] - q0vel[-1])
            q0vel.append(state[1])
        if i == 1:
            q1pos.append(state[0])
            if len(q1vel) > 1:
                q1acc.append(state[1] - q1vel[-1])
            q1vel.append(state[1])
    return q0pos, q0vel, q0acc, q1pos, q1vel, q1acc


def affichageDatas(q0pos, q0vel, q0acc, q1pos, q1vel, q1acc):
    """
    Permet d'afficher l'évolution des positions / vitesses / accélérations
    :param q0pos: Liste des positions de la liaison 0
    :param q0vel: Liste des vitesses de la liaison 0
    :param q0acc: Liste des accélérations de la liaison 0
    :param q1pos: Liste des positions de la liaison 1
    :param q1vel: Liste des vitesses de la liaison 1
    :param q1acc: Liste des accélérations de la liaison 1
    :return: None
    """
    plt.figure()
    plt.subplot(121)
    plt.plot(np.arange(len(q0pos)) * timestep, q0pos)
    plt.xlabel("steps")
    plt.ylabel("position")
    plt.title("Q0")

    plt.subplot(122)
    plt.plot(np.arange(len(q1pos)) * timestep, q1pos)
    plt.xlabel("steps")
    plt.title("Q1")
    plt.suptitle('Position')
    plt.show()

    plt.figure()
    plt.subplot(121)
    plt.plot(np.arange(len(q0vel)) * timestep, q0vel)
    plt.xlabel("steps")
    plt.ylabel("vitesse")
    plt.title("dQ0")

    plt.subplot(122)
    plt.plot(np.arange(len(q1vel)) * timestep, q1vel)
    plt.xlabel("steps")
    plt.title("dQ1")
    plt.suptitle('Vitesse')
    plt.show()

    plt.figure()
    plt.subplot(121)
    plt.plot(np.arange(len(q0acc)) * timestep, q0acc)
    plt.xlabel("steps")
    plt.ylabel("acceleration")
    plt.title("ddQ0")

    plt.subplot(122)
    plt.plot(np.arange(len(q1acc)) * timestep, q1acc)
    plt.xlabel("steps")
    plt.title("ddQ1")
    plt.suptitle('Accélération')
    plt.show()


def trajectoire(t):
    """
    Retourne la position à atteindre à l'instant t
    :param t: Instant t
    :return: Position à atteindre à l'instant t
    """
    return [(np.cos(t) + 1) / 2, (np.sin(t) + 1) / 2, coordZ]


"""
####     Boucle principale     ####
"""
resetPosition(robot2RId, [1, 0.1, 0])
while True:
    # Récupération de la position à atteindre à l'instant t
    pos = trajectoire(t)

    # Aller à cette position
    goToPoint(robot2RId, pos)

    # Affichage de la ligne sur l'interface graphique
    if hasPrevPose:
        p.addUserDebugLine(prevPose, pos, [1, 0, 1], 3, 15)
    prevPose = pos
    hasPrevPose = 1

    # On fait avancer la simulation d'une étape
    p.stepSimulation()
    time.sleep(1. / 240.)
    t += 0.025

# Déconnexion de PyBullet
p.disconnect()

