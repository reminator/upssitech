

class Joint:
    def __init__(self, bodyUniqueId: int, jointInfo: tuple):
        print(jointInfo)
        self.bodyUniqueId = bodyUniqueId

        self.jointIndex = jointInfo[0]
        self.jointName = jointInfo[1]

        self.jointType = jointInfo[2]
        self.qIndex = jointInfo[3]
        self.uIndex = jointInfo[4]
        self.flags = jointInfo[5]
        self.jointDamping = jointInfo[6]
        self.jointFriction = jointInfo[7]
        self.jointLowerLimit = jointInfo[8]
        self.jointUpperLimit = jointInfo[9]
        self.jointMaxForce = jointInfo[10]
        self.jointMaxVelocity = jointInfo[11]
        self.linkName = jointInfo[12]
        self.jointAxis = jointInfo[13]
        self.parentFramePos = jointInfo[14]
        self.parentFrameOrn = jointInfo[15]
        self.parentIndex = jointInfo[16]

    def __repr__(self):
        return self.jointName.decode("utf-8")

    def getIndex(self):
        return self.jointIndex

    def getJointName(self):
        return self.jointName