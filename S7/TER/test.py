import pybullet as p
import pybullet_data

# Initialisation
physicsClient = p.connect(p.GUI)
planeId = p.loadURDF("2R.urdf")

# Commandes
# [...]




p.setAdditionalSearchPath(pybullet_data.getDataPath())

robot2RId = p.loadURDF("2R.urdf", [0, 0, 0], p.getQuaternionFromEuler([0, 0, 0]), useFixedBase=1)

# Commandes
# [...]

# Arrêt du simulateur
# p.disconnect()

