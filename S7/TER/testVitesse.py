import pybullet as p
import time
import pybullet_data
import math
import numpy as np
import matplotlib.pyplot as plt


Vmax = 5
Amax = 4.9


def getTrajectoryTrap(A, B):
    print("B-A =", B-A)
    t0 = 0
    t1 = Vmax / Amax
    t2 = 1 / 2 * t1 - 1 / 2 * Vmax / Amax + (B - A) / Vmax
    tf = Vmax / Amax + t2
    return [t0, t1, t2, tf]


def vitesse(t, ts):
    t0 = ts[0]
    t1 = ts[1]
    t2 = ts[2]
    tf = ts[3]

    if t < t1:
        return t * Amax
    if t < t2:
        return Vmax
    if t > tf:
        return 0
    return - (t - t2) * Amax

def ControlPosition(robotId, joint, targetPosition):
    mode = p.POSITION_CONTROL
    p.setJointMotorControl2(bodyUniqueId=robotId,
                            jointIndex=joint,
                            controlMode=mode,
                            targetPosition=targetPosition, )


def ControlVitesse(robotId, joint, targetVelocity):
    mode = p.VELOCITY_CONTROL
    p.setJointMotorControl2(bodyUniqueId=robotId,
                            jointIndex=joint,
                            controlMode=mode,
                            targetVelocity=targetVelocity, )


def getMGDMGI(robotId, nbLiaisonMoinsUn, targetPosition):
    jointpos = p.calculateInverseKinematics(robot2RId, nbLiaisonMoinsUn, targetPosition)
    return jointpos


def goToPoint(robotId, position):
    nbLiaisonMoinsUn = p.getNumJoints(robotId) - 1
    jointpos = getMGDMGI(robotId, nbLiaisonMoinsUn, position)
    for i in range(nbLiaisonMoinsUn):
        ControlPosition(robot2RId, i, jointpos[i])


def resetPosition(robotId, position):
    nbLiaisonMoinsUn = p.getNumJoints(robotId) - 1
    jointpos = getMGDMGI(robotId, nbLiaisonMoinsUn, position)
    for i in range(nbLiaisonMoinsUn):
        p.resetJointState(robot2RId, i, jointpos[i])


def getDatas(robotId, q0pos, q0vel, q0acc, q1pos, q1vel, q1acc):
    nbJoint = p.getNumJoints(robotId)
    for i in range(nbJoint - 1):
        state = p.getJointState(robotId, i)
        if i == 0:
            q0pos.append(state[0])
            if len(q0vel) > 1:
                q0acc.append(state[1] - q0vel[-1])
            q0vel.append(state[1])
        if i == 1:
            q1pos.append(state[0])
            if len(q1vel) > 1:
                q1acc.append(state[1] - q1vel[-1])
            q1vel.append(state[1])
    return q0pos, q0vel, q0acc, q1pos, q1vel, q1acc


def affichageDatas(q0pos, q0vel, q0acc, q1pos, q1vel, q1acc):
    plt.figure()
    plt.subplot(121)
    plt.plot(range(len(q0pos)), q0pos)
    plt.xlabel("steps")
    plt.ylabel("position")
    plt.title("Q0")

    plt.subplot(122)
    plt.plot(range(len(q1pos)), q1pos)
    plt.xlabel("steps")
    plt.title("Q1")
    plt.suptitle('Position')
    plt.show()

    plt.figure()
    plt.subplot(121)
    plt.plot(range(len(q0vel)), q0vel)
    plt.xlabel("steps")
    plt.ylabel("vitesse")
    plt.title("dQ0")

    plt.subplot(122)
    plt.plot(range(len(q1vel)), q1vel)
    plt.xlabel("steps")
    plt.title("dQ1")
    plt.suptitle('Vitesse')
    plt.show()

    plt.figure()
    plt.subplot(121)
    plt.plot(range(len(q0acc)), q0acc)
    plt.xlabel("steps")
    plt.ylabel("acceleration")
    plt.title("ddQ0")

    plt.subplot(122)
    plt.plot(range(len(q1acc)), q1acc)
    plt.xlabel("steps")
    plt.title("ddQ1")
    plt.suptitle('Accélération')
    plt.show()


physicsClient = p.connect(p.GUI)  # or p.DIRECT for non-graphical version
p.setAdditionalSearchPath(pybullet_data.getDataPath())  # optionally
p.setGravity(0, 0, -10)
planeId = p.loadURDF("plane.urdf")
startPosRef = [3, 0, 0]
startPos = [0, 0, 0]
startOrientation = p.getQuaternionFromEuler([0, 0, 0])

robot2RId = p.loadURDF("2R.urdf", startPos, startOrientation, useFixedBase=1)

# ControlPosition(robot2RIdRef, 0, 0)
# ControlPosition(robot2RIdRef, 1, 0)

# targetPosJoint1 = 0
# targetPosJoint2 = 0

# ControlPosition(robot2RId, 0, targetPosJoint1)
# ControlPosition(robot2RId, 1, targetPosJoint2)

# ControlVitesse(robot2RId, 0, 25)
# ControlVitesse(robot2RId, 1, 0)

# Du point A au point B avec trace

state = p.getLinkState(robot2RId, 1)
coord = state[0]

pointA = [1, -0.1, coord[2]]
pointB = [0, 1, coord[2]]
erreur = 0.01

resetPosition(robot2RId, pointA)

nbJoint = p.getNumJoints(robot2RId)

q0pos = list()
q1pos = list()
q0vel = list()
q1vel = list()
q0acc = list()
q1acc = list()

hasPrevPose = False

#goToPoint(robot2RId, pointB)

orgTerm = p.getLinkState(robot2RId, 2)
posOrgTerm = orgTerm[0]
t = 0

nbLiaisonMoinsUn = p.getNumJoints(robot2RId) - 1
jointposA = getMGDMGI(robot2RId, nbLiaisonMoinsUn, pointA)
jointposB = getMGDMGI(robot2RId, nbLiaisonMoinsUn, pointB)

ts0 = getTrajectoryTrap(jointposA[0], jointposB[0])
ts1 = getTrajectoryTrap(jointposA[1], jointposB[1])

timestep = 1. / 240.

xs = []
dq0 = []
dq1 = []


ControlVitesse(robot2RId, 0, 5)
ControlVitesse(robot2RId, 1, 5)

#while np.linalg.norm(np.array(pointB) - np.array(posOrgTerm)) > erreur:
while t <0.3:
    print(t)
    print([ts0[-1], ts1[-1]])

    vit0 = vitesse(t, ts0)
    vit1 = vitesse(t, ts1)

    dq0.append(vit0)
    dq1.append(vit1)

    # Tracé
    if hasPrevPose:
        p.addUserDebugLine(prevPose, posOrgTerm, [1, 0, 1], 3, 15)
    prevPose = posOrgTerm
    hasPrevPose = 1

    # Récupération de la position de l'organe terminal
    orgTerm = p.getLinkState(robot2RId, 2)
    posOrgTerm = orgTerm[0]

    # Récupération des infos sur la config
    q0pos, q0vel, q0acc, q1pos, q1vel, q1acc = getDatas(robot2RId, q0pos, q0vel, q0acc, q1pos, q1vel, q1acc)

    # On fait avancer la simulation d'une étape
    xs.append(t)
    t += timestep
    p.stepSimulation()
    time.sleep(timestep)

# Affichage des infos
affichageDatas(q0pos, q0vel, q0acc, q1pos, q1vel, q1acc)


plt.figure()
plt.plot(xs, dq0)
plt.xlabel("steps")
plt.ylabel("vitesse")
plt.title("dQ0 - THEORIQUE")
plt.show()


plt.figure()
plt.plot(xs, dq1)
plt.xlabel("steps")
plt.ylabel("vitesse")
plt.title("dQ1 - THEORIQUE")
plt.show()

p.disconnect()

print([(i, j) for i, j in enumerate(q0vel)])