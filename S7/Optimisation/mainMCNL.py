# -*- coding: utf-8 -*-
"""

@author: taix
"""
from mpl_toolkits.mplot3d import Axes3D
from matplotlib import cm
import matplotlib.pyplot as plt
import numpy as np

import random

#################################################
def mesureB(x,b,ep):
    val = np.sqrt(np.square(x[0] - b[0]) +np.square(x[1] - b[1]))
    val = val*(1+ ep*random.random())
    return val
####################################################
def distance(x,b,ep):
    bal= b[0]
    d1=mesureB(x,bal,ep)
    bal= b[1]
    d2=mesureB(x,bal,ep)
    bal= b[2]
    d3=mesureB(x,bal,ep)
    d=[d1, d2, d3]
    return d
#################################################

def get_X0(bal, d):
    bal1 = bal[0]
    bal2 = bal[1]
    bal3 = bal[2]

    xb1 = bal1[0]
    yb1 = bal1[1]
    xb2 = bal2[0]
    yb2 = bal2[1]
    xb3 = bal3[0]
    yb3 = bal3[1]

    d1 = d[0]
    d2 = d[1]
    d3 = d[2]

    a = xb2 - xb1
    b = yb2 - yb1
    c = d2**2 - d1**2 - (xb2**2 - xb1**2) - (yb2**2 - yb1**2)

    ap = xb3 - xb1
    bp = yb3 - yb1
    cp = d3**2 - d1**2 - (xb3**2 - xb1**2) - (yb3**2 - yb1**2)

    x0 = (b * cp - bp * c) / 2 * (a * bp - ap * b)
    y0 = (a*cp -ap*c) / (2*(ap*b-a*bp))

    return (x0, y0)

def distance_euclidienne(x1, x2):
    return np.sqrt((x1[0] - x2[0])**2 + (x1[1] - x2[1])**2)


def get_A_b(X0, Xi):
    x0 = X0[0]
    y0 = X0[1]

    xi = Xi[0]
    yi = Xi[1]

    dist = distance_euclidienne(X0, Xi)

    A1 = (x0 - xi) / dist
    A2 = (y0 - yi) / dist

    b = dist - x0 * A1 - y0 * A2

    return [A1, A2], b

def moindre_carre_L(bal, d):

    X0 = get_X0(bal, d)

    A1, b1 = get_A_b(X0, bal[0])
    A2, b2 = get_A_b(X0, bal[1])
    A3, b3 = get_A_b(X0, bal[2])

    A = np.asmatrix([A1, A2, A3])
    b = np.asarray([[b1], [b2], [b3]])

    return np.linalg.inv(A.T * A) * A.T * b



#
# Position des 3 balises
#
B1= [0., 0.]
B2= [22., 0.]
B3= [11., 12.]
B4= [0, 20.]
B5=[30.,39.]

xi=[B1[0], B2[0],B3[0]]
#print("xi=", xi)
yi=[B1[1], B2[1],B3[1]]
#print("yi=", yi)
qreel=np.asarray([12, 8.])
print("Xbut :", qreel)
#
# Simulation de la mesure des 3 distances entre le robot réel et les balises
#
erreur = 0
bal = [B1, B2, B3]
d = distance(qreel, bal, erreur)

x0 = get_X0(bal, d)



print("distances = ", d[0], d[1], d[2])
print("X0", x0)

Xr = moindre_carre_L(bal, d)
print("Xr", Xr)




############################################################
#               A FAIRE
# Calcul de la position estimée Xe du robot
#
##########################################################
# Affichage des résultats
##print("X estimé  = ",Xe, "Erreur = ", er)
##abs = np.linspace(0,len(list_erreur)-1,(len(list_erreur)))
##plt.plot(abs,list_erreur,'k')
##plt.show()
