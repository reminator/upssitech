# -*- coding: utf-8 -*-
"""
Created on Wed Nov 18 14:38:33 2020

@author: taix
"""
from mpl_toolkits.mplot3d import Axes3D
from matplotlib import cm
import matplotlib.pyplot as plt
import numpy as np
import scipy
#################################################################

def affiche3courbes(numfig,nom,f,fd,fdd,t,tc):
    plt.figure(numfig)

    plt.subplot(311)
    plt.plot(t, f, "r-")
    plt.xlabel('Temps')
    plt.ylabel('Valeur de 1' )
    plt.grid(True)
    for x in tc:
        plt.axvline(x,color="g",linestyle="--")
    plt.title('Affichage des courbes fonction de ' + nom)
    plt.subplot(312)
    plt.plot(t, fd, "r-")
    plt.xlabel('Temps')
    plt.ylabel('Valeur de ')
    plt.grid(True)
    for x in tc:
        plt.axvline(x,color="g",linestyle="--")
    plt.subplot(313)
    plt.plot(t, fdd, "r-")
    plt.xlabel('Temps')
    plt.ylabel('Valeur de ')
    plt.grid(True)
    for x in tc:
        plt.axvline(x,color="g",linestyle="--")   
    
    plt.show(block=False)
    
#############################################################################
# Affichage d'une courbe 2D d'abscisse t et d'ordonnée d(t)
#INPUT:
#    numfig: numéro de la figure (entier)
#    nom: chaîne de caractèreS qui correspond à la fonction à afficher
#    t: valeurs discrètes du temps  en abscisse
#    d: valeurs discrètes de la fonction s en ordonnée 
#    coul: couleur de la courbe (exemple "r" pour rouge)
#
#    ATTENTION: il faut que les dimensions de d et t soient identiques
#############################################################################  
def affiche_courbe2D(numfig,nom,t,d,coul):
    plt.figure(numfig)
    plt.axis([-1.0,  np.max(t), 0, 1.2* np.max(d)])
    plt.plot(t, d,"-", label="ligne -",color=coul)
    plt.xlabel('Temps')
    plt.ylabel('Valeur de ' + nom)
    plt.title('Affichage de la courbe ' + nom)
    plt.show(block=False) # affiche la figure a l'ecran
################################
def bloque_affiche():
    plt.show(block=True)

#################################################    
def intertheta(th):
    while (th >= (2*np.pi)) and (th < 0) :
        if th >= (2*np.pi):
            th= th-(2*np.pi)
        elif th <= 0:
            th= th+(2*np.pi)
    return th
#################################################   
# Calcul du MGD du robot RRR
# q = vecteur de configuration en radian
# X = vecteur de situation = (x,y, theta) en radian
def mgd(qrad):
    global l
    c1= np.cos(qrad[0])
    s1=np.sin(qrad[0])
    c12= np.cos(qrad[0]+qrad[1])
    s12=np.sin(qrad[0]+qrad[1])
    theta= qrad[0]+qrad[1]+qrad[2]
    theta= intertheta(theta)
    c123=np.cos(theta)
    s123=np.sin(theta)
    x=l[0]*c1 + l[1]*c12 +l[2]*c123
    y=l[0]*s1 + l[1]*s12 +l[2]*s123
    Xd=[x,y,theta]
    return Xd
#################################################    
def jacobienne(qrad):
    global l
    c1= np.cos(qrad[0])
    s1= np.sin(qrad[0])
    c12= np.cos(qrad[0]+qrad[1])
    s12=np.sin(qrad[0]+qrad[1])
    theta= qrad[0]+qrad[1]+qrad[2]
    c123=np.cos(theta)
    s123=np.sin(theta) 
 
    J=np.array([[-(l[0]*s1 + l[1]*s12 +l[2]*s123), -(l[1]*s12 +l[2]*s123), -(l[2]*s123)], 
                [(l[0]*c1 + l[1]*c12 +l[2]*c123), (l[1]*c12 +l[2]*c123),  (l[2]*c123)], 
                 [1, 1, 1]])
    return J
################# A COMPLETER ###############################  
def direction(x, q):

    return jacobienne(q) * x * mgd(q)

################## A COMPLETER  ###############################
#def erreur(???):
#    val= ???????????????????????
#    return val
#################################################
#################################################
# Calcul du MGI avec méthode du gradient/newton en cherchant 
#   à minimiser l'erreur en X
#
l =[1, 1, 1]
####################################################
#                    ATTENTION                
# INPUT du MGI = Xbut = (x, y, theta)    
# OUTPUT du MGI = qfinal = (q1, q2, q3)     
# 
#  Ici on rentre la valeur de qbut pour générer le Xbut
#  mais en utilisation réelle on ne rentre que le Xbut.
#  Ce n'est que pour une facilité de test puisqu'on 
#  sait ainsi si notre configuration initiale qinit 
#  est loin ou près d'une des deux solutions du MGI.
###################################################
qbutdeg= np.array([ 20., 20., 40.])
qbut= (np.pi/180)*qbutdeg
Xbut= np.array(mgd(qbut))
print("Xbut=", Xbut, type(Xbut))
#
qinitdeg=np.array([58., 78.,63.])
qinit= (np.pi/180)*qinitdeg
Xinit=np.array(mgd(qinit))
print("Xinit=", Xinit, type(Xinit))
#
q= qinit
# A Completer
#er= erreur(...)

# Il est conseillé d'afficher l'évolution des vecteurs q, X et de l'erreur
listerreur = [er]
listxc= [Xinit]
listq=[q]
nbpas=50
# A Completer
eps= 0.01

pas=1.
i=0
#################       BOUCLE PRINCIPALE ########################
while (i < nbpas)&(er>eps):
#
#   A COMPLETER
#
    i=i+1

# Visualisation des résultats

print("Xfinal avec qfinal = ",X)
print("Xbut à atteindre   =", Xbut)

################### Afficher l'évolution de votre critére #############################
#affiche_courbe2D(1,"Critère d'erreur ....",...)

################### Afficher l'évolution de x,y et theta #############################
#affiche3courbes(2,"X",....)

################### Afficher l'évolution de q1, q2 et q3 #############################
#affiche3courbes(3,"Q en degré",...)