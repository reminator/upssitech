function [CentresGravite]=CalculerCentresGravite(Individus, NoClasses);
%---------------------------------------------------------------------------------
%function [CentresGravite]=CalculerCentresGravite(Individus, NoClasses);
%         CalculerCentresGravite.m   
%
% Calcul des centres de gravit� de chaque classe.
%---------------------------------------------------------------------------------
% Entree : Individus                [NbrIndividus x NbrParametres]
%          NoClasses                [NbrIndividus x 1 ]
%
% Sortie : CentresGravite           [NbrClasses   x NbrParametres]
%---------------------------------------------------------------------------------
% le 18 janv 00

[NbrIndividus NbrVariables]=size(Individus);
NbrClasses=max(NoClasses);

MoyenneClasse=zeros(NbrClasses,NbrVariables);
NbrIndivClasse=zeros(1,NbrClasses);
for i=1:NbrIndividus,
   MoyenneClasse(NoClasses(i),:)=MoyenneClasse(NoClasses(i),:) + Individus(i,:);
   NbrIndivClasse(NoClasses(i))=NbrIndivClasse(NoClasses(i))+1;
end;

for q=1:NbrClasses,
   MoyenneClasse(q,:)=MoyenneClasse(q,:)/NbrIndivClasse(q);
end;   
CentresGravite=MoyenneClasse;


