function [CT, CA, CE]=CalculerMatricesCovariance(Individus,Classes);
%---------------------------------------------------------------------------------
%  function [CT, CA, CE]=CalculerMatricesCovariance(Individus,Classes);
%          CalculerMatricesCovariance.m   /  matcov.m
%
% Calcul des matrices de covariance Totale, Intraclasses et Interclasses
%---------------------------------------------------------------------------------
% Entree : Individus [NbrIndividus x NbrParametres]
%          Classes   [NbrIndividus x 1 ]
%
% Sortie : CT = Matrice de covariance totale       [NbrParametres x NbrParametres]
%          CA = Matrice de covariance intraclasses [NbrParametres x NbrParametres]
%          CE = Matrice de covariance interclasses [NbrParametres x NbrParametres]
%----------------------------------------------------------------------------------
% le 18 janv 00

[NbrIndividus NbrVariables]=size(Individus);
NbrClasses=max(Classes);

% Moyenne g�n�rale

MoyenneGene=sum(Individus)/NbrIndividus;

% Variables centr�es 

for k=1:NbrVariables,
      TabMoyenneGene(:,k)=ones(NbrIndividus,1)*MoyenneGene(k);
end;   
IndividusCentres=Individus-TabMoyenneGene;

% Moyenne par classe

for q=1:NbrClasses,
   NoClasse=find(Classes==q);
   MoyenneClasse(q,:)=sum(Individus(NoClasse,:))/length(NoClasse);
end;

% Variables centr�es par classe

for k=1:NbrVariables,
   for q=1:NbrClasses,
      NoClasse=find(Classes==q);
      TabMoyenneClasse(NoClasse,k)=ones(size(NoClasse))*MoyenneClasse(q,k);
   end;   
end;   
IndividusCentresClasse=Individus-TabMoyenneClasse;

% Matrice de covariance intraclasse

for k=1:NbrVariables,
   for l=1:NbrVariables,
      CA(k,l)=( IndividusCentresClasse(:,k)'*IndividusCentresClasse(:,l) )/NbrIndividus; 
   end;
end;   

% Matrice de covariance interclasses

for k=1:NbrVariables,
   for l=1:NbrVariables,
      CE(k,l)=0;
      for q=1:NbrClasses,
         NoClasse=find(Classes==q);
         CE(k,l)=CE(k,l)+length(NoClasse)*( MoyenneClasse(q,k) - MoyenneGene(k) )*( MoyenneClasse(q,l) - MoyenneGene(l) );
      end;   
      CE(k,l)=CE(k,l)/NbrIndividus; 
   end;
end;   

% Matrice de covariance totale

for k=1:NbrVariables,
   for l=1:NbrVariables,
      CT(k,l)=(IndividusCentres(:,k)'*IndividusCentres(:,l))/NbrIndividus;
   end;
end;   

% V�rification

if abs(sum(sum(CA+CE-CT)))>1e-8,

   [t a e]=CalculerVariances(Individus,Classes, CalculerCentresGravite(Individus,Classes));
 
   if abs(trace(CT)-t)>1e-8,
      fprintf(1,'Attention pb de calcul sur la matrice de covariance totale CT!\n');
   end;   
   if abs(trace(CA)-a)>1e-8,
      fprintf(1,'Attention pb de calcul sur la matrice de covariance intraclasses CA!\n');
   end;   
   if abs(trace(CE)-e)>1e-8,
      fprintf(1,'Attention pb de calcul sur la matrice de covariance interclasses CE!\n');
   end;   
end;