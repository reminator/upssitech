function [VT, VA, VE]=CalculerVariances(Individus, NoClasses, CentresGravite);
%---------------------------------------------------------------------------------
% function [VT, VA, VE]=CalculerVariances(Individus, NoClasses, CentresGravite);
%           CalculerVariances.m   /  calcvar.m
%
% Calcul des variances Totale, Intraclasses et Interclasses
%---------------------------------------------------------------------------------
% Entree : Individus          [NbrIndividus x NbrParametres]
%          NoClasses          [NbrIndividus x 1 ]
%          CentresGravite     [NbrClasses   x NbrParametres]
%
% Sortie : VT = Variance totale       [1 x 1]
%          VA = Variance intraclasses [1 x 1]
%          VE = Variance interclasses [1 x 1]
%----------------------------------------------------------------------------------
% le 18 janv 00

[NbrIndividus NbrVariables]=size(Individus);
NbrClasses=max(NoClasses);

VA=0;
for q=1:NbrClasses,
   IndClasse=find(NoClasses==q);
   VA=VA+sum(sum( ( Individus(IndClasse,:) - ones(size(IndClasse))*CentresGravite(q,:) ).^2));
end;
VA=VA/NbrIndividus;

VE=0;
CGlobal=sum(Individus,1)/NbrIndividus;
for q=1:NbrClasses,
   IndClasse=find(NoClasses==q);
   VE=VE+length(IndClasse)*sum( (CentresGravite(q,:)-CGlobal ).^2);
end;   
VE=VE/NbrIndividus;

VT=0;
for k=1:NbrVariables,
   VT=VT+sum((Individus(:,k)-ones(NbrIndividus,1)*CGlobal(k)).^2);
end;
VT=VT/NbrIndividus;

% Vérification

if abs(VT-VA-VE)>1e-8,
   fprintf(1,'Attention pb de calcul pour les variances (VT<>VA+VE)!\n');
end;   