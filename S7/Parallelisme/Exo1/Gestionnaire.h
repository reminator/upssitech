#include <iostream>
#include <semaphore.h>
#define N 100

class Gestionnaire {
    sem_t * tour = new sem_t[N];
    
    void demanderTour(unsigned int i);
    void donnerTour(unsigned int i);
};