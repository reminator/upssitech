#define Info int
#define N 100

struct Case{
    Info info;
    bool type;
};

Case buffer[N];
// Voir diapo 30 pour comprendre avec le schema
// Represente l'indice de la prochaine case vide disponible
unsigned int indVide = 0;

// Represente l'indice de la première case pleine
unsigned int indPlein = 0;

void faireDepot(const Info &info, bool type) {
    buffer[indVide].info = info;
    buffer[indVide].type = type;
    indVide = (indVide + 1) % N;
}

void faireRetrait(Info &info, bool &type) {
    info = buffer[indPlein].info;
    type = buffer[indPlein].type;
    indPlein = (indPlein + 1) % N;
}

void producteur(bool type) {
    Info info;
    while(1) {
        // evaluer info
        deposer(info, type);
    }
}

void consomateur() {
    Info info;
    bool type;
    while(1) {
        retirer(info, type);
        // traiter info + type
    }
}

/**
 * @brief fonction deposer 
 * appele par un producteur
 * il a besoin de quoi ?        -> une case vide
 * il produit quoi a la fin ?   -> une case pleine
 * 
 */

/**
 * @brief fonction retirer
 * appelee par un consommateur
 * il a besoin de quoi ?        -> une case pleine
 * il produit quoi ?            -> une case vide
 * 
 */

/**
 * @brief Initialement il y a :
 * N case vides                 -> semaphore caseVide(N)
 * 0 case pleine                -> semaphore casePleine(0)
 * 
 */


/**
 * @brief Eviter la concurence
 * 
 * Pour eviter que 2 producteurs ne déposent sur la meme case   -> mutexP -> semaphore(1)
 * Pour eviter que 2 consomateurs ne retirent sur la meme case  -> mutexC -> semaphore(1)
 * 
 * On utilise 2 mutex differents pour qu'on puisse tout de meme faire une production et une consomation en meme temps.
 * 
 * @param info 
 * @param type 
 */

void deposer(const Info &info, bool type) {
    /*
    { // Fonction P de semaphore
        nbVide--;
        if (nbVide < 0) {
            // Bloquer le processus
        }
    }
    */
    P(codeVide);
    // On a maintenant une case vide

    faireDepot(info, type);

    /*
    { // Fonction V de semaphore
        nbPlein--;
        // Débloquer un consommateur
    }
    */

    // On ajoute une case pleine
    V(casePleine);
}

void retirer(Info info, bool &type) {
    // Prendre une case pleine
    P(casePleine);
    // On a maintenant une case pleine

    faireRetrait(info, type);

    // On ajoute une case vide
    V(caseVide);
}