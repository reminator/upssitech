#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/sem.h>
#include <sys/shm.h>
#include <sys/ipc.h>
#include <sys/wait.h>

union semun {
    int              val;    /* Value for SETVAL */
    struct semid_ds *buf;    /* Buffer for IPC_STAT, IPC_SET */
    unsigned short  *array;  /* Array for GETALL, SETALL */
    struct seminfo  *__buf;  /* Buffer for IPC_INFO
                                           (Linux-specific) */
};

void demanderTour(int semid, int i) {
    struct sembuf * buf = malloc(sizeof(struct sembuf));
    buf->sem_num = i;
    buf->sem_op = -1;
    buf->sem_flg = 0;

    int retour_semop = semop(semid, buf, 1);
    if (retour_semop == -1) {
        perror("Sem op demander tour");
        exit(6);
    }
}

void donnerTour(int semid, int i, int nbSem) {
	unsigned int suivant = (i+1) % nbSem;

    struct sembuf * buf = malloc(sizeof(struct sembuf));
    buf->sem_num = suivant;
    buf->sem_op = 1;
    buf->sem_flg = 0;

    int retour_semop = semop(semid, buf, 1);
    if (retour_semop == -1) {
        perror("Sem op donner tour");
        exit(7);
    }
}

int main(int argc, char *argv[]) {

    // Vérification et transformation des paramètres
    if (argc != 5) {
        printf("Usage : ./exo2 <rang> <nbProcessus> <nbTours> <nbLignes>");
        exit(1);
    }

    int rang = atoi(argv[1]);
    int nbSem = atoi(argv[2]);
    int nbTours = atoi(argv[3]);
    int nbLignes = atoi(argv[4]);


    // Création du fichier de mémoire partagé
    // La commande touch est ignorée si le fichier existe déjà
    char * fichier = "shar";
    char commande[20];

    if (sprintf(commande, "touch %s", fichier) == -1) {
        perror("sprintf");
        exit(2);
    }
    system(commande);


    // Récupération de la clé
    key_t key;
    if ((key = ftok(fichier, 1)) == -1) {
        perror("ftok");
        exit(3);
    }


    // Création des sémaphores
    // Observation de l'existance des sémaphores
        printf("Test2\n");
    int semid;
    if ((semid = semget(key, nbSem, IPC_CREAT | IPC_EXCL | 0666)) == -1) {
        printf("Test3\n");
        // Si existe
        if ((semid = semget(key, 0, 0)) == -1) {
            perror("semget");
            exit(4);
        }
    } else {
        printf("Test\n");

        union semun arg;
        arg.array = malloc(sizeof(unsigned short[nbSem]));
        arg.array[0] = 1;
        for(unsigned int i=1; i<nbSem; i++){
            arg.array[i] = 0;
        }
        if (semctl(semid, 0, SETALL, arg) == -1) {
            perror("semctl setall");
            exit(5);
        }
    }

    // Affichage
    for (int tour = 1; tour <= nbTours; tour++) {
        demanderTour(semid, rang);
        
        for (int ligne = 1; ligne <= nbLignes; ligne++) {
            printf("Processus %d (%d) : Ligne %d/%d du message %d/%d\n", rang, getpid(), ligne, nbLignes, tour, nbTours);
        }
        if (tour != nbTours) {
            printf("\n");
            donnerTour(semid, rang, nbSem);
        }
    }
    printf("Processus %d (%d) : Terminé\n\n", rang, getpid());
    donnerTour(semid, rang, nbSem);

    // Détruire le sémaphore si dernier à écrire
    if (rang == nbSem - 1) {
        if (semctl(semid, 0, IPC_RMID, NULL) == -1) {
            perror("Détruire sem");
            exit(8);
        }
    }
}