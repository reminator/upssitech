#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/sem.h>
#include <sys/shm.h>
#include <sys/ipc.h>
#include <sys/wait.h>

union semun {
    int              val;    /* Value for SETVAL */
    struct semid_ds *buf;    /* Buffer for IPC_STAT, IPC_SET */
    unsigned short  *array;  /* Array for GETALL, SETALL */
    struct seminfo  *__buf;  /* Buffer for IPC_INFO
                                           (Linux-specific) */
};

/*   
struct sembuf {
    unsigned short sem_num; 
    short sem_op;
    short sem_flg; 
};
*/


void initTour(int semid, int nbSem) {
    unsigned short a[nbSem];
    a[0] = 1;
    for(unsigned int i=1; i<nbSem; i++){
        a[i] = 0;
	}

    union semun arg;
    arg.array = a;

    semctl(semid, 0, SETALL, arg);
}

void demanderTour(int semid, int i) {
    struct sembuf buf;
    buf.sem_num = i;
    buf.sem_op = -1;
    buf.sem_flg = 0;

    int retour_semop = semop(semid, &buf, 1);
    if (retour_semop == -1) {
        perror("Sem op demander tour");
        exit(4);
    }
}

void donnerTour(int semid, int i, int nbSem) {
	unsigned int suivant = (i+1) % nbSem;

    struct sembuf buf;
    buf.sem_num = suivant;
    buf.sem_op = 1;
    buf.sem_flg = 0;

    int retour_semop = semop(semid, &buf, 1);
    if (retour_semop == -1) {
        perror("Sem op donner tour");
        exit(5);
    }
}

void codeFils(int numeroFils, int semid, int nbTours, int nbLignes, int nbSem) {
    for (int tour = 1; tour <= nbTours; tour++) {
        demanderTour(semid, numeroFils);
        
        for (int ligne = 1; ligne <= nbLignes; ligne++) {
            printf("Processus %d (%d) : Ligne %d/%d du message %d/%d\n", numeroFils, getpid(), ligne, nbLignes, tour, nbTours);
        }
        if (tour != nbTours) {
            printf("\n");
            donnerTour(semid, numeroFils, nbSem);
        }
    }
    printf("Processus %d (%d) : Terminé\n\n", numeroFils, getpid());
    donnerTour(semid, numeroFils, nbSem);
}

int main(int argc, char *argv[]) {

    int nbSem = 2;
    int nbTours = atoi(argv[1]);
    int nbLignes = atoi(argv[2]);

    int semid = semget(IPC_PRIVATE, nbSem, IPC_CREAT | 0666);
    if (semid == -1) {
        perror("semget");
        exit(3);
    }

    initTour(semid, nbSem);
    
    pid_t pid1 = fork();
    if (pid1 == -1) {
        perror("Fork 1");
        exit(1);
    }

    
    // Code du fils 1
    if (pid1 == 0) {
        codeFils(0, semid, nbTours, nbLignes, nbSem);

    }

    pid_t pid2 = fork();
    if (pid2 == -1) {
        perror("Fork 2");
        exit(2);
    }

    // Code du fils 2
    if (pid2 == 0) {
        codeFils(1, semid, nbTours, nbLignes, nbSem);
    }

    // Code du père
    for (int i = 0; i < nbSem; i++) {
        wait(NULL);
    }
    int retour = semctl(semid, 0, IPC_RMID, NULL);
    if (retour == -1) {
        perror("Détruire sem");
        exit(6);
    }
    printf("Processus père (%d) : Terminé\n", getpid());
    exit(0);
}