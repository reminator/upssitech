#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/sem.h>
#include <sys/shm.h>
#include <sys/ipc.h>
#include <sys/wait.h>

union semun {
    int              val;    /* Value for SETVAL */
    struct semid_ds *buf;    /* Buffer for IPC_STAT, IPC_SET */
    unsigned short  *array;  /* Array for GETALL, SETALL */
    struct seminfo  *__buf;  /* Buffer for IPC_INFO
                                           (Linux-specific) */
};

int main(int argc, char *argv[]) {


    // Vérification et transformation des paramètres
    if (argc != 3) {
        printf("Usage : %s <Nb processus> <taille tableau>", argv[0]);
        exit(1);
    }

    int nbProcessus = atoi(argv[1]);
    int taille = atoi(argv[2]);

    int shmid = shmget(IPC_PRIVATE, sizeof(int[taille]), IPC_CREAT | 0666);
    if (shmid == -1) {
        perror("shmget");
        exit(2);
    }
    
    int * ptr_tab;
    if ((ptr_tab = (int *) shmat(shmid, NULL, 0)) == (int *) -1) {
        perror("shmmat");
        exit(4);
    }

    for (int i = 0; i < taille; i++) {
        ptr_tab[i] = 0;
    }

    for (int numProcessus = 0; numProcessus < nbProcessus; numProcessus++) {
        pid_t pid = fork();
        if (pid == -1) {
            char str[20];
            sprintf(str, "Fork %d", numProcessus);
            perror(str);
            exit(4);
        }
        if (pid == 0) {
            for (int i = 0; i < taille; i++) {
                ptr_tab[i] += 1;
            }
            printf("Processus %d (%d) : ", numProcessus, getpid());
            for (int i = 0; i < taille; i++) {
                if (i + 1 == taille) {
                    printf("%d\n", ptr_tab[i]);
                } else {
                    printf("%d ", ptr_tab[i]);
                }
            }

            int retour = shmdt(ptr_tab);
            if (retour == -1) {
                perror("shmdt");
                exit(6);
            }

            exit(0);
        }

    }

    // Code du père
    for (int i = 0; i < nbProcessus; i++) {
        wait(NULL);
    }

            printf("Processus père (%d) : ", getpid());
            for (int i = 0; i < taille; i++) {
                if (i + 1 == taille) {
                    printf("%d\n", ptr_tab[i]);
                } else {
                    printf("%d ", ptr_tab[i]);
                }
            }

    int retour = shmdt(ptr_tab);
    if (retour == -1) {
        perror("shmdt");
        exit(6);
    }

    retour = shmctl(shmid, IPC_RMID, NULL);
    if (retour == -1) {
        perror("shmctl RMID");
        exit(7);
    }
    exit(0);
}