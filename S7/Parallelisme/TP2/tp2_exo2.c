#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/sem.h>
#include <sys/shm.h>
#include <sys/ipc.h>
#include <sys/wait.h>

union semun {
    int              val;    /* Value for SETVAL */
    struct semid_ds *buf;    /* Buffer for IPC_STAT, IPC_SET */
    unsigned short  *array;  /* Array for GETALL, SETALL */
    struct seminfo  *__buf;  /* Buffer for IPC_INFO
                                           (Linux-specific) */
};

void P(int semid) {
    struct sembuf * buf = malloc(sizeof(struct sembuf));
    buf->sem_num = 0;
    buf->sem_op = -1;
    buf->sem_flg = 0;

    int retour_semop = semop(semid, buf, 1);
    if (retour_semop == -1) {
        perror("Sem op P");
        exit(10);
    }
}

void V(int semid) {
    struct sembuf * buf = malloc(sizeof(struct sembuf));
    buf->sem_num = 0;
    buf->sem_op = 1;
    buf->sem_flg = 0;

    int retour_semop = semop(semid, buf, 1);
    if (retour_semop == -1) {
        perror("Sem op V");
        exit(11);
    }
}

int main(int argc, char *argv[]) {


    // Vérification et transformation des paramètres
    if (argc != 3) {
        printf("Usage : %s <Nb processus> <taille tableau>", argv[0]);
        exit(1);
    }

    int nbProcessus = atoi(argv[1]);
    int taille = atoi(argv[2]);


    int semid = semget(IPC_PRIVATE, 1, IPC_CREAT | 0666);
    if (semid == -1) {
        perror("semget");
        exit(8);
    }

    union semun arg;
    arg.val = 1;
    if (semctl(semid, 0, SETVAL, arg) == -1) {
        perror("semctl");
        exit(9);
    }


    int shmid = shmget(IPC_PRIVATE, sizeof(int[taille]), IPC_CREAT | 0666);
    if (shmid == -1) {
        perror("shmget");
        exit(2);
    }
    
    int * ptr_tab;
    if ((ptr_tab = (int *) shmat(shmid, NULL, 0)) == (int *) -1) {
        perror("shmmat");
        exit(4);
    }

    for (int i = 0; i < taille; i++) {
        ptr_tab[i] = 0;
    }

    for (int numProcessus = 0; numProcessus < nbProcessus; numProcessus++) {
        pid_t pid = fork();
        if (pid == -1) {
            char str[20];
            sprintf(str, "Fork %d", numProcessus);
            perror(str);
            exit(4);
        }
        if (pid == 0) {

            P(semid);
            for (int i = 0; i < taille; i++) {
                usleep(1000);
                ptr_tab[i] += 1;
            }
            V(semid);
            
            printf("Processus %d (%d) : ", numProcessus, getpid());
            for (int i = 0; i < taille; i++) {
                if (i + 1 == taille) {
                    printf("%d\n", ptr_tab[i]);
                } else {
                    printf("%d ", ptr_tab[i]);
                }
            }

            int retour = shmdt(ptr_tab);
            if (retour == -1) {
                perror("shmdt");
                exit(6);
            }

            exit(0);
        }

    }

    // Code du père
    for (int i = 0; i < nbProcessus; i++) {
        wait(NULL);
    }

            printf("Processus père (%d) : ", getpid());
            for (int i = 0; i < taille; i++) {
                if (i + 1 == taille) {
                    printf("%d\n", ptr_tab[i]);
                } else {
                    printf("%d ", ptr_tab[i]);
                }
            }

    int retour = shmdt(ptr_tab);
    if (retour == -1) {
        perror("shmdt");
        exit(6);
    }

    retour = shmctl(shmid, IPC_RMID, NULL);
    if (retour == -1) {
        perror("shmctl RMID");
        exit(7);
    }
    exit(0);
}