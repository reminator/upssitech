#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/sem.h>
#include <sys/shm.h>
#include <sys/ipc.h>
#include <sys/wait.h>

union semun {
    int              val;    /* Value for SETVAL */
    struct semid_ds *buf;    /* Buffer for IPC_STAT, IPC_SET */
    unsigned short  *array;  /* Array for GETALL, SETALL */
    struct seminfo  *__buf;  /* Buffer for IPC_INFO
                                           (Linux-specific) */
};

#define FICHIER_MEMOIRE "shared"

void erreur(char * cause, int numero) {
    perror(cause);
    exit(numero);
}

void creer_fichier() {

    char commande[20];

    if (sprintf(commande, "touch %s", FICHIER_MEMOIRE) == -1) {
        erreur("sprintf", 2);
    }

    if (system(commande) == -1) {
        erreur("touch", 3);
    }
}

void get_key(key_t * key, int id) {
    if ((*key = ftok(FICHIER_MEMOIRE, id)) == -1) {
        erreur("ftok", 4);
    }
}

void get_ptr_shm(int ** ptr, int shmid) {
    if ((*ptr = (int *) shmat(shmid, NULL, 0)) == (int *) -1) {
        erreur("shmmat", 7);
    }
}

void affiche_tab(int * tab, int taille) {
    for (int i = 0; i < taille; i++) {
        if (i + 1 == taille) {
            printf("%d\n", tab[i]);
        } else {
            printf("%d ", tab[i]);
        }
    }
}

void shm_detach(int * id) {
    if (shmdt(id) == -1) {
        erreur("shmdt", 12);
    }
}

void shm_RMID(int id) {
    if (shmctl(id, IPC_RMID, NULL) == -1) {
        erreur("shmctl RMID", 19);
    }
}

void supprimer_fichier() {
    
    char commande[20];
    if (sprintf(commande, "rm %s -f", FICHIER_MEMOIRE) == -1) {
        erreur("sprintf", 20);
    }
    system(commande);
}

int main(int argc, char *argv[]) {


    // Vérification et transformation des paramètres
    if (argc > 4 || argc < 3) {
        printf("Usage : %s <rang> [Nb processus (si premier lancé)] <taille tableau>", argv[0]);
        exit(1);
    }

    int rang = atoi(argv[1]);
    
    int nbProcessus;
    int taille;
    if (argc == 4) {
        nbProcessus = atoi(argv[2]);
        taille = atoi(argv[3]);
    } else {
        nbProcessus = 0;
        taille = atoi(argv[2]);
    }

    // Création du fichier qui va permettre de stocker la mémoire partagée
    creer_fichier();


    // Récupération des clés
    key_t keyshm_tab;
    get_key(&keyshm_tab, 1);

    key_t keyshm_nb;
    get_key(&keyshm_nb, 2);

    key_t keysem;
    get_key(&keysem, 3);


    // Création des variables dans la mémoire partagée

    // Cette variable va permettre de stocker le tableau à modifier par les processus
    int shmid_tab = shmget(keyshm_tab, sizeof(int) * taille, IPC_CREAT | 0666);
    if (shmid_tab == -1) {
        erreur("shmget tab", 5);
    }

    // Cette variable va permettre de stocker le nombre de processus
    int shmid_nb = shmget(keyshm_nb, sizeof(int), IPC_CREAT | 0666);
    if (shmid_nb == -1) {
        erreur("shmget nb", 6);
    }

    // Récupération des pointeurs vers les emplacements de mémoire partagée
    int * ptr_tab;
    get_ptr_shm(&ptr_tab, shmid_tab);

    int * ptr_nb;
    get_ptr_shm(&ptr_nb, shmid_nb);


    // Initialisation du nombre de pointeur si l'information est renseignée
    if (argc == 4) {
        *ptr_nb = nbProcessus;
    }

    // Création du sémaphore

    // Ce sémaphore va permettre de faire attendre tous les processus à la fin du traitement
    int semid;
    // Vérification de l'existence du sémaphore
    if ((semid = semget(keysem, 1, IPC_CREAT | IPC_EXCL | 0666)) == -1) {
        // Si le sémaphore est déjà créé, récupérer son id
        if ((semid = semget(keysem, 0, 0)) == -1) {
            erreur("semget", 8);
        }
    } else {
        // Si le sémaphore n'est pas déjà créé, l'initialiser
        union semun arg;
        arg.val = 0;

        if (semctl(semid, 0, SETVAL, arg) == -1) {
            erreur("semctl INIT", 9);
        }

        // Initialisation du tableau à 0
        for (int i = 0; i < taille; i++) {
            ptr_tab[i] = 0;
        }
    }

    // Incrémentation du tableau
    for (int i = 0; i < taille; i++) {
        ptr_tab[i] += 1;
    }

    printf("Processus %d (%d) : ", rang, getpid());
    affiche_tab(ptr_tab, taille);

    struct sembuf buf;
    // Si c'est le dernier processus à incrémenter
    if (*ptr_nb == ptr_tab[0]) {
        // On ajoute des jetons au sémaphore pour que les autres processus se libèrent
        buf.sem_num = 0;
        buf.sem_op = *ptr_nb;
        buf.sem_flg = 0;

        if (semop(semid, &buf, 1) == -1) {
            erreur("Sem op donner jeton", 10);
        }
    }
    

    // On attend que tous les processus aient fini leur traitement
    buf.sem_num = 0;
    buf.sem_op = -1;
    buf.sem_flg = 0;

    if (semop(semid, &buf, 1) == -1) {
        erreur("Sem op donner jeton", 11);
    }

    printf("Fin de %d (%d) : ", rang, getpid());
    affiche_tab(ptr_tab, taille);

    shm_detach(ptr_tab);
    shm_detach(ptr_nb);

    if (rang == 0) {
        shm_RMID(shmid_tab);
        shm_RMID(shmid_nb);
    }

    supprimer_fichier();

    exit(0);
}