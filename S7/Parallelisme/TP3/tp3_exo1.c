#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/sem.h>
#include <sys/shm.h>
#include <sys/ipc.h>
#include <sys/wait.h>

#define N 10
#define NB_CONSOMMATION 3

#define MUTEX_P 0
#define MUTEX_C 1
#define CASE_PLEINE 2
#define CASE_VIDE 3




void erreur(char * cause, int numero) {
    perror(cause);
    exit(numero);
}

union semun {
    int              val;    /* Value for SETVAL */
    struct semid_ds *buf;    /* Buffer for IPC_STAT, IPC_SET */
    unsigned short  *array;  /* Array for GETALL, SETALL */
    struct seminfo  *__buf;  /* Buffer for IPC_INFO
                                           (Linux-specific) */
};

typedef struct {
    int info;
    int type;
} Message;

typedef struct {
    Message cases [N];
    unsigned int indVide;
    unsigned int indPlein;
} Buffer;

Buffer * ptr_buffer;
int semid;
int NBC;
int NBP;

void afficher_tableau() {
    printf("[");
    for (int i = 0; i < N; i++) {
        
                if (i + 1 == N) {
                    printf("%d]\n", ptr_buffer->cases[i].info);
                } else {
                    printf("%d, ", ptr_buffer->cases[i].info);
                }
    }
}

void P_Producteur() {
    struct sembuf * buf = malloc(sizeof(struct sembuf) * 2);
    buf[0].sem_num = CASE_VIDE;
    buf[0].sem_op = -1;
    buf[0].sem_flg = 0;

    buf[1].sem_num = MUTEX_P;
    buf[1].sem_op = -1;
    buf[1].sem_flg = 0;

    int retour_semop = semop(semid, buf, 2);
    if (retour_semop == -1) {
        erreur("P_P", 6);
    }
}

void V_Producteur() {
    struct sembuf * buf = malloc(sizeof(struct sembuf) * 2);
    buf[0].sem_num = MUTEX_P;
    buf[0].sem_op = 1;
    buf[0].sem_flg = 0;

    buf[1].sem_num = CASE_PLEINE;
    buf[1].sem_op = 1;
    buf[1].sem_flg = 0;

    int retour_semop = semop(semid, buf, 2);
    if (retour_semop == -1) {
        erreur("V_P", 7);
    }
}

void P_Consommateur() {
    struct sembuf * buf = malloc(sizeof(struct sembuf) * 2);
    buf[0].sem_num = CASE_PLEINE;
    buf[0].sem_op = -1;
    buf[0].sem_flg = 0;

    buf[1].sem_num = MUTEX_C;
    buf[1].sem_op = -1;
    buf[1].sem_flg = 0;

    int retour_semop = semop(semid, buf, 2);
    if (retour_semop == -1) {
        erreur("P_C", 8);
    }
}

void V_Consommateur() {
    struct sembuf * buf = malloc(sizeof(struct sembuf) * 2);
    buf[0].sem_num = MUTEX_C;
    buf[0].sem_op = 1;
    buf[0].sem_flg = 0;

    buf[1].sem_num = CASE_VIDE;
    buf[1].sem_op = 1;
    buf[1].sem_flg = 0;

    int retour_semop = semop(semid, buf, 2);
    if (retour_semop == -1) {
        erreur("V_C", 9);
    }
}

void faireDepot(Message message) {

    int indVide = ptr_buffer->indVide;

    ptr_buffer->cases[indVide].info = message.info;
    ptr_buffer->indVide = (indVide + 1) % N;
}

void faireRetrait(Message * message) {

   int indPlein = ptr_buffer->indPlein;
   
   *message = ptr_buffer->cases[indPlein];
   ptr_buffer->cases[indPlein].info = 0;
   ptr_buffer->indPlein = (indPlein + 1) % N;
}


void producteur(int numero) {
    Message message;
    message.info = numero;

    for (int i = 1; i <= NB_CONSOMMATION * NBC / NBP + 1; i++) {

        printf("%d demande à produire\n", numero);

        P_Producteur();
        // evaluer info
        faireDepot(message);

        printf("%d a produit %d fois\n", numero, i);
        afficher_tableau();
        printf("\n");

        V_Producteur();
        sleep(0.5);
    }
}

void consomateur(int numero) {
    Message message;
    for (int i = 1; i <= NB_CONSOMMATION; i++) {

        printf("%d demande à consommer\n", numero);

        P_Consommateur();

        faireRetrait(&message);
        // traiter info + type

        printf("%d a récupéré %d\n", numero, message.info);
        printf("%d a consommé %d fois\n", numero, i);
        afficher_tableau();
        printf("\n");

        V_Consommateur();
        sleep(1);
    }
}

int main(int argc, char *argv[]) {


    // Vérification et transformation des paramètres
    if (argc != 3) {
        printf("Usage : %s <Nb producteurs> <Nb Consommateurs>\n", argv[0]);
        exit(1);
    }

    NBP = atoi(argv[1]);
    NBC = atoi(argv[2]);

    
    // Création des sémaphores
    semid = semget(IPC_PRIVATE, 4, IPC_CREAT | 0666);
    if (semid == -1) {
        erreur("semget", 2);
    }
    
    
    // Initialisation des sémaphores
    unsigned short valeurs[4];
    valeurs[MUTEX_C] = 1;
    valeurs[MUTEX_P] = 1;
    valeurs[CASE_PLEINE] = 0;
    valeurs[CASE_VIDE] = N;


    union semun arg;
    arg.array = valeurs;
    if (semctl(semid, 0, SETALL, arg) == -1) {
        erreur("semctl", 3);
    }


    // Création mémoire partagé
    int shmid = shmget(IPC_PRIVATE, sizeof(Buffer), IPC_CREAT | 0666);
    if (shmid == -1) {
        erreur("shmget", 4);
    }


    // Récupération du pointeur vers la mémoire
    if ((ptr_buffer = (Buffer *) shmat(shmid, NULL, 0)) == (Buffer *) -1) {
        erreur("shmat", 5);
    }

    ptr_buffer->indPlein = 0;
    ptr_buffer->indVide = 0;
    for (int i = 0; i < N; i++) {
        ptr_buffer->cases[i].info = 0;
    }

    for (int numeroP = 1; numeroP <= NBP; numeroP++) {
        pid_t pid = fork();
        if (pid == -1) {
            char str[512];
            sprintf(str, "Fork producteur %d", numeroP);
            erreur(str, 10);
        }

        if (pid == 0) {
            producteur(numeroP);
            exit(0);
        }
    }

    for (int numeroC = 1; numeroC <= NBC; numeroC++) {
        pid_t pid = fork();
        if (pid == -1) {
            char str[512];
            sprintf(str, "Fork consommateur %d", numeroC);
            erreur(str, 11);
        }

        if (pid == 0) {
            consomateur(numeroC);
            exit(0);
        }
    }

    for (int i = 0; i < NBP + NBC; i++) {
        wait(NULL);
    }

    printf("Fin père\n");
    afficher_tableau();



    int retour = shmdt(ptr_buffer);
    if (retour == -1) {
            erreur("shmdt", 12);
    }

    retour = shmctl(shmid, IPC_RMID, NULL);
    if (retour == -1) {
            erreur("shmctl RMID", 13);
    }

        if (semctl(semid, 0, IPC_RMID, NULL) == -1) {
            erreur("semctl RMID", 14);
        }

}