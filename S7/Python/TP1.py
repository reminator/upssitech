from functools import singledispatch
import sys
import string
import math

file = open("alice_wonderland.utf8.txt", "r")
typeMots = open("alice_wonderland.utf8.conll", "r")


def afficherTexte(file):
    for line in file.readlines():
        print(line)

def getOccurences(file):
    motsOccurences = {}

    # Lecture de toutes les lignes
    for line in file.readlines():

        # Lecture mot par mot
        for mot in line.split():


            # Variable pour stocker les mots à ajouter
            m = ""

            # Caractère par caractère pour les enlever des mots
            for car in mot:

                # Si le caractère n'est pas de la ponctuation, on l'ajoute au mot
                if car not in string.punctuation:
                    m += car

                # Sinon, on ajoute la ponctuation au dictionnaire et on remplace par un espace dans le mot
                else:
                    m += " "
                    motsOccurences[car] = motsOccurences.get(car, 0) + 1

            # Ajout de chaque mots trouvés dans le dictionnaire
            for mot2 in m.split():
                motsOccurences[mot2] = motsOccurences.get(mot2, 0) + 1
    return motsOccurences

def afficherOccurences(occurences, seuil, punctuation = False):
    
    # Trier les mots par occurence
    listeOccurence = sorted(occurences.items(), key=lambda t: t[1])
    listeOccurence.reverse()
    

    # Affichage des occurences
    for (mot, occurence) in listeOccurence:
        if not punctuation and mot not in string.punctuation or punctuation:
            if occurences[mot] >= seuil:
                print(mot, "."*(30-len(mot)), occurence)
    

def getTypesMots(typeMots):
    mots = {}

    # Lecture de toutes les lignes
    for line in typeMots.readlines():
        valeurs = line.split()
        if valeurs[0] not in mots:
            mots[valeurs[0]] = {"occurence" : 1, "type" : valeurs[1], "lemme" : valeurs[2]}
        else:
            mots[valeurs[0]]["occurence"] += 1

    return mots

def afficherTypes(types, seuil, punctuation = False, *typs):
    print("Mot", " "*(30-len("Mot")), "Occurence", " "*(30-len("Occurence")), "Catégorie", " "*(30-len("Catégorie")), "Lemme")
    
    # Trier les mots par occurence
    listeOccurence = sorted(types.items(), key=lambda t: t[1]["occurence"])
    listeOccurence.reverse()

    # Affichage des occurences
    for (mot, valeur) in listeOccurence:
        if not punctuation and mot not in string.punctuation or punctuation:
            if valeur["occurence"] >= seuil:
                print(mot, "."*(30-len(mot)), valeur["occurence"], "."*(30-int(math.log10(valeur["occurence"])+1)), valeur["type"], "."*(30-len(valeur["type"])), valeur["lemme"])

occurences = getOccurences(file)
afficherOccurences(occurences, 100)

types = getTypesMots(typeMots)
afficherTypes(types, 100)
