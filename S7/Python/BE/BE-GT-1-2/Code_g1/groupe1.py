from mpl_toolkits import mplot3d
import numpy as np
import matplotlib.pyplot as plt
from math import cos, sin, sqrt, pi, atan2, acos, atan


def get_coeff(A, B, C):
    """
    Calcule les vecteurs directeurs des droites du mouvement.
    :param A: Point de départ.
    :param B: Point de transition.
    :param C: Point d'arrivée.
    :return: u (vecteur directeur de AB), DAB (distance entre A et B), u2 (vecteur directeur de BC).
    """
    AB = np.array(B) - np.array(A)
    BC = np.array(C) - np.array(B)

    DAB = np.linalg.norm(AB)
    DBC = np.linalg.norm(BC)
    u = AB / DAB
    u2 = BC / DBC

    return u, DAB, u2


def xs_gen(t, A, B, C, s):
    """
    Fonction x(s(t)) avec paramètres en fonction du temps.
    :param t: Temps auquel calculer la fonction.
    :param A: Point de départ.
    :param B: Point de transition.
    :param C: Point d'arrivée.
    :param s: Fonction s(t).
    :return: La valeur de x(s(t))
    """
    u, DAB, u2 = get_coeff(A, B, C)
    ss = s(t)
    if ss <= DAB:
        return u[0] * ss + A[0]
    else:
        return u2[0] * (ss - DAB) + B[0]


def ys_gen(t, A, B, C, s):
    """
    Fonction y(s(t)) avec paramètres en fonction du temps.
    :param t: Temps auquel calculer la fonction.
    :param A: Point de départ.
    :param B: Point de transition.
    :param C: Point d'arrivée.
    :param s: Fonction s(t).
    :return: La valeur de y(s(t))
    """
    u, DAB, u2 = get_coeff(A, B, C)
    ss = s(t)
    if ss <= DAB:
        return u[1] * ss + A[1]
    else:
        return u2[1] * (ss - DAB) + B[1]


def zs_gen(t, A, B, C, s):
    """
    Fonction z(s(t)) avec paramètres en fonction du temps.
    :param t: Temps auquel calculer la fonction.
    :param A: Point de départ.
    :param B: Point de transition.
    :param C: Point d'arrivée.
    :param s: Fonction s(t).
    :return: La valeur de z(s(t))
    """
    u, DAB, u2 = get_coeff(A, B, C)
    ss = s(t)
    if ss <= DAB:
        return u[2] * ss + A[2]
    else:
        return u2[2] * (ss - DAB) + B[2]


def dxs_gen(t, A, B, C, s):
    """
    Fonction dx(s(t)) avec paramètres en fonction du temps.
    :param t: Temps auquel calculer la fonction.
    :param A: Point de départ.
    :param B: Point de transition.
    :param C: Point d'arrivée.
    :param s: Fonction s(t).
    :return: La valeur de dx(s(t))
    """
    u, DAB, u2 = get_coeff(A, B, C)
    ss = s(t)
    if ss <= DAB:
        return u[0]
    else:
        return u2[0]


def dys_gen(t, A, B, C, s):
    """
    Fonction dy(s(t)) avec paramètres en fonction du temps.
    :param t: Temps auquel calculer la fonction.
    :param A: Point de départ.
    :param B: Point de transition.
    :param C: Point d'arrivée.
    :param s: Fonction s(t).
    :return: La valeur de dy(s(t))
    """
    u, DAB, u2 = get_coeff(A, B, C)
    ss = s(t)
    if ss <= DAB:
        return u[1]
    else:
        return u2[1]


def dzs_gen(t, A, B, C, s):
    """
    Fonction dz(s(t)) avec paramètres en fonction du temps.
    :param t: Temps auquel calculer la fonction.
    :param A: Point de départ.
    :param B: Point de transition.
    :param C: Point d'arrivée.
    :param s: Fonction s(t).
    :return: La valeur de dz(s(t))
    """
    u, DAB, u2 = get_coeff(A, B, C)
    ss = s(t)
    if ss <= DAB:
        return u[2]
    else:
        return u2[2]


def dx_gen(t, A, B, C, s, ds):
    """
    Fonction dx(t) avec paramètres en fonction du temps.
    :param t: Temps auquel calculer la fonction.
    :param A: Point de départ.
    :param B: Point de transition.
    :param C: Point d'arrivée.
    :param s: Fonction s(t).
    :param ds: Fonction ds(t).
    :return: La valeur de dx(t).
    """
    u, DAB, u2 = get_coeff(A, B, C)
    ss = s(t)
    if ss <= DAB:
        return u[0] * ds(t)
    else:
        return u2[0] * ds(t)


def dy_gen(t, A, B, C, s, ds):
    """
    Fonction dy(t) avec paramètres en fonction du temps.
    :param t: Temps auquel calculer la fonction.
    :param A: Point de départ.
    :param B: Point de transition.
    :param C: Point d'arrivée.
    :param s: Fonction s(t).
    :param ds: Fonction ds(t).
    :return: La valeur de dy(t).
    """
    u, DAB, u2 = get_coeff(A, B, C)
    ss = s(t)
    if ss <= DAB:
        return u[1] * ds(t)
    else:
        return u2[1] * ds(t)


def dz_gen(t, A, B, C, s, ds):
    """
    Fonction dz(t) avec paramètres en fonction du temps.
    :param t: Temps auquel calculer la fonction.
    :param A: Point de départ.
    :param B: Point de transition.
    :param C: Point d'arrivée.
    :param s: Fonction s(t).
    :param ds: Fonction ds(t).
    :return: La valeur de dz(t).
    """
    u, DAB, u2 = get_coeff(A, B, C)
    ss = s(t)
    if ss <= DAB:
        return u[2] * ds(t)
    else:
        return u2[2] * ds(t)


def get_Xs(A, B, C, V1, V2, Vmax, Amax):
    """
    Permet de récupérer toutes les fonctions X(s(t)). Avec X = (x, y, z).
    :param A: Point de départ.
    :param B: Point de transition.
    :param C: Point d'arrivée.
    :param V1: La vitesse maximale entre A et B.
    :param V2: La vitesse maximale entre B et C.
    :param Vmax: La vitesse maximale des moteurs.
    :param Amax: L'accélération maximale des moteurs.
    :return: xs (fonction x(s(t))), ys (fonction y(s(t))), zy (fonction z(s(t))), s (fonction s(t)), ts (temps de commutation).
    """

    def s(t):
        return s_gen(t, V1, V2, Vmax, Amax, A, B, C)

    def xs(t):
        return xs_gen(t, A, B, C, s)

    def ys(t):
        return ys_gen(t, A, B, C, s)

    def zs(t):
        return zs_gen(t, A, B, C, s)

    ts = getParams(V1, V2, Vmax, Amax, A, B, C)
    return xs, ys, zs, s, ts


def get_dX(A, B, C, V1, V2, Vmax, Amax):
    """
    Permet de récupérer toutes les fonctions dX(t). Avec X = (x, y, z).
    :param A: Point de départ.
    :param B: Point de transition.
    :param C: Point d'arrivée.
    :param V1: La vitesse maximale entre A et B.
    :param V2: La vitesse maximale entre B et C.
    :param Vmax: La vitesse maximale des moteurs.
    :param Amax: L'accélération maximale des moteurs.
    :return: xs (fonction dx(t)), dys (fonction y(t)), dzy (fonction z(t)), s (fonction s(t)), ts (temps de commutation).
    """

    def s(t):
        return s_gen(t, V1, V2, Vmax, Amax, A, B, C)

    def ds(t):
        return ds_gen(t, V1, V2, Vmax, Amax, A, B, C)

    def dx(t):
        return dx_gen(t, A, B, C, s, ds)

    def dy(t):
        return dy_gen(t, A, B, C, s, ds)

    def dz(t):
        return dz_gen(t, A, B, C, s, ds)

    ts = getParams(V1, V2, Vmax, Amax, A, B, C)
    return dx, dy, dz, s, ts


def get_dXs(A, B, C, V1, V2, Vmax, Amax):
    """
    Permet de récupérer toutes les fonctions dX(s(t)). Avec X = (x, y, z).
    :param A: Point de départ.
    :param B: Point de transition.
    :param C: Point d'arrivée.
    :param V1: La vitesse maximale entre A et B.
    :param V2: La vitesse maximale entre B et C.
    :param Vmax: La vitesse maximale des moteurs.
    :param Amax: L'accélération maximale des moteurs.
    :return: xs (fonction dx(s(t))), ys (fonction dy(s(t))), zy (fonction dz(s(t))), s (fonction s(t)), ts (temps de commutation).
    """

    def s(t):
        return s_gen(t, V1, V2, Vmax, Amax, A, B, C)

    def dxs(t):
        return dxs_gen(t, A, B, C, s)

    def dys(t):
        return dys_gen(t, A, B, C, s)

    def dzs(t):
        return dzs_gen(t, A, B, C, s)

    ts = getParams(V1, V2, Vmax, Amax, A, B, C)
    return dxs, dys, dzs, s, ts


def get_lois(A, B, C, V1, V2, Vmax, Amax):
    """
    Permet de récupérer toutes les fonctions S(t). Avec S = (s, ds, dds).
    :param A: Point de départ.
    :param B: Point de transition.
    :param C: Point d'arrivée.
    :param V1: La vitesse maximale entre A et B.
    :param V2: La vitesse maximale entre B et C.
    :param Vmax: La vitesse maximale des moteurs.
    :param Amax: L'accélération maximale des moteurs.
    :return: xs (fonction s(t)), ys (fonction ds(t)), zy (fonction dds(t)), ts (temps de commutation).
    """

    def s(t):
        return s_gen(t, V1, V2, Vmax, Amax, A, B, C)

    def ds(t):
        return ds_gen(t, V1, V2, Vmax, Amax, A, B, C)

    def dds(t):
        return dds_gen(t, V1, V2, Vmax, Amax, A, B, C)

    ts = getParams(V1, V2, Vmax, Amax, A, B, C)
    return s, ds, dds, ts


def invHomogene(matrice):
    """
    Permet d'inverser une matrice homogène.
    :param matrice: La matrice à inverser.
    :return: res (la matrice inversé).
    """
    R = np.asmatrix(matrice[:3, :3])
    P = np.asmatrix(matrice[:3, 3:])

    R2 = R.getT()
    P2 = - R2 * P
    res = np.concatenate((R2, P2), axis=1)
    res = np.concatenate((res, [[0, 0, 0, 1]]), axis=0)
    return res


def getTij(i, j, q, l, p, m):
    """
    Permet de calculer la matrice de passage homogène Tij.
    :param i: La base de départ.
    :param j: La base d'arrivée.
    :param q: [q1, q2, q3], la position des liaisons.
    :param l: Longueur l.
    :param p: Longueur p.
    :param m: Longueur m.
    :return: La matrice de passage homogène Tij.
    """
    if i == j:
        return np.matrix(np.eye(4))
    if j == i + 1:
        if i == 0:
            return getT01(q, l)
        if i == 1:
            return getT12(q, p)
        if i == 2:
            return getT23(q, m)
    if j > i:
        res = getTij(i, i + 1, q, l, p, m)
        for k in range(i + 1, j):
            res = res * getTij(k, k + 1, q, l, p, m)
        return res
    return invHomogene(getTij(j, i, q, l, p, m))


def getT01(q, l):
    """
    Permet de récupérer la matrice de passage homogène T01.
    :param q: [q1, q2, q3], la position des liaisons.
    :param l: Longueur l.
    :return: La matrice de passage homogène T01.
    """
    q1 = q[0]
    return np.matrix([
        [cos(q1), -sin(q1), 0, 0],
        [sin(q1), cos(q1), 0, 0],
        [0, 0, 1, l],
        [0, 0, 0, 1]])


def getT12(q, p):
    """
    Permet de récupérer la matrice de passage homogène T12.
    :param q: [q1, q2, q3], la position des liaisons.
    :param p: Longueur p.
    :return: La matrice de passage homogène T12.
    """
    q2 = q[1]
    return np.matrix([
        [cos(q2), -sin(q2), 0, p],
        [0, 0, -1, 0],
        [sin(q2), cos(q2), 0, 0],
        [0, 0, 0, 1]])


def getT23(q, m):
    """
    Permet de récupérer la matrice de passage homogène T23.
    :param q: [q1, q2, q3], la position des liaisons.
    :param m: Longueur m.
    :return: La matrice de passage homogène T23.
    """
    q3 = q[2]
    return np.matrix([
        [cos(q3), -sin(q3), 0, m],
        [sin(q3), cos(q3), 0, 0],
        [0, 0, 1, 0],
        [0, 0, 0, 1]])


def mgd(q, l, p, m, n):
    """
    Fonction mgd.
    :param q: [q1, q2, q3], la position des liaisons.
    :param l: Longueur l.
    :param p: Longueur p.
    :param m: Longueur m.
    :param n: Longieur n.
    :return: X = [x, y, z]
    """
    T03 = getTij(0, 3, q, l, p, m)
    R = np.asmatrix(T03[:3, :3])
    P = np.asmatrix(T03[:3, 3:])

    Xp = P + R * np.asmatrix([[n, 0, 0]]).getT()
    Xr = np.concatenate((R[:, 0], R[:, 2]), axis=0)

    return np.concatenate((Xp, Xr), axis=0)


def get_limit(lim, p):
    if p < lim[0] - 0.5:
        lim[0] = p - 0.5
    if p > lim[1] + 0.5:
        lim[1] = p + 0.5

    return lim


def update_limits(xlim, ylim, zlim, p):
    return get_limit(xlim, p[0]), get_limit(ylim, p[1]), get_limit(zlim, p[2])


def generate_lines(a, b, echant):
    x = np.linspace(a[0], b[0], echant)
    y = np.linspace(a[1], b[1], echant)
    z = np.linspace(a[2], b[2], echant)
    return x, y, z


def afficher(q, l, p, m, n, Xp=None):
    xlim = [-0.5, 0.5]
    ylim = [-0.5, 0.5]
    zlim = [0, 1.5]
    plt.figure()
    ax = plt.axes(projection='3d')
    colors = ('black', 'yellow', 'blue', 'green')

    liaison_j = [0, 0, 0]

    for i in range(len(q)):
        liaison_i = liaison_j
        liaison_j = np.squeeze(np.asarray(getTij(0, i + 1, q, l, p, m)[:3, 3:]))
        xline, yline, zline = generate_lines(liaison_i, liaison_j, 100)
        ax.plot3D(xline, yline, zline, colors[i])
        xlim, ylim, zlim = update_limits(xlim, ylim, zlim, liaison_j)

    liaison_i = liaison_j
    OT = np.squeeze(np.asarray(mgd(q, l, p, m, n)[:3]))
    xline, yline, zline = generate_lines(liaison_i, OT, 100)
    ax.plot3D(xline, yline, zline, colors[i + 1])
    _, _, _ = update_limits(xlim, ylim, zlim, OT)

    if Xp is not None:
        ax.scatter(Xp[0], Xp[1], Xp[2], marker='o')

    d = m + n + p
    ax.set_xlim(-d, d)
    ax.set_ylim(-d, d)
    ax.set_zlim(0, l)
    plt.show()


def mgi(Xp, l, p, m, n, qinit=None):
    """
    Le mgi.
    :param Xp: [xp, yp, zp], le point à atteindre.
    :param l: Longueur l.
    :param p: Longueur p.
    :param m: Longueur m.
    :param n: Longueur n
    :param qinit: [q01, q02, q03], la configuration initiale (None si premier appel)
    :return: Q = [q1, q2, q3], la solution la plus proche de qinit.
    """
    x0, y0, z0 = Xp
    if z0 < 0:
        return None

    q = []

    if x0 == y0 == 0:
        if qinit is None:
            q1 = 0
        else:
            q1 = qinit[0]
        print("Singularité")
    else:
        sq1 = sqrt(y0 ** 2 + x0 ** 2)
        s1 = y0 / sq1
        c1 = x0 / sq1
        q1 = atan2(s1, c1)

    z1 = z0 - l
    x1 = sqrt(y0 ** 2 + x0 ** 2) - p

    c3 = ((x1 ** 2 + z1 ** 2) - (n ** 2 + m ** 2)) / (2 * m * n)

    if x1 == z1 == 0:
        q2 = pi/2
        q3 = pi
        print("Singularité")
        q.append([q1, q2, q3])
    else:
        q3s = []
        if c3 > 1:
            return None
        elif c3 == 1:
            q3s.append(0)
        else:
            for e3 in [-1, 1]:
                q3s.append(e3 * acos(c3))
        for q3 in q3s:
            s3 = sin(q3)
            s = sqrt(x1 ** 2 + z1 ** 2)
            steta = z1 / s
            cteta = x1 / s

            spteta = n * s3 / s
            cpteta = (m + n * c3) / s

            teta = atan2(steta, cteta)
            pteta = atan2(spteta, cpteta)
            # q2 = atan(z1/x1) - atan((n * s3) / (m + n * c3))
            # Avec les atan2 c'est mieux
            q2 = teta - pteta
            q.append([q1, q2, q3])

    if qinit is None:
        res = q[0]
    else:
        # Cette ligne permet de choisir la solution la plus proche
        res = next(q[i] for i in range(len(q)) if
                   abs(qinit[2] - q[i][2]) == min([abs(qinit[2] - q[j][2]) for j in range(len(q))]))
    return res


def mdi(Xd, q, qinit, N, P):
    """
    Le mdi.
    :param Xd: [dxp, dyp, dzp], la vitesse à atteindre.
    :param q: [q01, q02, q02], la position actuelle des liaisons.
    :param qinit: [dq01, dq02, dq03], la vitesse actuelle des liaisons. (Mettre à None si premier appel)
    :param N: Longueur n.
    :param P: Longueur p.
    :return: sQ = [dq1, dq2, dq3]
    """
    if q is None:
        return [0, 0, 0]

    # On définit q1, q2 et q3 à partir du vecteur des q
    q1 = q[0]
    q2 = q[1]
    q3 = q[2]

    # Détermination du MDD, entrée dans une matrice numpy
    jacobAnalytique = np.matrix([[-np.sin(q1) * (N * np.cos(q2) + P + N * np.cos(q2 + q3)),
                                  - N * np.cos(q1) * (np.sin(q2) + np.sin(q2 + q3)),
                                  - N * np.cos(q1) * np.sin(q2 + q3)],
                                 [np.cos(q1) * (N * np.cos(q2) + P + N * np.cos(q2 + q3)),
                                  - N * np.sin(q1) * (np.sin(q2) + np.sin(q2 + q3)), -N * np.sin(q1) * np.sin(q2 + q3)],
                                 [0, N * (np.cos(q2) + np.cos(q2 + q3)), N * np.cos(q2 + q3)]])

    # On calcule le déterminant : s'il est nul, nous avons à faire à une singularité, on return le qinit passé en paramètre
    if np.linalg.det(jacobAnalytique) == 0:
        print("Singularité")
        if qinit is None:
            return np.array([0, 0, 0])
        return qinit
    # Sinon, on calcule l'inverse de la jacobienne, et on retourne le produit de la jacobienne inverse avec dX^T
    invJacobAnalytique = np.matrix(np.linalg.inv(jacobAnalytique))
    # print(np.array(invJacobAnalytique.dot(Xd.reshape((3, 1))).T)[0])
    return np.array(invJacobAnalytique.dot(Xd.reshape((3, 1))).T)[0]


def getNewParams(V2, AB, Amax):
    """
    Nouveaux paramètres en cas de cas dégradé (on atteint pas V1).
    :param V2: Vitesse maximale entre B et C.
    :param AB: Distance entre A et B.
    :param Amax: Accélération maximale des moteurs.
    :return: t12 (t1 et t2 maintenant égaux), t3
    """
    v = sqrt((V2 ** 2 + 2 * AB * Amax) / 2)
    t12 = ((V2 - v) / Amax * V2 * 2 + (v - V2) * (V2 - v) / Amax + 2 * AB) / v
    t3 = (v - V2) / Amax + t12

    return t12, t3


def getParams(V1, V2, Vmax, Amax, A, B, C):
    """
    Calcule les temps de commutation.
    :param V1: Vitesse macimale entre A et B.
    :param V2: Vitesse macimale entre B et C.
    :param Vmax: Vitesse maximale des moteurs.
    :param Amax: Accélération maximale des moteurs.
    :param A: Point de départ.
    :param B: Point de transition.
    :param C: Point d'arrivée.
    :return: t = [t0, t1, t2, t3, t4, t5], les temps de commutation.
    """
    if V1 > Vmax or V2 > Vmax:
        return None
    t = [0 for _ in range(6)]
    t[0] = 0
    t[1] = V1 / Amax
    AB = np.linalg.norm(np.array(B) - np.array(A))
    t[3] = (AB - (V1 * t[1]) / 2 - ((V1 + V2) * (V1 - V2)) / (Amax * 2) + V1 * t[1] + V1 * (V1 - V2) / Amax) / V1
    t[2] = t[3] - (V1 - V2) / Amax
    BC = np.linalg.norm(np.array(C) - np.array(B))

    # Si on atteint pas V2
    if (Amax * ((V2 / Amax) ** 2)) / 2 > AB:
        t[1] = t[2] = t[3] = V2 / Amax
        BC = np.linalg.norm(np.array(C)) - (Amax * (V2 / Amax) ** 2) / 2

    # Si on atteint pas V1
    elif t[1] > t[2]:
        t12, t3 = getNewParams(V2, AB, Amax)
        t[1] = t12
        t[2] = t12
        t[3] = t3
    t[5] = BC / V2 + t[3] + V2 / Amax - V2 / (2 * Amax)
    t[4] = t[5] - V2 / Amax
    return t


def s_gen(t, V1, V2, Vmax, Amax, A, B, C):
    """
    Fonction s(t) avec paramètres en fonction du temps.
    :param t: Temps auquel calculer la fonction.
    :param V1: Vitesse macimale entre A et B.
    :param V2: Vitesse macimale entre B et C.
    :param Vmax: Vitesse maximale des moteurs.
    :param Amax: Accélération maximale des moteurs.
    :param A: Point de départ.
    :param B: Point de transition.
    :param C: Point d'arrivée.
    :return: La valeur de s(t)
    """
    ts = getParams(V1, V2, Vmax, Amax, A, B, C)
    t1 = ts[1]
    t2 = ts[2]
    t3 = ts[3]
    t4 = ts[4]
    tf = ts[5]

    if t <= t1:
        return (Amax * t ** 2) / 2
    if t <= t2:
        return s_gen(t1, V1, V2, Vmax, Amax, A, B, C) + Amax * t1 * (t - t1)
    if t <= t3:
        return s_gen(t2, V1, V2, Vmax, Amax, A, B, C) - (Amax * (t - t2) ** 2) / 2 + Amax * t1 * (t - t2)
    if t <= t4:
        return s_gen(t3, V1, V2, Vmax, Amax, A, B, C) + V2 * (t - t3)
    if t <= tf:
        return s_gen(t4, V1, V2, Vmax, Amax, A, B, C) - (Amax * (t - t4) ** 2) / 2 + V2 * (t - t4)


def ds_gen(t, V1, V2, Vmax, Amax, A, B, C):
    """
    Fonction ds(t) avec paramètres en fonction du temps.
    :param t: Temps auquel calculer la fonction.
    :param V1: Vitesse macimale entre A et B.
    :param V2: Vitesse macimale entre B et C.
    :param Vmax: Vitesse maximale des moteurs.
    :param Amax: Accélération maximale des moteurs.
    :param A: Point de départ.
    :param B: Point de transition.
    :param C: Point d'arrivée.
    :return: La valeur de ds(t)
    """
    ts = getParams(V1, V2, Vmax, Amax, A, B, C)
    t1 = ts[1]
    t2 = ts[2]
    t3 = ts[3]
    t4 = ts[4]
    tf = ts[5]

    if t <= t1:
        return Amax * t
    if t <= t2:
        return Amax * t1
    if t <= t3:
        return -Amax * (t - t2) + Amax * t1
    if t <= t4:
        return V2
    if t <= tf:
        return -Amax * (t - t4) + V2
    return 0


def dds_gen(t, V1, V2, Vmax, Amax, A, B, C):
    """
    Fonction dds(t) avec paramètres en fonction du temps.
    :param t: Temps auquel calculer la fonction.
    :param V1: Vitesse macimale entre A et B.
    :param V2: Vitesse macimale entre B et C.
    :param Vmax: Vitesse maximale des moteurs.
    :param Amax: Accélération maximale des moteurs.
    :param A: Point de départ.
    :param B: Point de transition.
    :param C: Point d'arrivée.
    :return: La valeur de dds(t)
    """
    ts = getParams(V1, V2, Vmax, Amax, A, B, C)
    t1 = ts[1]
    t2 = ts[2]
    t3 = ts[3]
    t4 = ts[4]
    tf = ts[5]

    if t <= t1:
        return Amax
    if t <= t2:
        return 0
    if t <= t3:
        return -Amax
    if t <= t4:
        return 0
    if t <= tf:
        return -Amax
    return 0


def afficher_courbes(x1, x2, x3, tcs, titre, t1, t2, t3, s=None, tc=False, q=False):
    y1 = []
    y2 = []
    y3 = []
    x = []
    if not q:
        for t in np.arange(0, tcs[5], step=0.01):
            y1.append(x1(t))
            y2.append(x2(t))
            y3.append(x3(t))
            if s is None:
                x.append(t)
            else:
                x.append(s(t))
    else:
        for q, t in x1:
            y1.append(q[0])
            y2.append(q[1])
            y3.append(q[2])
            if s is None:
                x.append(t)
            else:
                x.append(s(t))

    plt.figure()
    plt.subplot(311)
    plt.title(titre)
    plt.ylabel(t1)
    plt.plot(x, y1, "y-")
    if tc:
        for t in tcs:
            plt.plot([t, t], [min(y1), max(y1)], "r-")

    plt.subplot(312)
    plt.ylabel(t2)
    plt.plot(x, y2, "y-")
    if tc:
        for t in tcs:
            plt.plot([t, t], [min(y2), max(y2)], "r-")

    plt.subplot(313)
    plt.ylabel(t3)
    plt.plot(x, y3, "y-")
    if tc:
        for t in tcs:
            plt.plot([t, t], [min(y3), max(y3)], "r-")
    plt.show()


def q_generateur(xs, ys, zs, ts, l, p, m, n):
    q0 = None
    for t in np.arange(0, ts[5], step=0.01):
        q = mgi(np.array([xs(t), ys(t), zs(t)]), l, p, m, n, q0)
        q0 = q
        yield q, t


def dq_generateur(xs, ys, zs, dxs, dys, dzs, ts, l, p, m, n):
    dq0 = None
    q0 = None
    for t in np.arange(0, ts[5], step=0.01):
        X = np.array([xs(t), ys(t), zs(t)])
        dX = np.array([dxs(t), dys(t), dzs(t)])
        dq = mdi(dX, q0, dq0, n, p)
        q0 = mgi(X, l, p, m, n, q0)
        dq0 = dq
        yield dq, t


class BE:

    def __init__(self, l=1, m=5, p=3, n=5, Amax=1, Vmax=100, A=np.array([1, 0, 1]), B=np.array([3, 0, 1]),
                 C=np.array([7, 0, 1]), V1=99, V2=1):
        """
        Initialisation de la classe BE qui permet de faire toutes les opérations du projet. Les paramètres sont optionnels, ils peuvent être modifier par d'autes fonctions.
        :param l: Longueur l.
        :param m: Longueur m.
        :param p: Longueur p.
        :param n: Longueur n.
        :param Amax: Accélération maximale des moteurs.
        :param Vmax: Vitesse maximale des moteurs.
        :param A: Position de départ.
        :param B: Position de transition.
        :param C: Position d'arrivée.
        :param V1: Vitesse maximale entre A et B.
        :param V2: Vitesse maximale entre B et C.
        """
        self.l = l
        self.m = m
        self.n = n
        self.p = p
        self.Amax = Amax
        self.Vmax = Vmax
        self.A = np.array(A)
        self.B = np.array(B)
        self.C = np.array(C)
        self.V1 = V1
        self.V2 = V2

    def setA(self, A):
        """
        Permet de modifier le point de départ A.
        :param A:
        """
        self.A = np.array(A)

    def setB(self, B):
        """
        Permet de modifier le point de transition B.
        :param B:
        """
        self.B = np.array(B)

    def setC(self, C):
        """
        Permet de modifier le point de départ C.
        :param C:
        """
        self.C = np.array(C)

    def setL(self, l):
        """
        Permet de modifier la longueur l.
        :param l:
        """
        self.l = l

    def setM(self, m):
        """
        Permet de modifier la longueur m.
        :param m:
        """
        self.m = m

    def setN(self, n):
        """
        Permet de modifier la longueur n.
        :param n:
        """
        self.n = n

    def setP(self, p):
        """
        Permet de modifier la longueur p.
        :param p:
        :return:
        """
        self.p = p

    def setAmax(self, Amax):
        """
        Permet de modifier l'accélération maximale des moteurs.
        :param Amax:
        """
        self.Amax = Amax

    def setVmax(self, Vmax):
        """
        Permet de modifier la vitesse maximale des moteurs.
        :param Vmax:
        """
        self.Vmax = Vmax

    def setV1(self, V1):
        """
        Permet de modifier la vitesse maximale entre A et B.
        :param V1:
        """
        self.V1 = V1

    def setV2(self, V2):
        """
        Permet de modifier la vitesse maximale entre B et C.
        :param V2:
        """
        self.V2 = V2

    def q(self, A=None, B=None, C=None, V1=None, V2=None):
        """
        Permet d'afficher les courbes de position des liaisons.
        :param A: Position de départ.
        :param B: Position de transition.
        :param C: Position d'arrivée.
        :param V1: Vitesse maximale entre A et B.
        :param V2: Vitesse maximale entre B et C.
        """
        if A is None:
            A = self.A
        if B is None:
            B = self.B
        if C is None:
            C = self.C
        if V1 is None:
            V1 = self.V1
        if V2 is None:
            V2 = self.V2

        xs, ys, zs, s, ts = get_Xs(A, B, C, V1, V2, self.Vmax, self.Amax)
        generateur_q = q_generateur(xs, ys, zs, ts, self.l, self.p, self.m, self.n)

        ts = getParams(V1, V2, self.Vmax, self.Amax, A, B, C)
        afficher_courbes(generateur_q, None, None, ts, "Position des liaisons", "q1(t)", "q2(t)", "q3(t)", tc=True,
                         q=True)

    def dq(self, A=None, B=None, C=None, V1=None, V2=None):
        """
        Permet d'afficher les courbes de vitesse des liaisons.
        :param A: Position de départ.
        :param B: Position de transition.
        :param C: Position d'arrivée.
        :param V1: Vitesse maximale entre A et B.
        :param V2: Vitesse maximale entre B et C.
        :return:
        """
        if A is None:
            A = self.A
        if B is None:
            B = self.B
        if C is None:
            C = self.C
        if V1 is None:
            V1 = self.V1
        if V2 is None:
            V2 = self.V2

        xs, ys, zs, _, _ = get_Xs(A, B, C, V1, V2, self.Vmax, self.Amax)
        dxs, dys, dzs, s, ts = get_dX(A, B, C, V1, V2, self.Vmax, self.Amax)
        generateur_dq = dq_generateur(xs, ys, zs, dxs, dys, dzs, ts, self.l, self.p, self.m, self.n)

        ts = getParams(V1, V2, self.Vmax, self.Amax, A, B, C)
        afficher_courbes(generateur_dq, None, None, ts, "Vitesse des liaisons", "dq1(t)", "dq2(t)", "dq3(t)", tc=True,
                         q=True)

    def setParams(self, A, B, C, V1, V2):
        """
        Permet de modifier les paramètres pour les lois de mouvements.
        :param A: Position de départ.
        :param B: Position de transition.
        :param C: Position d'arrivée.
        :param V1: Vitesse maximale entre A et B.
        :param V2: Vitesse maximale entre B et C.
        """
        self.A = A
        self.B = B
        self.C = C
        self.V1 = V1
        self.V2 = V2

    def dxs(self, A=None, B=None, C=None, V1=None, V2=None):
        """
        Permet d'afficher les courbes de vitesse dans l'espace opérationnel.
        :param A: Position de départ.
        :param B: Position de transition.
        :param C: Position d'arrivée.
        :param V1: Vitesse maximale entre A et B.
        :param V2: Vitesse maximale entre B et C.
        """
        if A is None:
            A = self.A
        if B is None:
            B = self.B
        if C is None:
            C = self.C
        if V1 is None:
            V1 = self.V1
        if V2 is None:
            V2 = self.V2

        dxs, dys, dzs, s, ts = get_dXs(A, B, C, V1, V2, self.Vmax, self.Amax)
        afficher_courbes(dxs, dys, dzs, ts, "Vitesses dans l'espace opérationnel", "dx(s)", "dy(s)", "dz(s)", s)

    def xt(self, A=None, B=None, C=None, V1=None, V2=None):
        """
        Permet d'afficher les courbes de trajectoire dans l'espace cartésien.
        :param A: Position de départ.
        :param B: Position de transition.
        :param C: Position d'arrivée.
        :param V1: Vitesse maximale entre A et B.
        :param V2: Vitesse maximale entre B et C.
        """
        if A is None:
            A = self.A
        if B is None:
            B = self.B
        if C is None:
            C = self.C
        if V1 is None:
            V1 = self.V1
        if V2 is None:
            V2 = self.V2

        xs, ys, zs, s, ts = get_Xs(A, B, C, V1, V2, self.Vmax, self.Amax)
        afficher_courbes(xs, ys, zs, ts, "Trajectoires dans l'espace cartésien", "x(t)", "y(t)", "z(t)", tc=True)

    def lois_de_mouvement(self, A=None, B=None, C=None, V1=None, V2=None):
        """
        Permet d'afficher les lois de mouvements.
        :param A: Position de départ.
        :param B: Position de transition.
        :param C: Position d'arrivée.
        :param V1: Vitesse maximale entre A et B.
        :param V2: Vitesse maximale entre B et C.
        """
        if A is None:
            A = self.A
        if B is None:
            B = self.B
        if C is None:
            C = self.C
        if V1 is None:
            V1 = self.V1
        if V2 is None:
            V2 = self.V2

        s, ds, dds, ts = get_lois(A, B, C, V1, V2, self.Vmax, self.Amax)
        afficher_courbes(s, ds, dds, ts, "Lois de mouvements", "s(t)", "ds(t)", "dds(t)", tc=True)

    def dxt(self, A=None, B=None, C=None, V1=None, V2=None):
        """
        Permet d'afficher les courbes de vitesse dans l'espace cartésien.
        :param A: Position de départ.
        :param B: Position de transition.
        :param C: Position d'arrivée.
        :param V1: Vitesse maximale entre A et B.
        :param V2: Vitesse maximale entre B et C.
        """
        if A is None:
            A = self.A
        if B is None:
            B = self.B
        if C is None:
            C = self.C
        if V1 is None:
            V1 = self.V1
        if V2 is None:
            V2 = self.V2

        dx, dy, dz, s, ts = get_dX(A, B, C, V1, V2, self.Vmax, self.Amax)
        afficher_courbes(dx, dy, dz, ts, "Vitesses dans l'espace cartésien", "dx(t)", "dy(t)", "dz(t)", tc=True)

    def xs(self, A=None, B=None, C=None, V1=None, V2=None):
        """
        Permet d'afficher les courbes de trajectoire dans l'espace opérationnel.
        :param A: Position de départ.
        :param B: Position de transition.
        :param C: Position d'arrivée.
        :param V1: Vitesse maximale entre A et B.
        :param V2: Vitesse maximale entre B et C.
        """
        if A is None:
            A = self.A
        if B is None:
            B = self.B
        if C is None:
            C = self.C
        if V1 is None:
            V1 = self.V1
        if V2 is None:
            V2 = self.V2

        xs, ys, zs, s, ts = get_Xs(A, B, C, V1, V2, self.Vmax, self.Amax)
        afficher_courbes(xs, ys, zs, ts, "Trajectoires dans l'espace opérationnel", "x(s)", "y(s)", "z(s)", s)

    def trajrecouvre(self, A=None, B=None, C=None, V1=None, V2=None):
        """
        Permet d'afficher toutes les courbes utiles pour la planification de trajectoire.
        :param A: Position de départ.
        :param B: Position de transition.
        :param C: Position d'arrivée.
        :param V1: Vitesse maximale entre A et B.
        :param V2: Vitesse maximale entre B et C.
        """
        if A is None:
            A = self.A
        if B is None:
            B = self.B
        if C is None:
            C = self.C
        if V1 is None:
            V1 = self.V1
        if V2 is None:
            V2 = self.V2

        self.lois_de_mouvement(A, B, C, V1, V2)
        self.xs(A, B, C, V1, V2)
        self.dxs(A, B, C, V1, V2)
        self.xt(A, B, C, V1, V2)
        self.dxt(A, B, C, V1, V2)
        self.q(A, B, C, V1, V2)
        self.dq(A, B, C, V1, V2)
