# Utiliser le code du groupe 1
AIRAUD Thomas, BORDES Dorian, GONET Killian, LABORIE Rémi, RUIMY Yaël

## I - Importer la librairie
Notre code doit s'utiliser comme une librairie python.<br>
Il faut utiliser la classe BE du module groupe1.<br><br>
```python
from groupe1 import BE
```

## II - Modifier les paramètres du robot et du mouvement
Il y a plusieurs façon de faire, le code est assez flexible.<br>

### II - 1 - Lors de l'initialisation
Si les variables sont définies lors de l'initialisation, elles agiront comme des **valeurs par défaut** par la suite.
```python
from groupe1 import BE

l = 1
m = 5
p = 3
n = 5
Amax = 1
Vmax = 100
A = [5, 0, 1]
B = [6, 0, 1]
C = [7, 0, 1]
V1 = 99
V2 = 1

be = BE(l=l, m=m, n=n, p=p, Amax=Amax, Vmax=Vmax, A=A, B=B, C=C, V1=V1, V2=V2)
```

### II - 2 - Utilisation des setters
Il existe un setter pour chaque variable. Si une variable est modifié de cette façon, **la valeur par défaut sera modifiée**.
````python
from groupe1 import BE

be = BE()

be.setA(A)
be.setB(B)
be.setC(C)
be.setV1(V1)
be.setV2(V2)

be.setParams(A, B, C, V1, V2)

be.setAmax(Amax)
be.setVmax(Vmax)
````

### II - 3 - Lors de l'appel aux fonctions
Il est également possible de modifier les variables **temporairement** pour afficher des courbes particulières.<br>
Les variables non attribués sont automatiquement mises à leur valeur par défaut.

`````python
from groupe1 import BE

be = BE()
be.trajrecouvre(A, B, C, V1, V2)
be.trajrecouvre(C=C, V2=V2, B=B)

`````

## III - Affichage des courbes
Il existe une fonction pour chaque triplet de courbes.<br>
Également une fonction qui affiche toutes les courbes intéressantes : `trajrecouvre`

````python
from groupe1 import BE

be = BE()

# Lois de mouvement
be.lois_de_mouvement()

# Mouvement de la tâche
be.xt()
be.dxt()

# Trajectoire opérationnelle
be.xs()
be.dxs()

# Espace articulaire
be.q()
be.dq()

be.trajrecouvre()
````