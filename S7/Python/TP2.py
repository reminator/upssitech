from os import defpath


class Stack:

    def __init__(self, liste):
        self.elements = liste

    def empile(self, element):
        self.elements.append(element)

    def depile(self):
        return self.elements.pop()

    def __len__(self):
        return len(self.elements)

    def next(self):
        return self.elements[(len(self) - 1)]

    def toList(self):
        return self.elements

    def __iter__(self):
        return iter(self.elements)

    def copy(self):
        return Stack(self.elements.copy())

    def __repr__(self) -> str:
        return str(self.elements)


class Graph:

    def __init__(self, sommets=None):
        if sommets is None:
            sommets = {}
        self.sommets = sommets

    def __repr__(self) -> str:
        sommets = ""
        for (s, a) in self.sommets.items():
            sommets += str(s) + " " * (15 - len(str(s))) + " -> " + str(a) + "\n"

        return sommets

    def add(self, sommet, adj):
        if sommet in self.sommets:
            self.sommets[sommet].append(adj)
        else:
            self.sommets[sommet] = [adj]

        if adj in self.sommets:
            self.sommets[adj].append(sommet)
        else:
            self.sommets[adj] = [sommet]

    def depth_first(self, first):
        shown = [first]
        pile = Stack([first])

        # Tant qu'il y a un sommet à visiter, on visite
        while len(pile) != 0:
            sommet = pile.next()
            # On regarde les adjacents
            for a in self.sommets[sommet]:
                # Si on trouve un adjacent, on l'ajoute dans shown, on l'empile et on le yield
                if a not in shown:
                    shown.append(a)
                    pile.empile(a)
                    yield a
            if sommet == pile.next():
                pile.depile()

    def comp_con(self):
        shown = []
        res = []
        for s in self.sommets:
            if s not in shown:
                new = set(self.depth_first(s))
                new.add(s)
                res.append(new)
                for n in new:
                    shown.append(n)

        return res

    def dijkstra(self, start, end=None):
        composante = set(self.depth_first(start))
        composante.add(start)
        res = {}
        completed = set()

        for sommet in self.sommets:
            res[(start, sommet)] = []
        res[(start, start)] = [start]

        while set(completed) != composante:

            choix = next((sommet for sommet in composante if
                          sommet not in completed and len(res[(start, sommet)]) == min(
                              len(res[start, s]) for s in composante if
                              s not in completed and len(res[(start, s)]) != 0)))
            completed.add(choix)
            if choix != start:
                res[(start, choix)].append(choix)

            for suivant in [s for s in self.sommets[choix] if s not in completed]:
                if len(res[start, suivant]) > len(res[start, choix]) + 1 or len(res[start, suivant]) == 0:
                    res[(start, suivant)] = res[(start, choix)].copy()

        for sommet in self.sommets:
            if sommet not in composante:
                res[(start, sommet)] = "Pas de chemin disponible"

        if end == None:
            return res
        else:
            return res[(start, end)]


graphe = Graph({
    1: [2, 3, 8],
    2: [1],
    3: [3, 4],
    4: [3, 7],
    5: [6],
    6: [5],
    7: [4, 8],
    8: [1, 7]
})

print("\n\nLe graphe :")
print(graphe)
sommet = 1
print("Parcours en profondeur de", sommet)
for s in graphe.depth_first(sommet):
    print(s)

print("Composantes :")
print(graphe.comp_con())

print(graphe.dijkstra(1))

file = open("stormofswords.csv")
lines = file.readlines()
g = Graph()

for line in lines[1:]:
    mots = line.split(",")
    g.add(mots[0], mots[1])

print("\n\n\nLe graphe :")
print(g)

sommet = "Marillion"
print("Parcours en profondeur de", sommet)
for s in g.depth_first(sommet):
    print(s)

print("Composantes :")
for c in g.comp_con():
    print(c)

print(g.dijkstra("Ilyn", "Aegon"))
