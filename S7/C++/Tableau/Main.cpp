#include "Tableau.h"
#include <iostream>
using namespace std;

int main() {
    Tableau t(10);

    t.add(3);
    t.add(1);
    t.add(4);
    t.add(1);
    t.add(5);

    t.afficher();
    Tableau t2(t);
    t2.afficher();

    t.add(9);

    t.afficher();
    t2.afficher();
}