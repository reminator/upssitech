#include "Tableau.h"
#include <iostream>
using namespace std;

Tableau::Tableau(int tailleMax) : tailleMax(tailleMax), nbElements(0){
    this->valeurs = new int[tailleMax];
}

// Constructeur copie
Tableau::Tableau(Tableau & copie) {
    this->valeurs = new int[copie.tailleMax];
    for (int i = 0; i < copie.nbElements; i++) {
        this->valeurs[i] = copie.valeurs[i];
    }
    this->nbElements = copie.nbElements;
    this->tailleMax = copie.tailleMax;
}

// Destructeur
Tableau::~Tableau() {
    cout << "Destruction de ";
    this->afficher();
    delete [] valeurs;
}

void Tableau::add(int valeur){
    this->valeurs[this->nbElements] = valeur;
    this->nbElements++;
}

void Tableau::afficher() const{
    cout << "[";
    for (int i = 0; i < this->nbElements; i++) {
        if (i == this->nbElements - 1)
            cout << this->valeurs[i];
        else
            cout << this->valeurs[i] << ", ";
    }
    cout << "]" << endl;
}

/*
Tableau::Tableau() {
    Tableau::tableaux.push_back(this);
}
*/

Tableau Tableau::operator+(const Tableau & t2) {
    // ...
    // Retourner une nouvelle instance de l'objet
}

/*
Tableau & Tableau::operator=(const Tableau & copie) {
    if (this != &copie) {
        delete [] this->entrees;
        this->entrees = new Entree[copie.tailleMax];
        for (int i = 0; i < copie.nbElements; i++) {
            this->entrees[i] = copie.entrees[i];
        }
        this->tailleMax = copie.tailleMax;
        this->nbElements = copie.nbElements;
    }
    return *this;
}*/