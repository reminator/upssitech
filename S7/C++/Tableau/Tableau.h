#ifndef TABLEAU_H
#define TABLEAU_H

class Tableau {
    // Compte pourra acceder au private de Tableau
    friend class Compte;

    private: // Facultatif
    int * valeurs;
    int nbElements = 0;
    int tailleMax;
    static int nbTableau; // Obligé de l'initialiser (on peut aussi le faire dans le .cpp : int Tableau::nbTableau = 0) ???? marche pas
    // ??? static list<Tableau *> tableaux;

    public:
    // Il faut impérativement coder ces 4 fonctions si la classe contient des atributs pointeurs
    Tableau(int tailleMax = 10);
    Tableau(Tableau & copie);
    ~Tableau();
    // Il faut aussi définir l'opérateur égal quand elle contient des pointeurs (redite)
    Tableau& operator=(const Tableau & copie);

    void add(int valeur);
    // const pour dire qu'on ne modifie pas les variables
    void afficher() const;

    // Redéfinir l'addition
    Tableau operator+(const Tableau &);
};

#endif