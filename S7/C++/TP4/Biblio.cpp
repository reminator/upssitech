#include "Biblio.h"

Biblio::Biblio() {
}

void Biblio::ajouter(Document * document) {
    this->tab.push_back(document);
}

Document * Biblio::rechercher(string titre) {
    for (list<Document *>::iterator it = this->tab.begin(); it !=this->tab.end(); it++) {
        if ((*it)->titre == titre) {
            return *it;
        }
    }
}

void Biblio::afficher() {
    cout << "Biblio{[";
    int i = 0;
    for (list<Document *>::iterator it = this->tab.begin(); it !=this->tab.end(); it++) {
        if (i == this->tab.size() - 1) {
            cout << *(*it);
        } else {
            cout << *(*it) << ", ";
        }
        i++;
    }
    cout << "]}" << endl;
}