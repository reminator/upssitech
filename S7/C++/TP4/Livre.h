#include "Document.h"

class Livre : public Document {

    string editeur;
    string parution;

    public:
    Livre();
    Livre(const string & titre, const string auteur, const string * resume, const string editeur, const string parution);
    Livre(const Livre & copie);

    int getCout();

    Livre & clone() const;
    friend ostream & operator<<(ostream &, const Livre &);
    Livre & operator=(const Livre &);
};