#include <iostream>
#include <string>
using namespace std;

#ifndef DOCUMENT_H
#define DOCUMENT_H

class Document {

    friend class Biblio;
    
    protected:
    string & titre;
    string auteur;
    string * resume;


    public:
    Document();
    Document(const string & titre, const string auteur, const string * resume);
    Document(const Document & copie);
    ~Document();

    virtual int getCout() = 0;

    virtual Document & clone() const = 0;
    friend ostream & operator<<(ostream &, const Document &);
    Document & operator=(const Document &);
};

#endif