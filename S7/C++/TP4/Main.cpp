#include "Livre.h"
#include "Article.h"
#include "Biblio.h"

int main(void) {
    string s = "*******************************************************************************************";

    
    cout << endl << s << endl << "Création et affichage d'un Livre avec le constructeur par défaut" << endl << s << endl << endl;
    Livre l1 = Livre();
    cout << l1 << endl;

    cout << endl << s << endl << "Création et affichage d'un Livre créé avec un paramètre" << endl << s << endl << endl;
    Livre l2 = Livre(*new string("Titre"), "auteur", new string("resume"), "éditeur", "date de parution");
    cout << l2 << endl;

    cout << endl << s << endl << "Création et affichage d'un Livre créé par copie" << endl << s << endl << endl;
    Livre l3 = Livre(l2);
    cout << l3 << endl;

    cout << endl << s << endl << "Récupération et affichage d'un clone" << endl << s << endl << endl;
    Livre cloneLivre = l3.clone();
    cout << cloneLivre << endl;



    
    cout << endl << s << endl << "Création et affichage d'un Article avec le constructeur par défaut" << endl << s << endl << endl;
    Article a1 = Article();
    cout << a1 << endl;

    cout << endl << s << endl << "Création et affichage d'un Article créé avec un paramètre" << endl << s << endl << endl;
    Article a2 = Article(*new string("Titre"), "auteur", new string("resume"), "Titre de revue", "éditeur", "Numéro d'édition");
    cout << a2 << endl;

    cout << endl << s << endl << "Création et affichage d'un Article créé par copie" << endl << s << endl << endl;
    Article a3(a2);
    cout << a3 << endl;

    cout << endl << s << endl << "Récupération et affichage d'un clone" << endl << s << endl << endl;
    Article cloneArticle = a2.clone();
    cout << cloneArticle << endl;


    
    cout << endl << s << endl << "Test des operator=" << endl << s << endl << endl;
    l3 = l1;
    a1 = a3;

    cout << l3 << endl;
    cout << a1 << endl;




    cout << endl << s << endl << "Test de la Biblio" << endl << s << endl << endl;
    Biblio b;
    b.afficher();

    b.ajouter(&l1);
    b.ajouter(&a1);
    b.afficher();

    
    cout << endl << s << endl << "Test de getCout" << endl << s << endl << endl;

    cout << endl << s << endl << "Cout d'un livre" << endl << s << endl << endl;

    Document * d = b.rechercher("Pas de titre");
    cout << d->getCout() << endl;

    cout << endl << s << endl << "Cout d'un article" << endl << s << endl << endl;

    d = b.rechercher("Titre");
    cout << d->getCout() << endl;
}