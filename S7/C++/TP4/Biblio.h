#include <list>
#include <iostream>
#include "Document.h"
using namespace std;
 
class Biblio {

    list <Document *> tab;

    public:
    Biblio();

    void ajouter (Document * document); 
    Document * rechercher(string titre); 
    void afficher();
};