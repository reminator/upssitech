//
// Created by remil on 15/12/2021.
//

#include "CarteParade.h"
#include "CarteAction.h"
#include "Joueur.h"

CarteParade::CarteParade(TypeAttaque type) : CarteAction(type) {

}

bool CarteParade::applicable(const Joueur & j) const {
    CarteAction ** attaques = j.getAttaques();
    int nbElements = j.getNbAttaques();

    for (int i = 0; i < nbElements; i++) {
        if (attaques[i]->getType() == this->getType()) {
            return true;
        }
    }
    return false;
}

int CarteParade::getValeur(const Joueur & j) const {
    if(this->applicable(j)){
        return 300;
    }
    return 0;
}

void CarteParade::appliquer(Joueur & joueur) const {
    if(this->applicable(joueur)){
        joueur.retirerAttaque(this->getType());
    }
}