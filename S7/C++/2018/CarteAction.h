//
// Created by remil on 15/12/2021.
//

#ifndef INC_2018_CARTEACTION_H
#define INC_2018_CARTEACTION_H
enum TypeAttaque {Limite, Panne, Crevaison, Feu};

#include "Joueur.h"
#include "Carte.h"

class Joueur;

class CarteAction : public Carte {

    TypeAttaque type;

    public:
        CarteAction(TypeAttaque);
        CarteAction();

        virtual bool applicable(const Joueur &) const = 0;
        virtual void appliquer(Joueur &) const = 0;
        virtual int getValeur(const Joueur &) const = 0;

        TypeAttaque getType() const;
};


#endif //INC_2018_CARTEACTION_H
