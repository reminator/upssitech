//
// Created by remil on 15/12/2021.
//

#include "Joueur.h"
#include "Jeu.h"


Joueur::Joueur(string name, Jeu & jeu, int tailleAttaques=TAILLE_MAX, int tailleCartes=TAILLE_MAX) :
                                                                tailleCartes(tailleCartes),
                                                                tailleAttaques(tailleAttaques),
                                                                refJeu(jeu),
                                                                nom(name),
                                                                nbElemAttaques(0),
                                                                nbElemCarte(0) {

    this->cartes = new Carte*[this->tailleCartes];
    this->attaques = new CarteAction*[this->tailleAttaques];
}

Joueur::Joueur(const Joueur & copie) : refJeu(copie.refJeu),
                                       tailleCartes(copie.tailleCartes),
                                       tailleAttaques(copie.tailleAttaques),
                                       nom(copie.nom),
                                       nbElemAttaques(copie.nbElemAttaques),
                                       nbElemCarte(copie.nbElemCarte) {

    this->cartes = new Carte*[this->tailleCartes];
    this->attaques = new CarteAction*[this->tailleAttaques];

    for (int i = 0; i < this->tailleCartes; i++) {
        *this->cartes[i] = *copie.cartes[i];
    }

    for (int i = 0; i < this->tailleAttaques; i++) {
        *this->attaques[i] = *copie.attaques[i];
    }
}

Joueur & Joueur::operator=(const Joueur & copie) {
    refJeu = copie.refJeu;

    for (int i = 0; i < this->tailleCartes; i++) {
        delete this->cartes[i];
    }
    delete [] this->cartes;

    for (int i = 0; i < this->tailleAttaques; i++) {
        delete this->attaques[i];
    }
    delete [] this->attaques;

    return *this;
}

Joueur::~Joueur() {

    for (int i = 0; i < this->tailleCartes; i++) {
        delete this->cartes[i];
    }
    delete [] this->cartes;

    for (int i = 0; i < this->tailleAttaques; i++) {
        delete this->attaques[i];
    }
    delete [] this->attaques;
}

void Joueur::operator+(Carte * carte) {
    if (this->nbElemCarte < this->tailleCartes) {
        this->cartes[this->nbElemCarte] = carte;
        this->nbElemCarte++;
    }
}

void Joueur::ajouterCarteAction(CarteAction * carte) {
    if (this->nbElemAttaques < this->tailleAttaques) {
        this->attaques[this->nbElemAttaques] = carte;
        this->nbElemAttaques++;
    }
}

int Joueur::getNbAttaques() const {
    return this->nbElemAttaques;
}

CarteAction ** Joueur::getAttaques() const {
    return this->attaques;
}

void Joueur::retirerAttaque(const TypeAttaque & type) {
    for (int i = 0; i < this->nbElemAttaques; i++) {
        if (this->attaques[i]->getType() == type) {
            delete attaques[i];
            this->attaques[i] = this->attaques[nbElemAttaques - 1];
            this->nbElemAttaques--;
            break;
        }
    }
}

void Joueur::afficherCartes() const {

    cout << "Joueur " << this->nom << endl;

    cout << "Main : [";
    for (int i = 0; i < this->nbElemCarte; i++) {
        cout << this->cartes[i]->getNom();
        if (i < this->nbElemCarte-1) {
            cout << ", ";
        }
    }
    cout << "]" << endl;

    cout << "Attaques : [";
    for (int i = 0; i < this->nbElemAttaques; i++) {
        cout << this->attaques[i]->getNom();
        if (i < this->nbElemAttaques-1) {
            cout << ", ";
        }
    }
    cout << "]";
}

ostream & operator<<(ostream & out, const Joueur & j) {
    out << j.nom;
    return out;
}