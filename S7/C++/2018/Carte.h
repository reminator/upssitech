//
// Created by remil on 15/12/2021.
//

#ifndef INC_2018_CARTE_H
#define INC_2018_CARTE_H

#include <iostream>
using namespace std;
#include "Joueur.h"

class Joueur;
class Carte {

    string nom;

public:
    virtual bool applicable(const Joueur &) const = 0;
    virtual void appliquer(Joueur &) const = 0;
    virtual int getValeur(const Joueur &) const = 0;
    string getNom() const;
};


#endif //INC_2018_CARTE_H
