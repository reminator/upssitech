//
// Created by remil on 15/12/2021.
//

#ifndef INC_2018_CARTEPARADE_H
#define INC_2018_CARTEPARADE_H


#include "Joueur.h"
#include "CarteAction.h"

class CarteParade : public CarteAction {

public:

    CarteParade(TypeAttaque);

    virtual bool applicable(const Joueur &) const;
    virtual void appliquer(Joueur &) const;
    virtual int getValeur(const Joueur &) const;
};


#endif //INC_2018_CARTEPARADE_H
