#ifndef AGENDA_H
#define AGENDA_H

#include "Tableau.h"


class Agenda
{
    private:
        Tableau tableau;

    public:
        Agenda(int taille);
        Agenda(const Agenda& copie);
        void concat(Agenda& a1);
        void ajouter(string nom, string numTel);
        void supprimer(string nom, string numTel);
        void supprimer(string nom);
        void afficher();


};

#endif // AGENDA_H
