#include <iostream>
#include "Agenda.h"

using namespace std;

int main()
{
    // Test de Entree
    Entree e1 ("Oui", "0606060606");
    e1.afficher();

    //Test de Tableau
    int taille;
    cout << "Entrez la taille du Tableau t1 : ";
    cin >> taille;
    Tableau t1 (taille);
    cout << endl << "Affichage de t1 vide" << endl;
    t1.afficher();
    t1.ajouter("Monsieur", "0102030405");
    t1.ajouter("Madame", "0123451789");
    t1.ajouter("Oui", "0153456789");
    t1.ajouter("Non", "1123456789");
    t1.ajouter("PeutEtre", "2123456789");
    t1.ajouter("JeNeSaisPas", "3123456789");
    t1.ajouter("Possible", "4123456789");
    t1.ajouter("Impossible", "5123456789");
    t1.ajouter("Madame", "6123456789");
    t1.ajouter("Monsieur", "7123456789");
    t1.ajouter("Mhhhhh", "8123456789");
    cout << endl << "Affichage de t1 apres ajout d'Entree" << endl;
    t1.afficher();
    t1.supprimer("Impossible", "5123456789");
    t1.supprimer("Monsieur");
    cout << endl << "Affichage de t1 apres suppression d'Entree" << endl;
    t1.afficher();
    cout << "Nombre d'elements : " << t1.getNbelem() << endl;
    cout << "Taille du Tableau : " << t1.getTaille() << endl;
    Tableau t2 = t1;
    cout << endl << "Affichage de t2 - copie de t1" << endl;
    t2.afficher();

    //Test d'Agenda
    cout << endl << "Entrez la taille de l'Agenda a1: ";
    cin >> taille;
    Agenda a1 (taille);
    a1.ajouter("A", "0");
    a1.ajouter("B", "1");
    a1.ajouter("C", "2");
    a1.ajouter("D", "3");
    a1.ajouter("E", "4");
    a1.ajouter("F", "5");
    a1.ajouter("G", "6");
    a1.ajouter("H", "7");
    a1.ajouter("I", "8");
    a1.ajouter("J", "9");
    cout << "Affichage de a1 avec Entree" << endl;
    a1.afficher();
    a1.supprimer("E");
    a1.supprimer("G", "6");
    cout << endl << "Affichage de a1 apres suppression d'Entree" << endl;
    a1.afficher();
    Agenda a2 (a1);
    cout << endl << "Affichage de a2 - copie de a1" << endl;
    a2.afficher();
    a1.concat(a2);
    cout << endl << "Affichage de a1 concatene a a2" << endl;
    a1.afficher();

    return 0;
}
