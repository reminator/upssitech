#ifndef TABLEAU_H
#define TABLEAU_H
#include "Entree.h"


class Tableau
{
    public:
        Tableau(int taille);
        ~Tableau();
        Tableau(const Tableau& copie);
        void afficher();
        void ajouter(string nom, string numTel);
        void supprimer (string nom, string numTel);
        void supprimer (string nom);
        int getTaille();
        int getNbelem();
        Entree getEntree(int i);
        Tableau & operator=(const Tableau & t2);

    private:
        int taille;
        int nbelem;
        Entree *ptrEntree;
};

#endif // TABLEAU_H
