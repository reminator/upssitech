#include "Agenda.h"

Agenda::Agenda(int taille): tableau(taille){}

Agenda::Agenda(const Agenda& copie): tableau(copie.tableau){}

void Agenda::concat(Agenda& a)
{
    int newTaille = this->tableau.getTaille() + a.tableau.getTaille();
    Tableau newTab (newTaille);
    for (int i = 0; i < this->tableau.getNbelem(); i++) {
        newTab.ajouter(this->tableau.getEntree(i).nom, this->tableau.getEntree(i).numTel);
    }
    for (int j = 0; j < a.tableau.getNbelem(); j++) {
        newTab.ajouter(a.tableau.getEntree(j).nom, a.tableau.getEntree(j).numTel);
    }
    this->tableau = newTab;
}

void Agenda::ajouter(string nom, string numTel)
{
    this->tableau.ajouter(nom, numTel);
}

void Agenda::supprimer(string nom, string numTel)
{
    this->tableau.supprimer(nom, numTel);
}

void Agenda::supprimer(string nom)
{
    this->tableau.supprimer(nom);
}

void Agenda::afficher()
{
    this->tableau.afficher();
}
