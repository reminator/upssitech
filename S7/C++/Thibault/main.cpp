#include <iostream>
#include "Agenda.h"

using namespace std;

int main()
{
    //Creation d'un Agenda
    int taille;
    cout << endl << "Entrez la taille de l'Agenda a1 et a2: ";
    cin >> taille;
    Agenda a1 (taille);
    Agenda a2 (taille);
    a1.ajouter("A", "0");
    a1.ajouter("B", "1");
    a1.ajouter("C", "2");
    a1.ajouter("D", "3");
    a1.ajouter("E", "4");

    a2.ajouter("F", "5");
    a2.ajouter("G", "6");
    a2.ajouter("H", "7");
    a2.ajouter("I", "8");
    a2.ajouter("J", "9");

    // Test de l'operateur <<
    cout << endl << a1;

    // Test de l'operateur += (entree)
    Entree e1 ("Oui", "0606060606");
    a1 += e1;
    cout << endl << a1;

    // Test de l'operateur =
    Agenda a3(5);
    a3 = a1;
    cout << endl << a3;

    // Test de l'operateur +
    Agenda a4 = a1 + a2;
    cout << endl << a4;

    /** Pas encore fini
    // test de l'operateur += (agenda)
    Agenda a5(3);
    a5 += a1;
    cout << endl <<  a5;
     */

    return 0;
}
