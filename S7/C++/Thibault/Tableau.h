#ifndef TABLEAU_H
#define TABLEAU_H
#include "Entree.h"


class Tableau
{
    public:
        Tableau(int taille=100);
        ~Tableau();
        Tableau(const Tableau& copie);
        void afficher();
        void ajouter(string nom, string numTel);
        void supprimer (string nom, string numTel);
        void supprimer (string nom);
        int getTaille();
        int getNbelem();
        Entree getEntree(int i);
        friend ostream& operator<<(ostream& out, Tableau& t1);
        void operator+=(Entree e1);
        void ajouter(Entree e1);
        Tableau & operator=(const Tableau & t2);

    private:
        int taille;
        int nbelem;
        Entree *ptrEntree;
};

#endif // TABLEAU_H
