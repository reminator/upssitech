#ifndef ENTREE_H
#define ENTREE_H

#include <string>
#include <iostream>

using namespace std;

class Entree
{
    public:
        Entree(string nom = "", string numTel = "");
        void afficher();
        friend ostream& operator<<(ostream& out, Entree& e1);

    private:
        string nom;
        string numTel;
        friend class Tableau;
        friend class Agenda;

};

#endif // ENTREE_H
