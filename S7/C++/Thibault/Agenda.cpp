#include "Agenda.h"

Agenda::Agenda(int taille): tableau(taille){}

Agenda::Agenda(const Agenda& copie): tableau(copie.tableau){}

void Agenda::concat(Agenda& a)
{
    int newTaille = this->tableau.getTaille() + a.tableau.getTaille();
    Tableau newTab (newTaille);
    for (int i = 0; i < this->tableau.getNbelem(); i++) {
        newTab.ajouter(this->tableau.getEntree(i).nom, this->tableau.getEntree(i).numTel);
    }
    for (int j = 0; j < a.tableau.getNbelem(); j++) {
        newTab.ajouter(a.tableau.getEntree(j).nom, a.tableau.getEntree(j).numTel);
    }
    this->tableau = newTab;
}

Agenda concat(Agenda a1, Agenda a2)
{
    int newTaille = a1.getTaille() + a2.getTaille();
    Agenda out (newTaille);
    for (int i = 0; i < a1.getNbelem(); i++) {
        out.ajouter(a1.getEntree(i));
    }
    for (int j = 0; j < a2.getNbelem(); j++) {
        out.ajouter(a2.getEntree(j));
    }
    return out;
}

void Agenda::ajouter(string nom, string numTel)
{
    this->tableau.ajouter(nom, numTel);
}

void Agenda::ajouter(Entree e1)
{
    this->tableau.ajouter(e1);
}

void Agenda::supprimer(string nom, string numTel)
{
    this->tableau.supprimer(nom, numTel);
}

void Agenda::supprimer(string nom)
{
    this->tableau.supprimer(nom);
}

void Agenda::afficher()
{
    this->tableau.afficher();
}

ostream& operator<< (ostream& out, Agenda& a1)
{
    out << a1.tableau;
    return out;
}

void Agenda::operator+=(Entree e1) {
    this->tableau += e1;
}

Agenda operator+(const Agenda & a1, const Agenda & a2)
{
    return concat(a1, a2);
}

const int Agenda::getTaille() {
    return this->tableau.getTaille();
}

const int Agenda::getNbelem() {
    return this->tableau.getNbelem();
}

const Entree Agenda::getEntree(int i) {
    return this->tableau.getEntree(i);
}

Agenda & Agenda::operator=(const Agenda & a2)
{
    if (this != &a2) {
        this->tableau = a2.tableau;
    }
    return *this;
}

void Agenda::operator+=(Agenda a2) {
    concat(a2);
}