#ifndef AGENDA_H
#define AGENDA_H

#include "Tableau.h"


class Agenda
{
    private:
        Tableau tableau;

    public:
        Agenda(int taille=100);
        Agenda(const Agenda& copie);
        void concat(Agenda& a1);
        void ajouter(string nom, string numTel);
        void ajouter(Entree e1);
        void supprimer(string nom, string numTel);
        void supprimer(string nom);
        void afficher();
        friend ostream& operator<<(ostream& out, Agenda& a1);
        void operator+=(Entree e1);
        friend Agenda operator+(const Agenda & a1, const Agenda & a2);
        friend Agenda concat(Agenda& a1, Agenda& a2);
        const int getNbelem();
        const int getTaille();
        const Entree getEntree(int i);
        Agenda & operator=(const Agenda & t2);
        void operator+=(Agenda a2);
};

#endif // AGENDA_H
