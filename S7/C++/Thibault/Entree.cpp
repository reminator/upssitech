#include "Entree.h"


Entree::Entree(string nom, string numTel)
{
    this->nom = nom;
    this->numTel = numTel;
}

void Entree::afficher()
{
    cout << "Nom : " << this->nom << " Numero de tel : " << this->numTel << endl;
}

ostream& operator<< (ostream& out, Entree& e1)
{
    out << "Nom : " << e1.nom << " ; Numero de tel : " << e1.numTel << endl;
    return out;
}

