#include "Tableau.h"
#include "Entree.h"

Tableau::Tableau(int taille)
{
    this->taille = taille;
    this->nbelem = 0;
    this->ptrEntree = new Entree[taille];
}

Tableau::Tableau(const Tableau& copie)
{
    this->taille = copie.taille;
    this->nbelem = copie.nbelem;
    this->ptrEntree = new Entree[copie.taille];
    for (int i = 0; i < copie.nbelem; i++) {
        this->ptrEntree[i] = copie.ptrEntree[i];
    }
}

Tableau::~Tableau()
{
    delete [] ptrEntree;
}

void Tableau::afficher()
{
    for (int i = 0; i < this->nbelem; i++) {
        this->ptrEntree[i].afficher();
    }
}

void Tableau::ajouter(string nom, string numTel)
{
    if (this->nbelem < taille) {
        this->ptrEntree[nbelem] = Entree (nom, numTel);
        this->nbelem ++;
    }
    else {
        cout << "Tableau plein, ajout impossible" << endl;
    }
}

void Tableau::ajouter(Entree e1)
{
    if (this->nbelem < taille) {
        this->ptrEntree[nbelem] = e1;
        this->nbelem ++;
    }
    else {
        cout << "Tableau plein, ajout impossible" << endl;
    }
}

void Tableau::supprimer(string nom, string numTel)
{
    for (int i = 0; i < this->nbelem; i++) {
        if (this->ptrEntree[i].nom == nom && this->ptrEntree[i].numTel == numTel) {
            this->ptrEntree[i] = this->ptrEntree[nbelem-1];
            this->nbelem--;
            break;
        }
    }
}

void Tableau::supprimer(string nom)
{
    for (int i = 0; i < this->nbelem; i++) {
        if (this->ptrEntree[i].nom == nom) {
            this->ptrEntree[i] = this->ptrEntree[nbelem-1];
            i--;
            this->nbelem--;
        }
    }
}

int Tableau::getTaille() {
    return this->taille;
}

int Tableau::getNbelem()
{
    return this->nbelem;
}

Entree Tableau::getEntree(int i)
{
    return this->ptrEntree[i];
}

ostream& operator<< (ostream& out, Tableau& t1)
{
    Entree e1;
    for (int i = 0; i < t1.nbelem; i++) {
        Entree e1 = t1.getEntree(i);
        out << e1;
    }
    return out;
}

void Tableau::operator+=(Entree e1)
{
    ajouter(e1);
}

Tableau & Tableau::operator=(const Tableau & t2)
{
    if (this != &t2) {
        delete [] ptrEntree;
        taille = t2.taille; nbelem = t2.nbelem; ptrEntree=new Entree [taille];
        for (int i=0;i<taille;i++)
            ptrEntree[i] = t2.ptrEntree[i];
    }
    return *this;
}