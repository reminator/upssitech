#include "Tableau.h"
#include <iostream>
using namespace std;

Tableau::Tableau(int tailleMax) : tailleMax(tailleMax), nbElements(0){
    this->entrees = new Entree[tailleMax];
}

Tableau::Tableau(const Tableau & copie) {
    this->entrees = new Entree[copie.tailleMax];
    for (int i = 0; i < copie.nbElements; i++) {
        this->entrees[i] = copie.entrees[i];
    }
    this->nbElements = copie.nbElements;
    this->tailleMax = copie.tailleMax;
}

Tableau::~Tableau() {
    cout << "Destruction de ";
    this->afficher();
    delete [] entrees;
}

void Tableau::ajouter(string nom, string numero){
    if (this->tailleMax <= this->nbElements) return;
    this->entrees[this->nbElements] = Entree(nom, numero);
    this->nbElements++;
}

void Tableau::afficher() const{
    cout << "[";
    for (int i = 0; i < this->nbElements; i++) {
        if (i == this->nbElements - 1)
            this->entrees[i].afficher();
        else {
            this->entrees[i].afficher();
            cout << ", ";
        }
    }
    cout << "]" << endl;
}

void Tableau::supprimer(string nom, string numero) {
    // Mes fonctions supprimer() font en sorte de ne pas changer l'ordre du tableau.
    int indice = 0;
    for (int i = 0; i < this->nbElements; i++) {
        if (this->entrees[i].nom != nom || this->entrees[i].numero != numero) {
            this->entrees[indice] = this->entrees[i];
            indice++;
        }
    }
    this->nbElements = indice;
}

void Tableau::supprimer(string nom) {
    // Mes fonctions supprimer() font en sorte de ne pas changer l'ordre du tableau.
    int indice = 0;
    for (int i = 0; i < this->nbElements; i++) {
        if (this->entrees[i].nom != nom) {
            this->entrees[indice] = this->entrees[i];
            indice++;
        }
    }
    this->nbElements = indice;
}

Tableau & Tableau::operator=(const Tableau & copie) {
    if (this != &copie) {
        delete [] this->entrees;
        this->entrees = new Entree[copie.tailleMax];
        for (int i = 0; i < copie.nbElements; i++) {
            this->entrees[i] = copie.entrees[i];
        }
        this->tailleMax = copie.tailleMax;
        this->nbElements = copie.nbElements;
    }
    return *this;
}