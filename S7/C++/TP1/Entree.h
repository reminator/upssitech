#include <iostream>
using namespace std;

#ifndef ENTREE_H
#define ENTREE_H

class Entree {
    friend class Tableau;
    friend class Agenda;
    
    string nom;
    string numero;

    public:
    Entree();
    Entree(string nom, string numero);

    void afficher() const;

    string getNom() { return this->nom; }
    string getNumero() { return this->numero; }
};

#endif