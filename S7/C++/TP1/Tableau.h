#ifndef TABLEAU_H
#define TABLEAU_H

#include "Entree.h"

class Tableau {

    Entree * entrees;
    int nbElements = 0;
    int tailleMax;

    public:
    Tableau(int tailleMax = 10);
    ~Tableau();
    Tableau(const Tableau & copie);

    void afficher() const;
    
    Tableau& operator=(const Tableau & copie);

    void ajouter(string nom, string numero);

    void supprimer(string nom, string numero);

    void supprimer(string nom);
    
    int getNbElements() const { return this->nbElements; }
    int getTailleMax() const { return this->tailleMax; }
    Entree & getEntree(int indice) const { return this->entrees[indice]; }
};

#endif