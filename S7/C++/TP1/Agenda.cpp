#include "Agenda.h"

Agenda::Agenda() {
    //this->tableau = Tableau();
    this->tableau = new Tableau();
}

Agenda::Agenda(const Tableau & tableau) {
    //this->tableau = tableau;
    this->tableau = new Tableau(tableau);
}

Agenda::Agenda(const Agenda & copie) {
    // this->tableau = copie.table
    this->tableau = new Tableau();
    (*this->tableau) = (*copie.tableau);
}

void Agenda::concat(const Agenda & agenda) {
    /*
    for (int i = 0; i < agenda.tableau.getNbElements(); i++) {
        if (this->tableau.getTailleMax() <= this->tableau.getNbElements()) return;
        Entree e = agenda.tableau.getEntree(i);
        this->tableau.ajouter(e.nom, e.numero);
    }
    */
    for (int i = 0; i < agenda.tableau->getNbElements(); i++) {
        if (this->tableau->getTailleMax() <= this->tableau->getNbElements()) return;
        Entree e = agenda.tableau->getEntree(i);
        this->tableau->ajouter(e.nom, e.numero);
    }
}

void Agenda::ajouter(string nom, string numero) {
    //this->tableau.ajouter(nom, numero);
    this->tableau->ajouter(nom, numero);
}

void Agenda::supprimer(string nom, string numero) {
    //this->tableau.supprimer(nom, numero);
    this->tableau->supprimer(nom, numero);
}

void Agenda::supprimer(string nom) {
    //this->tableau.supprimer(nom);
    this->tableau->supprimer(nom);
}

void Agenda::afficher() {
    //this->tableau.afficher();
    this->tableau->afficher();
}

Agenda::~Agenda() {
    delete this->tableau;
}

Agenda & Agenda::operator=(const Agenda & copie) {
    if (this != &copie) {
        delete this->tableau;
        this->tableau = new Tableau(copie.tableau->getTailleMax());
        (*this->tableau) = (*copie.tableau);
    }
    return *this;
}