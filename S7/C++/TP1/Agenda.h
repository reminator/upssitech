#ifndef AGENDA_H
#define AGENDA_H

#include "Tableau.h"

class Agenda {
    // En commentaire, le TP sans le pointeur. Pour voir les changements entre l'ancienne et la nouvelle version.
    //Tableau tableau;
    Tableau * tableau;

    public:
    Agenda();
    Agenda(const Tableau & tableau);
    Agenda(const Agenda & copie);
    // Il faut ajouter le desctructeur quand il y a un pointeur.
    ~Agenda();

    void concat(const Agenda & agenda);
    void ajouter(string nom, string numero);
    void supprimer(string nom, string numero);
    void supprimer(string nom);
    void afficher();
    // Il faut ajouter l'opérateur = quand on a un pointeur. Pour éviter de copier la référence mais plutôt la valeur.
    Agenda& operator=(const Agenda & copie);

};

#endif