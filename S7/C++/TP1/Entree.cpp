#include "Entree.h"
#include <iostream>
using namespace std;

Entree::Entree() : nom("Pas de nom"), numero("Pas de numéro de téléphone") {
}

Entree::Entree(string nom, string numero) : nom(nom), numero(numero) {
}

void Entree::afficher() const {
    cout << "Entree{Nom : " << nom << ", Numéro de téléphone : " << numero << "}";
}