#include <iostream>
using namespace std;

#include "Agenda.h"

int main(void) {
    string s = "*******************************************************************************************";
    cout << endl << s << endl << "Bienvenue sur le programme principal de Rémi Laborie pour le TP1 !" << endl << s << endl << endl;

    Entree e1 = Entree("Bordes", "06 13 46 79 55");
    Entree e2 = Entree();
    Entree e3 = Entree(e2);

    e1.afficher();
    cout << endl;
    e2.afficher();
    cout << endl;
    e3.afficher();
    cout << endl;

    e3 = e1;
    cout << endl;

    e1.afficher();
    cout << endl;
    e2.afficher();
    cout << endl;
    e3.afficher();
    cout << endl;

    e1 = e2;
    cout << endl;

    e1.afficher();
    cout << endl;
    e2.afficher();
    cout << endl;
    e3.afficher();
    cout << endl;


    Agenda agenda1 = Agenda();
    Agenda agenda2 = Agenda();
    Agenda agenda3 = Agenda(agenda2);

    agenda1.ajouter(e1.getNom(), e1.getNumero());
    agenda1.ajouter(e3.getNom(), e3.getNumero());
    agenda2.ajouter("Laborie", "06 95 18 73 55");

    agenda1.concat(agenda2);
    cout << endl;
    agenda1.afficher();
    agenda2.afficher();

    agenda1.supprimer("Bordes");
    cout << "Suppression de Dorian" << endl;
    agenda1.afficher();

    cout << "Ajout d'un 2eme Laborie" << endl;
    agenda1.ajouter("Laborie", "06 76 76 48 08");
    agenda1.afficher();

    cout << "Suppression des Laborie" << endl;
    agenda1.supprimer("Laborie");
    agenda1.afficher();
}