#include "Base.h"

class Point : public Base {

    int x;
    int y;

    public:
    Point(int x, int y);
    Point(const Point & copie);
};