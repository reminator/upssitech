#include "Compte.h"
#include <iostream>
using namespace std;

int Compte::depot(float montant) {
    this->solde += montant;
    return 0;
}

int Compte::retrait(float montant) {
    if (solde < montant) return 1;
    this->solde -= montant;
    return 0;
}

void Compte::afficherSolde() {
    cout << "Solde : " << this->solde << endl;
}

int Compte::virer(Compte & c, float montant) {
    int erreur = this->retrait(montant);
    if (erreur) return erreur;
    c.depot(montant);
    return 0;
}


// S'il y avait une allocation dans le constructeur :
Compte::~Compte() {
    cout << "Destruction de ";
    this->afficherSolde();
    //delete solde; // Si solde est une pointeur
}
