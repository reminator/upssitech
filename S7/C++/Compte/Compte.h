class Compte{
    float solde;

    public:
    int depot(float montant);
    int retrait(float montant);
    void afficherSolde();
    float getSolde(){return this->solde;}
    int virer(Compte & c, float montant);

    Compte(int solde) {
        this->solde = solde;
    }
    Compte() {
        this->solde = 0;
    }
    ~Compte();

    /* Crée par C++ si pas de pointeurs dans les attributs
    Compte(Compte & copie) {
        this->solde = copie.solde;
    }
    */
};