#include "Compte.h"
#include <iostream>
using namespace std;

int main() {
    cout << "test" << endl;
    Compte c;
    c.afficherSolde();
    c.depot(100);
    c.afficherSolde();
    c.retrait(20);
    c.afficherSolde();
    Compte c2(c);

    c.virer(c2, 20);
    c.afficherSolde();
    c2.afficherSolde();

    // Création d'un tableau de 10 Compte avec le constructeur par défault
    Compte * comptes = new Compte[10];

    delete [] comptes;
}