//
// Created by dbord on 06/11/2021.
//

#ifndef TPAGENDA_AGENDA_H
#define TPAGENDA_AGENDA_H


#include <iostream>
using namespace std;

class Agenda{
protected:
    Tableau *tab;
public:
    Agenda &operator=(Agenda & copie);
    Agenda(int taille);
    Agenda(Agenda& copie);
    void concat(Agenda &agd);
    void ajouter(string nom, string tel);
    void supprimer(string nom, string tel);
    void supprimer(string nom);
    void affichage();
    int getnbelem();
    Tableau gettab();
    friend ostream& operator<<(ostream& o, Agenda& copie);
    Agenda & operator+(const Agenda & agd);
    friend void operator+=(Agenda& agd, Entree& ent);
    Agenda & operator+=(Agenda& agd);
    Entree operator[](string nom);
    Agenda& operator-=(string nom);
    bool operator==(Agenda agd);
};

#endif //TPAGENDA_AGENDA_H
