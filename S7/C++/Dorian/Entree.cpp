//
// Created by dbord on 06/11/2021.
//

#include "Entree.h"

Entree::Entree(string n, string t) {
    this->nom = n;
    this->numtel = t;
}

Entree::Entree() {
    this->nom = "pas de nom";
    this->numtel = "pas de numero";
}

void Entree::affichage() {
    cout << "Nom : " << nom << " Numero : " << numtel << endl;
}

ostream & operator<<(ostream & o, Entree & copie) {
    o << "Nom : " << copie.nom << " Numero : " << copie.numtel << endl;
    return o;
}

string Entree::getnom() {return this->nom;}
string Entree::getnum() {return this->numtel;}