//
// Created by dbord on 06/11/2021.
//

#include "Tableau.h"
#include "Entree.h"
#include "Agenda.h"

Agenda::Agenda(int taille) {
    this->tab = new Tableau(taille);
}

Agenda::Agenda(Agenda &copie) {
    this->tab = new Tableau(copie.tab->gettailletotale());
    *this->tab = *copie.tab;
}

Agenda & Agenda::operator=(Agenda & copie) {
    if (this != &copie) {
        delete this->tab;
        this->tab = new Tableau(copie.tab->gettailletotale());
        *this->tab = *copie.tab;
    }
    return *this;
}

ostream & operator<<(ostream & o, Agenda & copie) {
    o << (*copie.tab);
    return o;
}

void Agenda::ajouter(string n, string t){
    this->tab->ajouter(n,t);
}

void Agenda::supprimer(string n, string t) {
    this->tab->supprimer(n,t);
}

void Agenda::supprimer(string n) {
    this->tab->supprimer(n);
}

void Agenda::affichage() {
    this->tab->affichage();
}

void Agenda::concat(Agenda &agd) {
    int j;
    string nom;
    string tel;
    //Tableau *tabl = &agd->tab;
    for(j=0; j < agd.getnbelem(); j++){
        nom = agd.tab->getentree(j).getnom();
        tel = agd.tab->getentree(j).getnum();
        this->ajouter(nom, tel);
    }
}

int Agenda::getnbelem(){return this->tab->getnbelem();}

Agenda & Agenda::operator+(const Agenda & agd){
    Agenda *retour = new Agenda(this->tab->gettailletotale() + agd.tab->gettailletotale());
    for(int i = 0; i < this->tab->getnbelem(); i++){
        retour->ajouter(this->tab->getentree(i).getnom(), this->tab->getentree(i).getnum());
    }
    for(int i = 0; i < agd.tab->getnbelem(); i++){
        retour->ajouter(agd.tab->getentree(i).getnom(), agd.tab->getentree(i).getnum());
    }
    return *retour;
}

void operator+=(Agenda& agd, Entree& ent) {
    agd.ajouter(ent.getnom(), ent.getnum());
}

Agenda &Agenda::operator+=(Agenda &agd) {
    this->concat(agd);
    return *this;
}

Entree Agenda::operator[](string nom) {
    if(this->tab != NULL){
        for(int i = 0; i < this->getnbelem(); i++) {
            if(nom == this->tab->getentree(i).getnom()){
                return this->tab->getentree(i);
            }
        }
    }
    throw new exception;
}

Agenda &Agenda::operator-=(string nom) {
    this->supprimer(nom);
    return *this;
}

bool Agenda::operator==(Agenda agd) {
    return *this->tab == *agd.tab;
}
