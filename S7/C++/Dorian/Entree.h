//
// Created by dbord on 06/11/2021.
//

#ifndef TPAGENDA_ENTREE_H
#define TPAGENDA_ENTREE_H

#include <iostream>
using namespace std;

class Entree {
    string nom;
    string numtel;
public:
    Entree();
    Entree(string n, string t);
    void affichage();
    string getnom();
    string getnum();
    friend ostream& operator<<(ostream&, Entree&);
};

#endif //TPAGENDA_ENTREE_H