//
// Created by dbord on 09/11/2021.
//

#ifndef TPAGENDA_TABLEAU_H
#define TPAGENDA_TABLEAU_H

#include "Entree.h"
#include <iostream>
using namespace std;

class Tableau {
    int tailletotale;
    int nbelem;
    Entree* entrees;
public:
    Tableau &operator=(Tableau & copie);
    Tableau(int taille);
    ~Tableau();
    Tableau(Tableau& copie);
    void affichage();
    void ajouter(string nom, string tel);
    void supprimer(string nom, string tel);
    void supprimer(string nom);
    int getnbelem();
    int gettailletotale();
    Entree getentree(int i);
    friend ostream& operator<<(ostream&, Tableau&);
    Tableau& operator=(const Tableau& tab);
    bool operator==(Tableau tab);
};

#endif //TPAGENDA_TABLEAU_H