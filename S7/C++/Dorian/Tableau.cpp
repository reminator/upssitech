//
// Created by dbord on 09/11/2021.
//

#include "Tableau.h"
#include "Entree.h"

Tableau::Tableau(int taille) {
    this->tailletotale = taille;
    this->nbelem = 0;
    this->entrees = new Entree[tailletotale];
}

Tableau::~Tableau() {
    delete [] entrees;
}

Tableau::Tableau(Tableau &copie) {
    this->tailletotale = copie.tailletotale;
    this->nbelem = copie.nbelem;
    this->entrees = new Entree[copie.tailletotale];
    for (int i = 0; i < copie.nbelem; i++) {
        this->entrees[i] = copie.entrees[i];
    }
}

Tableau & Tableau::operator=(Tableau & copie) {
    if (this != &copie) {
        delete [] this->entrees;
        this->entrees = new Entree[copie.tailletotale];
        for (int i = 0; i < copie.nbelem; i++) {
            this->entrees[i] = copie.entrees[i];
        }
        this->tailletotale = copie.tailletotale;
        this->nbelem = copie.nbelem;
    }
    return *this;
}

void Tableau::ajouter(string nom, string tel) {
    if(nbelem < tailletotale){
        Entree e = Entree(nom, tel);
        this->entrees[nbelem] = e;
        nbelem++;
    }
    else{
        cout << "Tableau plein" << endl;
    }
}

void Tableau::supprimer(string nom, string tel) {
    int i;
    for(i=0; i<nbelem; i++){
        if(nom == entrees[i].getnom() && tel == entrees[i].getnum()){
            entrees[i] = entrees[nbelem-1];
            nbelem--;
        }
    }
}

void Tableau::supprimer(string nom) {
    int i;
    for(i=0; i<nbelem; i++){
        if(nom == entrees[i].getnom()){
            entrees[i] = entrees[nbelem-1];
            nbelem--;
        }
    }
}

void Tableau::affichage() {
    for(int i = 0; i<this->nbelem ; i++){
        entrees[i].affichage();
    }
}

ostream & operator<<(ostream & o, Tableau & copie) {
    for(int i = 0; i<copie.nbelem ; i++){
        o << copie.entrees[i];
    }
    return o;
}

int Tableau::getnbelem() {return this->nbelem;}
int Tableau::gettailletotale() {return this->tailletotale;}
Entree Tableau::getentree(int i){return this->entrees[i];}

Tableau &Tableau::operator=(const Tableau &tab) {
    if(this != &tab){
        if(this->entrees != NULL){
            delete [] this->entrees;
        }
        this->entrees = new Entree[tab.tailletotale];
        this->nbelem = tab.nbelem;
        this->tailletotale = tab.tailletotale;
        for(int i = 0; i < tab.nbelem; i++){
            this->entrees[i] = tab.entrees[i];
        }
    }
    return *this;
}

bool Tableau::operator==(Tableau tab) {
    bool res = false;
    if(this->getnbelem() != tab.nbelem){
        return false;
    }
    for(int i = 0; i < this->getnbelem(); i++){
        res = false;
        for(int j = 0; j < tab.nbelem; j++){
            if(this->getentree(i).getnom() == tab.getentree(j).getnom() && this->getentree(i).getnum() == tab.getentree(j).getnum()){
                res = true;
            }
        }
        if(res == false){
            return false;
        }
    }
    return true;
}
