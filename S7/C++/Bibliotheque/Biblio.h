#include <iostream>
using namespace std;

#ifndef BIBLIO_H
#define BIBLIO_H

// Déclaration diférée pour éviter d'include le .h dans le .h
class Livre;

class Biblio {

    Livre * livres;
    int tailleMax;
    int nbElements;

    public:
    Biblio(int tailleMax);
    Biblio(const Biblio & copie);
    ~Biblio();
    Biblio & operator=(const Biblio & copie);
    // Lecture et écriture
    Livre & operator[](int i);
    // Lecture seulement
    Livre operator[](int i) const;
    friend ostream & operator<<(ostream &, Biblio &);
    
};

#endif