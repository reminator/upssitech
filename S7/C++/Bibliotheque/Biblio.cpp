#include "Biblio.h"
#include "Livre.h"

Biblio::Biblio(int tailleMax) : tailleMax(tailleMax), nbElements(0) {
    this->livres = new Livre[tailleMax];
}

Biblio::Biblio(const Biblio & copie) {
    this->tailleMax = copie.tailleMax;
    this->nbElements = copie.nbElements;

    this->livres = new Livre[copie.tailleMax];
    for (int i = 0; i < copie.nbElements; i++) {
        this->livres[i] = copie.livres[i];
    }
}

Biblio::~Biblio() {
    // Déstruction des pointeurs
    delete [] this->livres;
}

Biblio & Biblio::operator=(const Biblio & copie) {
    // On vérifie que les adresses des objets sont différentes
    if (this != &copie) {
        delete [] this->livres;
        this->livres = new Livre[copie.tailleMax];
        for (int i = 0; i < copie.nbElements; i++) {
            this->livres[i] = copie.livres[i];
        }
        this->tailleMax = copie.tailleMax;
        this->nbElements = copie.nbElements;
    }
    return *this;
}

ostream & operator<<(ostream& out, Biblio& b) {
    out << b.tailleMax << endl;
    out << b.nbElements << endl;
    for (int i = 0; i < b.nbElements; i++) {
        out << b.livres[i];
    }
    return out;
}

// Vérifier la définition de ces fonctions
Livre & Biblio::operator[](int i) {
    return this->livres[i];
}

Livre Biblio::operator[](int i) const {
    return this->livres[i];
}