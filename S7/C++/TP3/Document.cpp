#include "Document.h"

Document::Document() : titre(*new string("Pas de titre") ) {
    this->auteur = "Pas d'auteur";
    this->resume = new string("Pas de résumé");
}

Document::Document(const string & titre, const string auteur, const string * resume) : titre(*new string(titre)) {
    this->auteur = auteur;
    this->resume = new string(*resume);
}

Document::Document(const Document & copie) : titre(*new string(copie.titre)){
    this->auteur = copie.auteur;
    this->resume = new string(*copie.resume);
}

ostream & operator<<(ostream & out, const Document & doc) {
    out << "Document{Titre : " << doc.titre << ", Auteur : " << doc.auteur << ", Résumé : " << *doc.resume << "}";
    return out;
}

Document & Document::operator=(const Document & copie) {
    if (this != &copie) {
        this->titre = *new string(copie.titre);
        this->auteur = copie.auteur;
        delete this->resume;
        this->resume = new string(*copie.resume);
    }
    return *this;
}

Document & Document::clone() {
    return *new Document(*this);
}

Document::~Document() {
    cout << "Destruction de " << *this << endl;
    delete this->resume;
}