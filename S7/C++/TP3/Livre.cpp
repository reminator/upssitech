#include "Livre.h"

Livre::Livre() : editeur("Pas d'éditeur"), parution("Pas de date de parution") {}

Livre::Livre(const string & titre, const string auteur, const string * resume, const string editeur, const string parution) : 
    Document(titre, auteur, resume),
    editeur(editeur),
    parution(parution) {}

Livre::Livre(const Livre & copie) :
    Document(copie),
    editeur(copie.editeur),
    parution(copie.parution) {}

Livre & Livre::clone() const {
    return *new Livre(*this);
}

ostream & operator<<(ostream & out, const Livre & livre) {
    out << "Livre{Titre : " << livre.titre << ", Auteur : " << livre.auteur << ", Résumé : " << *livre.resume << ", Éditeur : " << livre.editeur << ", Date de parution : " << livre.parution << "}";
    return out;
}

Livre & Livre::operator=(const Livre & copie) {
    if (this != &copie) {
        this->titre = *new string(copie.titre);
        this->auteur = copie.auteur;
        delete this->resume;
        this->resume = new string(*copie.resume);

        this->editeur = copie.editeur;
        this->parution = copie.parution;
    }
    return *this;
}