#include "Document.h"

class Article : public Document {

    string revue;
    string editeur;
    string numero;

    public:
    Article();
    Article(const string & titre, const string auteur, const string * resume, const string revue, const string editeur, const string numero);
    Article(const Article & copie);

    Article & clone() const;
    friend ostream & operator<<(ostream &, const Article &);
    Article & operator=(const Article &);
};