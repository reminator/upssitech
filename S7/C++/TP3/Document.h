#include <iostream>
#include <string>
using namespace std;

#ifndef DOCUMENT_H
#define DOCUMENT_H

class Document {
    
    protected:
    string & titre;
    string auteur;
    string * resume;


    public:
    Document();
    Document(const string & titre, const string auteur, const string * resume);
    Document(const Document & copie);
    ~Document();

    Document & clone();
    friend ostream & operator<<(ostream &, const Document & );
    Document & operator=(const Document &);
};

#endif