#include "Livre.h"
#include "Article.h"

int main(void) {
    string s = "*******************************************************************************************";
    cout << endl << s << endl << "Création et affichage d'un document avec le constructeur par défaut" << endl << s << endl << endl;
    Document d1 = Document();
    cout << d1 << endl;

    cout << endl << s << endl << "Création et affichage d'un document créé avec un paramètre" << endl << s << endl << endl;
    Document d2 = Document(*new string("Titre"), "auteur", new string("resume"));
    cout << d2 << endl;

    cout << endl << s << endl << "Création et affichage d'un document créé par copie" << endl << s << endl << endl;
    Document d3 = Document(d2);
    cout << d3 << endl;

    cout << endl << s << endl << "Récupération et affichage d'un clone" << endl << s << endl << endl;
    Document clone = d3.clone();
    cout << clone << endl;




    
    cout << endl << s << endl << "Création et affichage d'un Livre avec le constructeur par défaut" << endl << s << endl << endl;
    Livre l1 = Livre();
    cout << l1 << endl;

    cout << endl << s << endl << "Création et affichage d'un Livre créé avec un paramètre" << endl << s << endl << endl;
    Livre l2 = Livre(*new string("Titre"), "auteur", new string("resume"), "éditeur", "date de parution");
    cout << l2 << endl;

    cout << endl << s << endl << "Création et affichage d'un Livre créé par copie" << endl << s << endl << endl;
    Livre l3 = Livre(l2);
    cout << l3 << endl;

    cout << endl << s << endl << "Récupération et affichage d'un clone" << endl << s << endl << endl;
    Livre cloneLivre = l3.clone();
    cout << cloneLivre << endl;



    
    cout << endl << s << endl << "Création et affichage d'un Article avec le constructeur par défaut" << endl << s << endl << endl;
    Article a1 = Article();
    cout << a1 << endl;

    cout << endl << s << endl << "Création et affichage d'un Article créé avec un paramètre" << endl << s << endl << endl;
    Article a2 = Article(*new string("Titre"), "auteur", new string("resume"), "Titre de revue", "éditeur", "Numéro d'édition");
    cout << a2 << endl;

    cout << endl << s << endl << "Création et affichage d'un Article créé par copie" << endl << s << endl << endl;
    Article a3(a2);
    cout << a3 << endl;

    cout << endl << s << endl << "Récupération et affichage d'un clone" << endl << s << endl << endl;
    Article cloneArticle = a2.clone();
    cout << cloneArticle << endl;


    
    cout << endl << s << endl << "Test des operator=" << endl << s << endl << endl;
    d1 = d3;
    l1 = l3;
    a1 = a3;

    cout << d1 << endl;
    cout << l1 << endl;
    cout << a1 << endl;
}