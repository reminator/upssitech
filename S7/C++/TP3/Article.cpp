#include "Article.h"

Article::Article() : revue("Pas de titre de revue"), editeur("Pas d'éditeur"), numero("Pas de numéro d'édition") {}

Article::Article(const string & titre, const string auteur, const string * resume, const string revue, const string editeur, const string numero) : 
    Document(titre, auteur, resume),
    revue(revue),
    editeur(editeur),
    numero(numero) {}

Article::Article(const Article & copie) :
    Document(copie),
    revue(copie.revue),
    editeur(copie.editeur),
    numero(copie.numero) {}

Article & Article::clone() const {
    return *new Article(*this);
}

ostream & operator<<(ostream & out, const Article & article) {
    out << "Article{Titre : " << article.titre << ", Auteur : " << article.auteur << ", Résumé : " << *article.resume << ", Titre de la revue : " << article.revue << ", Éditeur : " << article.editeur << ", Numéro de parution : " << article.numero << "}";
    return out;
}

Article & Article::operator=(const Article & copie) {
    if (this != &copie) {
        this->titre = copie.titre;
        this->auteur = copie.auteur;
        delete this->resume;
        this->resume = new string(*copie.resume);

        this->revue = copie.revue;
        this->editeur = copie.editeur;
        this->numero = copie.numero;

    }
    return *this;
}