#include "Entree.h"
#include <iostream>
using namespace std;

Entree::Entree() : nom("Pas de nom"), numero("Pas de numéro de téléphone") {
}

Entree::Entree(string nom, string numero) : nom(nom), numero(numero) {
}

void Entree::afficher() const {
    cout << "Entree{Nom : " << nom << ", Numéro de téléphone : " << numero << "}";
}

ostream & operator<<(ostream& out, Entree& e) {
    out << "Entree{Nom : " << e.nom << ", Numéro de téléphone : " << e.numero << "}";
    return out;
}

bool Entree::operator==(const Entree & e) {
    if (this->nom == e.nom && this->numero == e.numero) return true;
    return false;
}