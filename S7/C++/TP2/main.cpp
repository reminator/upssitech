#include <iostream>
using namespace std;

#include "Agenda.h"

int main(void) {
    string s = "*******************************************************************************************";
    cout << endl << s << endl << "Bienvenue sur le programme principal de Rémi Laborie pour le TP2 !" << endl << s << endl << endl;

    Entree e1 = Entree("Bordes", "06 13 46 79 55");
    Entree e2 = Entree();
    Entree e3 = Entree(e2);

    cout << e1 << endl;
    cout << e2 << endl;
    cout << e3 << endl;

    e3 = e1;
    cout << endl;

    cout << e1 << endl;
    cout << e2 << endl;
    cout << e3 << endl;

    e1 = e2;
    cout << endl;

    cout << e1 << endl;
    cout << e2 << endl;
    cout << e3 << endl;


    Agenda agenda1 = Agenda();
    Agenda agenda2 = Agenda();

    agenda1+=e1;
    agenda1+=e3;
    agenda2+=Entree("Laborie", "06 95 18 73 55");

    agenda1+=agenda2;
    cout << endl;
    cout << agenda1 << endl;
    cout << agenda2 << endl;

    agenda1-="Bordes";
    cout << "Suppression de Dorian" << endl;
    cout << agenda1 << endl;

    cout << "Ajout d'un 2eme Laborie" << endl;
    agenda1+=Entree("Laborie", "06 76 76 48 08");
    cout << agenda1 << endl;
    cout << agenda1["Laborie"] << endl;
    agenda1('L');
    cout << "Laborie"/agenda1 << endl;
    cout << "Labori"/agenda1 << endl;

    cout << "Suppression des Laborie" << endl;
    agenda1-="Laborie";
    cout << agenda1 << endl;
    cout << "test" << endl;

    Agenda a3 = agenda1 + agenda2;
    cout << a3 << endl;

    return 0;
}