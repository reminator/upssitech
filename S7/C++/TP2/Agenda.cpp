#include "Agenda.h"

Agenda::Agenda(int tailleMax) {
    //this->tableau = Tableau();
    this->tableau = new Tableau(tailleMax);
}

Agenda::Agenda(const Tableau & tableau) {
    //this->tableau = tableau;
    this->tableau = new Tableau(tableau);
}

Agenda::Agenda(const Agenda & copie) {
    // this->tableau = copie.table
    this->tableau = new Tableau();
    (*this->tableau) = (*copie.tableau);
}

void Agenda::concat(const Agenda & agenda) {
    /*
    for (int i = 0; i < agenda.tableau.getNbElements(); i++) {
        if (this->tableau.getTailleMax() <= this->tableau.getNbElements()) return;
        Entree e = agenda.tableau.getEntree(i);
        this->tableau.ajouter(e.nom, e.numero);
    }
    */
    for (int i = 0; i < agenda.tableau->getNbElements(); i++) {
        if (this->tableau->getTailleMax() <= this->tableau->getNbElements()) return;
        Entree e = agenda.tableau->getEntree(i);
        this->ajouter(e.nom, e.numero);
    }
}

void Agenda::ajouter(string nom, string numero) {
    //this->tableau.ajouter(nom, numero);
    this->tableau->ajouter(nom, numero);
}

void Agenda::supprimer(string nom, string numero) {
    //this->tableau.supprimer(nom, numero);
    this->tableau->supprimer(nom, numero);
}

void Agenda::supprimer(string nom) {
    //this->tableau.supprimer(nom);
    this->tableau->supprimer(nom);
}

void Agenda::afficher() {
    //this->tableau.afficher();
    this->tableau->afficher();
}

Agenda::~Agenda() {
    delete this->tableau;
}

Agenda & Agenda::operator=(const Agenda & copie) {
    if (this != &copie) {
        delete this->tableau;
        this->tableau = new Tableau(copie.tableau->getTailleMax());
        (*this->tableau) = (*copie.tableau);
    }
    return *this;
}

ostream & operator<<(ostream& out, const Agenda& a) {
    out << "Agenda{" << (*a.tableau) << "}" <<  endl;
    return out;
}

void Agenda::operator+=(const Entree & entree) {
    this->tableau->ajouter(entree.nom, entree.numero);
}

Agenda & Agenda::operator+(const Agenda & a2) const {
    Agenda * a = new Agenda(this->tableau->getTailleMax() + this->tableau->getTailleMax());
    
    for (int i = 0; i < this->tableau->getNbElements(); i++) {
        if (a->tableau->getTailleMax() <= a->tableau->getNbElements()) return *a;
        Entree e = this->tableau->getEntree(i);
        a->tableau->ajouter(e.getNom(), e.getNumero());
    }

    for (int i = 0; i < a2.tableau->getNbElements(); i++) {
        if (a->tableau->getTailleMax() <= a->tableau->getNbElements()) return *a;
        Entree e = a2.tableau->getEntree(i);
        a->tableau->ajouter(e.getNom(), e.getNumero());
    }

    return *a;
}

void Agenda::operator+=(const Agenda & a) {
    this->concat(a);
}

Entree & Agenda::operator[](string nom) const {
    // J'ai défini l'opérateur [] dans Tableau et Entree
    return (*this->tableau)[nom];
}

void Agenda::operator-=(string nom) {
    this->supprimer(nom);
}

bool Agenda::operator==(const Agenda & a) const {
    // J'ai défini l'opérateur == dans Tableau et Entree
    return *this->tableau == *a.tableau;
}

bool operator/(string nom, const Agenda & a) {
    // J'ai défini l'opérateur / dans Tableau
    return nom/(*a.tableau);
}

void Agenda::operator()(char lettre) const {
    // J'ai défini l'opérateur () dans Tableau
    (*this->tableau)(lettre);
}