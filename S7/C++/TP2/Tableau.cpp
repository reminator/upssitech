#include "Tableau.h"
#include <iostream>
using namespace std;

Tableau::Tableau(int tailleMax) : tailleMax(tailleMax), nbElements(0){
    this->entrees = new Entree[tailleMax];
}

Tableau::Tableau(const Tableau & copie) {
    this->entrees = new Entree[copie.tailleMax];
    for (int i = 0; i < copie.nbElements; i++) {
        this->entrees[i] = copie.entrees[i];
    }
    this->nbElements = copie.nbElements;
    this->tailleMax = copie.tailleMax;
}

Tableau::~Tableau() {
    cout << "Destruction de " << *this << endl;
    delete [] entrees;
}

void Tableau::ajouter(string nom, string numero) {
    if (this->tailleMax <= this->nbElements) return;
    this->entrees[this->nbElements] = Entree(nom, numero);
    this->nbElements++;
}

void Tableau::afficher() const{
    cout << "[";
    for (int i = 0; i < this->nbElements; i++) {
        if (i == this->nbElements - 1)
            this->entrees[i].afficher();
        else {
            this->entrees[i].afficher();
            cout << ", ";
        }
    }
    cout << "]";
}

void Tableau::supprimer(string nom, string numero) {
    int indice = 0;
    for (int i = 0; i < this->nbElements; i++) {
        if (this->entrees[i].nom != nom || this->entrees[i].numero != numero) {
            this->entrees[indice] = this->entrees[i];
            indice++;
        }
    }
    this->nbElements = indice;
}

void Tableau::supprimer(string nom) {
    int indice = 0;
    for (int i = 0; i < this->nbElements; i++) {
        if (this->entrees[i].nom != nom) {
            this->entrees[indice] = this->entrees[i];
            indice++;
        }
    }
    this->nbElements = indice;
}

Tableau & Tableau::operator=(const Tableau & copie) {
    if (this != &copie) {
        delete [] this->entrees;
        this->entrees = new Entree[copie.tailleMax];
        for (int i = 0; i < copie.nbElements; i++) {
            this->entrees[i] = copie.entrees[i];
        }
        this->tailleMax = copie.tailleMax;
        this->nbElements = copie.nbElements;
    }
    return *this;
}

ostream & operator<<(ostream& out, Tableau& t) {
    out << "[";
    for (int i = 0; i < t.getNbElements(); i++) {
        if (i == t.getNbElements() - 1)
            out << t.getEntree(i);
        else {
            out << t.getEntree(i) << ", ";
        }
    }
    out << "]";
    return out;
}

bool Tableau::operator==(const Tableau & t) const {
    if (this->tailleMax != t.tailleMax) return false;
    if (this->nbElements != t.nbElements) return false;
    for (int i = 0; i < this->nbElements; i++) {
        if (!(this->entrees[i] == t.entrees[i])) return false;
    }
    return true;
}

bool operator/(string nom, const Tableau & t) {
    for (int i = 0; i < t.nbElements; i++) {
        if (t.entrees[i].getNom() == nom) return true;
    }
    return false;
}

void Tableau::operator()(char lettre) const {
    for (int i=0; i < this->nbElements; i++) {
        if (this->entrees[i].getNom().at(0) == lettre) {
            cout << this->entrees[i] << endl;
        }
    }
}

Entree & Tableau::operator[](string nom) const {
    for (int i = 0; i < this->nbElements; i++) {
        if (this->entrees[i].getNom() == nom) return this->entrees[i];
    }
    // TODO gérer le cas où il n'y a pas le nom, Exception ?
    cout << "Erreur" << endl;
}