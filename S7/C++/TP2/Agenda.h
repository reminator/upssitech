#ifndef AGENDA_H
#define AGENDA_H

#include "Tableau.h"

class Agenda {
    // En commentaire, le TP sans le pointeur. Pour voir les changements entre l'ancienne et la nouvelle version.
    //Tableau tableau;
    Tableau * tableau;

    public:
    Agenda(int tailleMax = 10);
    Agenda(const Tableau & tableau);
    Agenda(const Agenda & copie);
    // Il faut ajouter le desctructeur quand il y a un pointeur.
    ~Agenda();

    void concat(const Agenda & agenda);
    void ajouter(string nom, string numero);
    void supprimer(string nom, string numero);
    void supprimer(string nom);
    void afficher();
    // Il faut ajouter l'opérateur = quand on a un pointeur. Pour éviter de copier la référence mais plutôt la valeur.
    Agenda& operator=(const Agenda & copie);

    // TP2
    friend ostream & operator<<(ostream &, const Agenda &);
    // On ne peut pas mettre 2 paramètres dans un opérateur de classe
    void operator+=(const Entree & entree);
    // Opérateur = déjà fait
    Agenda & operator+(const Agenda & a2) const;
    void operator+=(const Agenda & a);
    Entree & operator[](string nom) const;
    void operator-=(string nom);
    bool operator==(const Agenda & a) const;
    friend bool operator/(string nom, const Agenda & a);
    void operator()(char lettre) const;
};

#endif