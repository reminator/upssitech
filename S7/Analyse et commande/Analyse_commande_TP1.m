close all;

% ########### Exercice 1 ###########

% question 1
G = tf(1,[1 1 1]);

A=[0 1;-1 -1];
B=[0 ; 1];
C=[1 0];
D=[0];

RE = ss(A,B,C,0);

% question 2
polesG = pole(G)
vpContinu = eig(A)

% question 3
figure;
%step(G);

% question 4
figure;
%bode(G);

% ### partie bloquer

% question 1 bis
% a)
omega_max=1.27; % en rad/s
%Te=pi/omega_max; % calcul de Te
Te=2; % on impose la valeur de Te pour d) et e)
% b)
sysd=c2d(G, Te, 'zoh');
%transZ=tf(sysd);
polesTransZ=pole(sysd);
abs(polesTransZ(:));
% c)
figure(20);
step(G)
hold on
step(sysd)

% question 2 bis
Te=0.25;
sysd2=c2d(RE, Te, 'zoh');
transZ2=tf(sysd2);


% question 3 bis
polesTransZ2=pole(transZ2)
Te=0.25
polediscret=exp(polesG*Te)
vpTransZ2=eig(expm(Te*A)) % Ad = exp(A*Te)
vpTransZ2=eig(sysd2)
%poledd=pole(sysdd);

abs(polesTransZ2(:));

% ########### Exercice 2 ###########

% question 1
% a)
Te = 0.1
Adis=[0 .8 .4; 1 0 0; 0 1 .7];
Bdis=[0; 0; 1];
Cdis=[1 1 1];
Ddis=0;
REdis = ss(Adis,Bdis,Cdis,Ddis, Te);
%step(REdis)
% b)
tfBis=tf(REdis);
% expresion theorique --> Y(z) = G(z)U(z) = [Cd*(zI-Ad)^-1*Bd+Dd]*U(z)
% c)
polesBis=pole(tfBis)

% question 2
% a)

tr = 5
D = 0.05

zetha=sqrt((log(D)^2)/(pi^2 + log(D)^2));
omega=3/(zetha*tr);

p1 = -zetha*omega-omega*sqrt(1-zetha^2)*j
p2 = -zetha*omega+omega*sqrt(1-zetha^2)*j
p3 = -zetha*omega * 10

p1d = exp(p1 * Te)
p2d = exp(p2 * Te)
p3d = exp(p3 * Te)

% b)
Te = 0.1
K = acker(Adis, Bdis, exp([p1 p2 p3]*Te));

% c)
Abf = Adis-Bdis*K
bf = ss(Abf, Bdis, Cdis, Ddis, Te);

% d)
figure;
%step(bf);

% e)

N = 1/(Cdis*inv(eye(3)-Abf)*Bdis)
bf = ss(Abf, Bdis*N, Cdis, Ddis, Te);
figure;
%step(bf);

% f)

ftdf = tf(bf)
figure;
%step(ftdf)


%% Exercice 3
% 2.

k = 0.5
ti = 10
td = 0.5

sys = tf(1,[0.05 1.05 1 0])
%margin(sys)
pole(sys)

figure;
step(sys)
figure;
bode(sys)

k = 0.5
td = 0.5
ti=10
pd=tf([k*td k],1)
Dp = tf([k*ti*td k*td+k*ti k], [ti 0])



sys_pid = sys * Dp
bf=sys_pid/(1+sys_pid)

figure;
step(bf)

figure;
step(sys_pid)
figure;
bode(sys_pid)
%margin(sys_pid)

% 3.
Te = 0.1;
p = tf('p')
z = tf('z', Te)

pEuler1 = (z-1)/Te
pEuler2 = (z-1)/z*Te

DpEuler1 = 1/(ti*pEuler1) * (1 + ti * pEuler1) * (1 + td * pEuler1)
DpEuler2 = 1/(ti*pEuler2) * (1 + ti * pEuler2) * (1 + td * pEuler2)

opt = c2dOptions('Method','tustin');
DpTustin = c2d(Dp,.1,opt)



sys_pid = sys * Dp
bf=sys_pid/(1+sys_pid)
sysTustin = c2d(bf,.1,opt)
figure;
hold on
ramp(bf)
ramp(sysTustin);

% 4.

% 5.
sysd = tf(1,[0.05 1.05 1 0], Te)
TustinBF = sysd * DpTustin
bf=TustinBF/(1+TustinBF)

figure;
step(sysd);
hold on;
step(TustinBF);

%integ = sys([1], [1 0])
%figure;
%step(sysd*integ);

