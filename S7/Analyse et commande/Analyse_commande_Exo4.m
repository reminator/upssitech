% Exercice 4

% Partie 1
kg = 1
T = 0.1

G = tf([kg], [T 1 0]);

% a)

F = feedback(G, 1)

% b)
poles = pole(F)
%  -8.8730 ; -1.1270
% Poles n�gatifs -> stable
% Pas d'oscillations car poles r�els

% c)
figure;
bode(F)
% -3dB -> 1.1 rad/s

% d)
figure;
step(F)

% Partie 2