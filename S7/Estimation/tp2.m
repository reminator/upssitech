load('data_MC.mat')
close all;

taille_h = size(h);
M = taille_h(1);
N = 50;

sigma2 = 0.4;
dirac = [1 zeros(1, M-1)];

%5
y = syst(dirac, h, sigma2);
h_estime = y(1:M);
% y=^h est l'estim� de h

%6
figure;
hold on;
plot(h_estime);
plot(h);
legend('Estim� de h', 'h');

dist_quad = 1/M * norm(h_estime - h)^2
% �loign� de z�ro, ce n'est pas satisfaisant du tout, l'estimation est trop
% aproximative, c'est PAS BIEN du tout comme dirait le prof

%7
Nr = 1000;

Y = zeros(M, Nr);
for i = 1:Nr
    yn = syst(dirac, h, sigma2);
    Y(:,i) = yn(1:M);
end
moyenne = mean(Y, 2);

figure;
hold on;
plot(moyenne);
plot(h);
legend('Moyenne de l''estimateur de h', 'h');

covariance = cov(Y');
MSE = norm(moyenne - h)^2/M
% MSE est tr�s proche de z�ro, donc c'est bien

%8
% Avec la question 3, on a montr� que Y = Ah + B Avec A qui correspond � la matrice toeplitz

A = toeplitz(x, [x(1) zeros(1, M-1)]);
yn = syst(x, h, sigma2);
h_hat = (A'*A)\A'*yn;
figure;
hold on;
plot(h_hat);
plot(h);
legend('Estim� de h', 'h');


MSE = norm(h_hat - h)^2/M

%10
H_hat = zeros(M, Nr);
for i=1:Nr
    H_hat(:, i) = (A'*A)\A'*syst(x, h, sigma2);
end
moy = mean(H_hat, 2)

figure;
hold on;
plot(moy);
plot(h);
legend('Moyenne de H_hat', 'h');

biais = h-moy;
cov_emp = cov(H_hat');
cov_theo = sigma2*eye(M)/(A'*A);
max(max(abs(cov_emp - cov_theo)))
% Flemme d'expliquer (de trouver une explication en fait)


%11 Toute la 3eme partie nul, j'ai fais n'importe quoi mais copi� le prof
%quand m�me
yn_aber = yn;
yn(10:14) = yn_aber(10:14) + 5;
h_hat_aber = (A'*A)\A'*yn_aber

figure;
hold on;
plot(h_hat_aber);
plot(h);
legend('h_hat aberrant', 'h');

MSE = norm(h_hat_aber - h)^2/M
max(max(abs(h_hat_aber - h_hat)))

%12
erreur = yn - syst(x, h_hat, 0)
% On demande une variance 0 pour avoir aucun bruit
erreur_aber = yn_aber - syst(x, h_hat_aber,0)

figure;
hold on;
subplot(121);
plot(erreur, 'o');
subplot(122);
plot(erreur_aber, 'x');

%13 Mal fait
yn_modif = [yn(1:9) ; yn(15:end)];
A_mod = [A(1:9,:) ; A(15:end, :)];
h_hat_modif = (A_mod'*A_mod)\A_mod'*yn_modif;

figure;
hold on;
plot(h, 'o');
plot(h_hat_modif, 'x');



