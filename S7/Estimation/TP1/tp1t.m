% I.1
N = 10000;
x = randn(N,1);

figure;
hold on;
[counts,centers] = hist(x,100);
bar(centers,counts/N/(centers(2)-centers(1)));

sigma = 1;
mu = 0;

px = @(m, s, a) (1/sqrt(2*pi*s)) * exp( - ((a(:) - m).^2) / (2*s));

xGauss = (min(x):0.1:max(x));
yGauss = px(mu, sigma, xGauss); %(1/(sigma*sqrt(2*pi))) * exp( - ((xGauss(:) - mu).^2) / (2*sigma^2));



plot(xGauss, yGauss);

y = 10 + x(:)*sqrt(2);

muY = 10
varianceY = 2

% mvnrnd(mu,Sigma,n)
y2 = sqrt(varianceY).*randn(N,1) + muY


[counts,centers] = hist(y2,100);
bar(centers,counts/N/(centers(2)-centers(1)));

xGauss = (min(y2):0.1:max(y2));
yGauss = px(muY, varianceY, xGauss);

plot(xGauss, yGauss);

% II.5

x1 = randn(1,N);
x2 = randn(1,N);
X1 = [x1 ; x2];
% X1 = randn(2, N);

%6
%%

figure;
% Les points sont �parpill�s -> Ind�pendants
scatter(x1, x2);


figure;
x21 = sqrt(2).*randn(1,N) + 10;
x22 = sqrt(0.2).*randn(1,N) + 2;
X2 = [x21 ; x22];
scatter(x21, x22);

%8
%%
x31 = x1;
a = 100;
x32 = x2 + a * x1;

% Var(x32) = Var(x2) + Var(a * x1)
%          = 1 + a�

X3 = [x31 ; x32];
%9
scatter(x31, x32);
% Les points sont align�s -> relation lin�aire, pas ind�pendant
% Avec a = 5, les points sont plus rapproch�. Plus la variance est �lev�e
% plus �a fait �a

%10

%mux3 = [ E(x31)]
%       [ E(x32)]

%var3 = [ 1             cov(x31, x32) ]
%       [ cov(x32, x31) 1 + a�]

% X3 = [ x11 ; a*x11 + x12] = [1 0 ; a 1] * [x11 ; x12]
%                                   A            X1

% Y = AX + b environ= N(A*muX + b, A * SIGMAX * AT)
%      X suit N(muX, SIGMAX)

% cov(x31, x32) = cov(x11, a * x11 + x12)
%               = cov(x11, a*x11) + cov(x11, x12)
%               = a cov(x11, x11) + 0 -> 0 car ind�pendants
% Var(X3) = A * Var(X1) * AT = [1 0 ; a 1] * I * [1 a ; 0 1] = [1 a ;a
% a�+1]


K = 3;
N = 10000;

X = randn(K, N);

% 1 -> additionne les colonnes ;  2 -> lignes
y = sum(X, 1);

figure;
hold on;
[counts,centers] = hist(y,100);
bar(centers,counts/N/(centers(2)-centers(1)));

%13
% Somme de K N(0,1) ind�pendants donc y suit N(0,K)

xGauss = (min(y):0.1:max(y));
yGauss = px(0, K, xGauss);

plot(xGauss, yGauss);


%14
z = sum(X.^2, 1);

figure;
hold on;
[counts,centers] = hist(z,100);
bar(centers,counts/N/(centers(2)-centers(1)));

pz = @(z, K) (z.^((K/2)-1) .* exp(-z/2)) / (2^(K/2) * gamma(K/2));


xChi2 = (min(z):0.1:max(z));
yChi2 = pz(xChi2, K);
x
plot(xChi2,yChi2);

%15 Var(z) = K� + 2K - K� = 2K ; E(z) = K apr�s calcul






