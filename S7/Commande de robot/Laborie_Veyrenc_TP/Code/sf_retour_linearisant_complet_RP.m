function [sys,x0,str,ts] = sf_retour_linearisant_complet_RP(t,x,u,flag)
% NE PAS MODIFIER CI-DESSOUS ; CF. SEULEMENT mdlOutputs PLUS BAS
%
switch flag,
  case 0,
    [sys,x0,str,ts] = mdlInitializeSizes;
  case 3,
    sys = mdlOutputs(t,x,u);
  case {1,2,4,9}
    sys = [];
  otherwise
    error(['Unhandled flag = ',num2str(flag)]);
end
%=============================================================================
function [sys,x0,str,ts] = mdlInitializeSizes
% NE PAS MODIFIER CI-DESSOUS ; CF. SEULEMENT mdlOutputs PLUS BAS
%
sizes = simsizes;
sizes.NumContStates  = 0;
sizes.NumDiscStates  = 0;
sizes.NumOutputs     = 2;               % [v1;v2]
sizes.NumInputs      = 10;             
% [q1star;q2star;dq1star;dq2star;ddq1star;ddq2star;q1;q2;dq1;dq2]
sizes.DirFeedthrough = 1;
sizes.NumSampleTimes = 1;
sys = simsizes(sizes);
x0  = [];
str = [];
ts  = [-1 0];                           % p�riode h�rit�e du bloc p�re
%=============================================================================
function sys = mdlOutputs(t,x,u)
%
Rp = [1/200 1/30 1];
r=Rp(1); 
m=15;
Km_R = 0.3; %Km_r=Km/R
Beff = 1/80;
Jm=1/100;
Jeff = Jm + r^2*m;
zeta=1;
W=4*zeta;
K=(W^2*Jeff)/Km_R;
Kd=(2*zeta*W*Jeff-Beff)/Km_R;

Jeff1Min = Jm + r^2 * m * 0.5^2;
Jeff1Max = Jm + r^2 * m * 1^2;
Jeff2 = Jm+r^2*m;
Gy = -9.81;


q1star = u(1); q2star = u(2); dq1star = u(3); dq2star = u(4);
ddq1star = u(5); ddq2star = u(6);
q1 = u(7); q2 = u(8); dq1 = u(9); dq2 = u(10);
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% � COMPL�TER � PARTIR D'ICI %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
u1_ = ddq1star + Kd * (dq1star - dq1) + K * (q1star - q1);
u2_ = ddq2star + Kd * (dq2star - dq2) + K * (q2star - q2);
% sys DOIT �TRE INTIALIS� AVEC LE CONTENU DU VECTEUR DE SORTIE DU BLOC
% e.g.
sys = [u1_; u2_];
%=============================================================================
