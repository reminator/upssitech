Rp = [1/200 1/30 1];
r=Rp(2); 
m=15;
Km_R = 0.3; %Km_r=Km/R
Beff = 1/80;
Jm=1/100;
Jeff = Jm + r^2*m;
zeta=1;
W=4*zeta;
K=(W^2*Jeff)/Km_R;
Kd=(2*zeta*W*Jeff-Beff)/Km_R;
Ti = 6;

FT = tf([K*Km_R], [Jeff Beff+Kd*Km_R K*Km_R])
%FT = tf(1, [Jeff/(K*Km_R) (Beff+Kd*Km_R)/(K*Km_R) 1]);
FTBO = tf(Km_R*r, [Jeff Beff 0]);
figure;
subplot(211);
rlocus(FTBO);
subplot(212);
rlocus(FT);

Jeff1Min = Jm + r^2 * m * 0.5^2;
Jeff1Max = Jm + r^2 * m * 1^2;
Jeff2 = Jm+r^2*m;
close all;

