function [sys,x0,str,ts] = sf_loi_avant_RP(t,x,u,flag)
% NE PAS MODIFIER CI-DESSOUS ; CF. SEULEMENT mdlOutputs PLUS BAS
%
switch flag,
  case 0,
    [sys,x0,str,ts] = mdlInitializeSizes;
  case 3,
    sys = mdlOutputs(t,x,u);
  case {1,2,4,9}
    sys = [];
  otherwise
    error(['Unhandled flag = ',num2str(flag)]);
end
%=============================================================================
function [sys,x0,str,ts] = mdlInitializeSizes
% NE PAS MODIFIER CI-DESSOUS ; CF. SEULEMENT mdlOutputs PLUS BAS
%
sizes = simsizes;
sizes.NumContStates  = 0;
sizes.NumDiscStates  = 0;
sizes.NumOutputs     = 2;               % [d1;d2]
sizes.NumInputs      = 6;               % [q1;q2;dq1;dq2;ddq1;ddq2]
sizes.DirFeedthrough = 1;
sizes.NumSampleTimes = 1;
sys = simsizes(sizes);
x0  = [];
str = [];
ts  = [-1 0];                           % p�riode h�rit�e du bloc p�re
%=============================================================================
function sys = mdlOutputs(t,x,u)
%
q1 = u(1); q2 = u(2); dq1 = u(3); dq2 = u(4); ddq1 = u(5); ddq2 = u(6);
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% � COMPL�TER � PARTIR D'ICI %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
Rp = [1/200 1/30 1];
r=Rp(2); 
m=15;
Km_R = 0.3; %Km_r=Km/R
Beff = 1/80;
Jm=1/100;
Jeff = Jm + r^2*m;
zeta=1;
W=4*zeta;
K=(W^2*Jeff)/Km_R;
Kd=(2*zeta*W*Jeff-Beff)/Km_R;

Jeff1Min = Jm + r^2 * m * 0.5^2;
Jeff1Max = Jm + r^2 * m * 1^2;
Jeff2 = Jm+r^2*m;
Gy = -9.81;



q1 = u(1); q2 = u(2); dq1 = u(3); dq2 = u(4); ddq1 = u(5); ddq2 = u(6);

zeta11 = (Jeff1Max/Km_R)*(ddq1/r)+ (Beff/Km_R + Kd )*(dq1/r);
zeta12 = (Jeff1Max/Km_R)*(ddq2/r)+ (Beff/Km_R + Kd )*(dq2/r);
zeta21 = (1/Km_R)*r*(2*m*q2*dq1*dq2-m*q2*Gy*cos(q1));
zeta22 = (1/Km_R)*r*(-m*q2*dq1^2-m*Gy*sin(q1));
s1 = zeta11+zeta21;
s2 = zeta12+zeta22;
% sys DOIT �TRE INTIALIS� AVEC LE CONTENU DU VECTEUR DE SORTIE DU BLOC
% e.g.
sys = [s1;s2];
%=============================================================================
