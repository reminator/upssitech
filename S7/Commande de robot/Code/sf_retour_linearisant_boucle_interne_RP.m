function [sys,x0,str,ts] = sf_retour_linearisant_boucle_interne_RP(t,x,u,flag)
% NE PAS MODIFIER CI-DESSOUS ; CF. SEULEMENT mdlOutputs PLUS BAS
%
switch flag,
  case 0,
    [sys,x0,str,ts] = mdlInitializeSizes;
  case 3,
    sys = mdlOutputs(t,x,u);
  case {1,2,4,9}
    sys = [];
  otherwise
    error(['Unhandled flag = ',num2str(flag)]);
end
%=============================================================================
function [sys,x0,str,ts] = mdlInitializeSizes
% NE PAS MODIFIER CI-DESSOUS ; CF. SEULEMENT mdlOutputs PLUS BAS
%
sizes = simsizes;
sizes.NumContStates  = 0;
sizes.NumDiscStates  = 0;
sizes.NumOutputs     = 2;               % [v1;v2]
sizes.NumInputs      = 6;               % [e1;e2;q1;q2;dq1;dq2]
sizes.DirFeedthrough = 1;
sizes.NumSampleTimes = 1;
sys = simsizes(sizes);
x0  = [];
str = [];
ts  = [-1 0];                           % p�riode h�rit�e du bloc p�re
%=============================================================================
function sys = mdlOutputs(t,x,u)
%
Rp = [1/200 1/30 1];
r=Rp(1); 
m=15;
Km_R = 0.3; %Km_r=Km/R
Beff = 1/80;
Jm=1/100;
Jeff = Jm + r^2*m;
zeta=1;
W=4*zeta;
K=(W^2*Jeff)/Km_R;
Kd=(2*zeta*W*Jeff-Beff)/Km_R;

Jeff1Min = Jm + r^2 * m * 0.5^2;
Jeff1Max = Jm + r^2 * m * 1^2;
Jeff2 = Jm+r^2*m;
Gy = -9.81;

e1 = u(1); e2 = u(2);
q1 = u(3); q2 = u(4); dq1 = u(5); dq2 = u(6);
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% � COMPL�TER � PARTIR D'ICI %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% sys DOIT �TRE INTIALIS� AVEC LE CONTENU DU VECTEUR DE SORTIE DU BLOC
% e.g.
u1 = (m * q2^2 + Jm / r^2) * e1 + 2 * m * q2 * dq1 * dq2 + Beff / r^2 * dq1 - m * q2 * Gy * cos(q1)
u2 = (m + Jm / r^2) * e2 - m * q2 * dq1^2 + Beff / r^2 * dq2 - m * Gy * sin(q1)
sys = [u1;u2];
%=============================================================================
