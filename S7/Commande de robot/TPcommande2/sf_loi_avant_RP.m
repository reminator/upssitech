function [sys,x0,str,ts] = sf_loi_avant_RP(t,x,u,flag)
% NE PAS MODIFIER CI-DESSOUS ; CF. SEULEMENT mdlOutputs PLUS BAS
%
switch flag,
  case 0,
    [sys,x0,str,ts] = mdlInitializeSizes;
  case 3,
    sys = mdlOutputs(t,x,u);
  case {1,2,4,9}
    sys = [];
  otherwise
    error(['Unhandled flag = ',num2str(flag)]);
end
%=============================================================================
function [sys,x0,str,ts] = mdlInitializeSizes
% NE PAS MODIFIER CI-DESSOUS ; CF. SEULEMENT mdlOutputs PLUS BAS
%
sizes = simsizes;
sizes.NumContStates  = 0;
sizes.NumDiscStates  = 0;
sizes.NumOutputs     = 2;               % [d1;d2]
sizes.NumInputs      = 6;               % [q1;q2;dq1;dq2;ddq1;ddq2]
sizes.DirFeedthrough = 1;
sizes.NumSampleTimes = 1;
sys = simsizes(sizes);
x0  = [];
str = [];
ts  = [-1 0];                           % p�riode h�rit�e du bloc p�re
%=============================================================================
function sys = mdlOutputs(t,x,u)
%
q1 = u(1); q2 = u(2); dq1 = u(3); dq2 = u(4); ddq1 = u(5); ddq2 = u(6);
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% � COMPL�TER � PARTIR D'ICI %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
Jm=1/100;
Beff=1/80;
m=15;
r=1/200;
Km_R=0.3;
Jeff=Jm+r^2*m;
Wn=4;

Kp=(Jeff*Wn^2)/Km_R; 
Kd=((2/Wn)*Kp*Km_R - Beff)/Km_R;
% sys DOIT �TRE INTIALIS� AVEC LE CONTENU DU VECTEUR DE SORTIE DU BLOC

sys = [
    dq1/r*(Beff/Km_R+Kd) + ddq1/r*Jeff/Km_R + 1/Km_R*r*(2*m*q2*dq1*dq2+m*q2*9.81*cos(q1));
    dq2/r*(Beff/Km_R+Kd) + ddq2/r*Jeff/Km_R + 1/Km_R*r*(-m*q2*dq1^2+m*9.81*sin(q1))
    ];
%=============================================================================
