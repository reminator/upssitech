clear;
close all;

[image1, descriptors1, locs1] = sift('TP02I01.bmp');

I = imread('visage1.jpeg');
imwrite(I, 'visage1.pgm');

I = imread('visage2.jpeg');
imwrite(I, 'visage2.pgm');

I = imread('visage3.jpeg');
imwrite(I, 'visage3.pgm');

[image, descrips, locs] = sift('visage1.pgm');
showkeys(image, locs);

[image, descrips, locs] = sift('visage2.pgm');
showkeys(image, locs);

[image, descrips, locs] = sift('visage3.pgm');
showkeys(image, locs);

%figure;
match('visage1.pgm','visage2.pgm');
match('visage1.pgm','visage3.pgm');

%%

clear;
close all;

I = double(imread('TP02I01.bmp'));
tailleImg = size(I);

res = zeros(tailleImg(1), tailleImg(2));
valeurs = [-5, -3, -1, 1, 3, 5];

for i = 1+10 : tailleImg(1) - 10
    for j = 1+10 : tailleImg(2) - 10
        vals = [];
        for a = valeurs
            for b = valeurs
                val = sum(sum(abs(I(i-5:i+5, j-5:j+5) - I(i-5+a:i+5+a, j-5+b:j+5+b))));
                vals = [vals val];
            end
        end
        res(i, j) = min(vals);
    end
end


%%

figure;
affiche_image_grise(res);

xs = [];
ys = [];
for i=2:tailleImg(1)-1
    for j=2:tailleImg(2)-1
        p = res(i, j);
        % test = res(i-1:i+1, j-1:j+1)
        % if p == max(test)
        if res(i-1, j-1) < p && res(i-1, j) < p && res(i-1, j+1) < p && res(i, j-1) < p && res(i, j+1) < p && res(i+1, j-1) < p && res(i+1, j) < p && res(i+1, j+1) < p
            xs = [xs i];
            ys = [ys j];
        end
    end
end

figure;
affiche_image_grise(I);
hold on;
plot(ys, xs, "o");
