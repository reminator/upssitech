function [xo,yo]=barycentre(img)
[is, js] = find(img);
si = size(is);

xo = 0;
yo = 0;
for k=1:si(1)
    i=is(k);
    j=js(k);
    xo = xo + i;
    yo = yo + j;
end

xo = xo / si(1);
yo = yo / si(1);
end

