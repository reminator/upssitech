clear;
close all;

I = double(imread('TP3I01.jpg'));

tailleI = size(I);
corners = edge(I);

figure;
affiche_image_grise(corners);

tailleImg = size(I);

V = zeros(480+462, 628);
tailleV = size(V);

[is, js]=find(corners);
S = size(is)

seuil = 0.5;
for ro=1:tailleV(1)
    for teta=1:tailleV(2)
        t = teta / 628 * 2 * pi;
        somme = 0;
        for k=1:S(1)
            i = is(k);
            j = js(k);
                val = abs(i * cos(t) + j * sin(t) - ro);
                if val <= seuil
                    somme = somme + 1;
                end
        end
        V(ro, teta) = somme;
    end
end

figure;
imagesc(log(V+1));
colormap(gray(256));

K=20;
V2 = V;
ros = zeros(K, 1);
tetas = zeros(K, 1);

y1 = zeros(K, 1);
y2 = zeros(K, 1);
x1 = zeros(K, 1);
x2 = zeros(K, 1);

for k=1:K
    [xs, ys] = find(V2==max(max(V2)));
    ros(k) = xs(1);
    tetas(k) = ys(1);
    V2(xs(1), ys(1)) = 0;
    
    ro = ros(k);
    teta = tetas(k);
    t = teta / 628 * 2 * pi;
    
    pxs = [];
    pys = [];
    for l=1:S(1)
        i = is(l);
        j = js(l);
        
        val = abs(i * cos(t) + j * sin(t) - ro);
        if val <= seuil
            pxs = [pxs ; i];
            pys = [pys ; j];
        end
    end
    
    S2 = size(pxs);
    dist = 0;
    for l=1:S2(1)
        for m=1:S2(1)
            d = sqrt((pxs(l)-pxs(m))^2 + (pys(l)-pys(m))^2);
            if d > dist
                dist = d;
                x1(k) = pxs(l);
                x2(k) = pxs(m);
                y1(k) = pys(l);
                y2(k) = pys(m);
            end
        end
    end
end

    figure;
    affiche_image_grise(corners);
    hold on;
for k=1:K
    plot([y1(k) y2(k)],[x1(k) x2(k)],'-w');
end

%%
clear;
close all;

I2 = double(imread('TP3I02.dib'));

tailleI2 = size(I2);

imgRes = zeros(468, 500);

tailleRes = size(imgRes);
for i=1:tailleRes(1)
    for j=1:tailleRes(2)
        somme = 0;
        for teta = 1:tailleRes(2)
            t = (teta / tailleI2(2) * 2 * pi);
            ro = round(i * cos(t) + j * sin(t));
            
            if ro <= tailleRes(1) && ro > 0
                somme = somme + I2(ro, teta);
            end
        end
        imgRes(i, j) = somme;
    end
end

    figure;
    affiche_image_grise(imgRes);
    
%%
clear;
close all;

img=imread('TP03I03.bmp');
img=img(:,:,1)>0;
% Recup�ration des informations sur le contour
[C, N]=contour(img);
% Initisalisation du mod�le :
% ligne = valeur de beta * 100
% colonne = num�ro d'ordre du couple (alpha, distance)
% troisi�me dimension : pour 1 = alpha, pour 2 = distance
H=zeros(round(100*2*pi),N,2);
% Calcul des coordonn�es du barycentre
[xo,yo] = barycentre(img);
% parcours des points du contour
for i=1:N
    % calcul de b=beta pour le ieme point du contour
    b=round(beta(C,img,i));
    % recherche de la premiere colonne "vide" dans le tableau sur la ligne beta
    k=1;
    while H(b+1,k,2)~=0
        k=k+1;
    end
    % enregistrement de (la valeur de alpha et de la distance) sur la b+1 ligne et la kieme colonne de H
    H(b+1, k, 1) = alpha(C, i, img);
    H(b+1, k, 2) = distance(xo,yo,C(i, 1),C(i, 2));
end 
