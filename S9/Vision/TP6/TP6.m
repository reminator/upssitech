
img1 = double(imread('training/img1.bmp'));
affiche_image_grise(img1);

T = zeros(100, 3301);
nbf = 3300;
for i=1:1000

    img = double(imread(['training/img', int2str(i), '.bmp']));
    V = haarfeature(img);
    T(i, 1:3300) = V;
    if i <= 500
        val = 1;
    else
        val = 0;
    end
    T(i, 3301) = val;
end

%%

V = T(:, 3301);
tetas = zeros(3300, 1);
for i=1:3300
    F = T(:, i);
    [teta,se] = seuil(F,V);
    tetas(i) = teta;

end