function [V] = haarfeature(img)

tailleImg = size(img);
X = tailleImg(1);
Y = tailleImg(2);

V = [];
nbFeatures = 0;

% Motif 1
for i=1:X
    for j=1:ceil(Y/2)
        for x=1:X-i+1
            for y=1:Y-(j*2)+1
                nbFeatures = nbFeatures + 1;
                f = sum(sum(img(x:x+i-1, y:y+j-1))) - sum(sum(img(x:x+i-1, y+j:y+(j*2)-1)));
                % V{nbFeatures} = img(x:x+i-1, y:y+(j*2)-1);
                V = [V f];
            end
        end
    end
end

% Motif 2
for i=1:ceil(X/2)
    for j=1:Y
        for x=1:X-(i*2)+1
            for y=1:Y-j+1
                nbFeatures = nbFeatures + 1;
                f = sum(sum(img(x:x+i-1, y:y+j-1))) - sum(sum(img(x+i:x+(i*2)-1, y:y+j-1)));
                V = [V f];
                % V{nbFeatures} = img(x:x+(i*2)-1, y:y+j-1);
            end
        end
    end
end

% Motif 3
for i=1:X
    for j=4:4:Y
        for x=1:X-i+1
            for y=1:Y-j+1
                nbFeatures = nbFeatures + 1;
                f = sum(sum(img(x:x+i-1, y:y+j/4-1))) - sum(sum(img(x:x+i-1, y+j/4:y+j/4+j/2-1))) + sum(sum(img(x:x+i-1, y+j/4+j/2-1:y+j-1)));
                V = [V f];
                %V{nbFeatures} = img(x:x+i-1, y:y+j-1);
            end
        end
    end
end

end

