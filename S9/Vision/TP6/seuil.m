function [teta,se] = seuil(F,V)

T = zeros(3300, 2);
T = [F V];

sort = sortrows(T,1);

taille = size(sort);

Tm = 0;
Tp = 0;

se = Inf;
for i=1:taille(1)
    if sort(i, 2) == 0
        Tm = Tm + 1;
    else
        Tp = Tp + 1;
    end
    
end

for i=1:taille(1)
    if i > 1
        teta = (sort(i-1, 1) + sort(i, 1)) / 2;
    else
        teta = 0;
    end
    
    Sm = 0;
    Sp = 0;
    for j=1:i
        if sort(j, 2) == 0
            Sm = Sm + 1;
        else
            Sp = Sp + 1;
        end
    end
    e = min(Sp + (Tm - Sm), Sm + (Tp - Sp));
    if e < se
        se = e;
        indice = teta;
    end
    
end

end

