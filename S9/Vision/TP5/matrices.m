function [res] = matrices(img, img2, tailleVoisin, k, seuil)
H = zeros(2, 2);
b = zeros(2, 1);

tailleImg = size(img);
res = zeros(tailleImg(1), tailleImg(2), 3);

sol = floor(tailleVoisin/2);
plafond = ceil(tailleVoisin/2);

minX = plafond;
maxX = tailleImg(1) - sol;

minY = plafond;
maxY = tailleImg(2) - sol;

for i=minX:maxX
    for j=minY:maxY
        sommeH11 = 0;
        sommeH12 = 0;
        sommeH21 = 0;
        sommeH22 = 0;
        
        sommeb1 = 0;
        sommeb2 = 0;
        
        % H(1, 1)
        for x=i-sol:i+sol-1
            for y=j-sol:j+sol
                sommeH11 = sommeH11 + (img(x + 1, y) - img(x, y)) ^ 2 ;
        % b(1)
                sommeb1 = sommeb1 + (img(x + 1, y) - img(x, y)) * (img2(x, y) - img(x, y));
            end
        end
        
        % H(2, 2)
        for x=i-sol:i+sol
            for y=j-sol:j+sol-1
                sommeH22 = sommeH22 + (img(x, y + 1) - img(x, y)) ^ 2 ;
        
        % b(2)
                sommeb1 = sommeb1 + (img(x, y + 1) - img(x, y)) * (img2(x, y) - img(x, y));
            end
        end
        
        % H(1, 2)
        % H(2, 1)
        for x=i-sol:i+sol-1
            for y=j-sol:j+sol-1
                sommeH12 = sommeH12 + (img(x, y + 1) - img(x, y)) * (img(x + 1, y) - img(x, y));
            end
        end
        sommeH21 = sommeH12;
        
        
        H(1, 1) = sommeH11;
        H(1, 2) = sommeH12;
        H(2, 1) = sommeH21;
        H(2, 2) = sommeH22;
        
        b(1) = sommeb1;
        b(2) = sommeb2;
        
        V = linsolve(H, b);
        lambda = eig(H);
        
        if lambda(1) < seuil && lambda(2) < seuil
            res(i, j, 1) = 0;
            res(i, j, 2) = 0;
            res(i, j, 3) = 255;
        else
            res(i, j, 1) = 128 + k * V(1);
            res(i, j, 2) = 128 + k * V(2);
            res(i, j, 3) = 255;
        end
    end
end

end

