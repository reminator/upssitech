clear;
close all;

img1 = double(imread('TP05I01.bmp'));
img2 = double(imread('TP05I02.bmp'));

nbVoisins = 3;
k = 20;
seuil = 5000;

res = matrices(img1, img2, nbVoisins, k, seuil);
image(uint8(res))