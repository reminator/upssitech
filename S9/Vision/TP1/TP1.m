clear;
close all;

I = double(imread('TP01I01.jpg'));
%affiche_image_grise(I);


F = fft2(I);
S = fftshift(F);
A = log(abs(S));

%affiche_image_grise(A);


F2 = fftshift(S);
I2 = ifft2(F2);

%affiche_image_grise(I2);

erreur = ErreurQuadratique(I, I2)

n = 1;
tailleI = size(I);
nMax = tailleI(1) / 2 - 1;

images = zeros(nMax - 1, tailleI(1), tailleI(2));
erreurs = zeros(nMax - 1, 1);

nbZeros = 0;
nbZeross = zeros(nMax - 1, 1);
while n < nMax
    if n == 1
        Stmp = S;
    end
    
    %affiche_image_grise(log(abs(Stmp)));
    
    for i = n:tailleI(1)-n+1
            Stmp(n, i) = 0;
            Stmp(tailleI(1) + 1 - n, i) = 0;

            Stmp(i, n) = 0;
            Stmp(i, tailleI(2) + 1 - n) = 0;
            nbZeros = nbZeros + 4;
    end
    nbZeros = nbZeros - 4;
    nbZeross(n) = nbZeros;
   
    
    
    F2 = fftshift(Stmp);
    I2 = ifft2(F2);
    images(n, :, :) = I2(:, :);
    %affiche_image_grise(abs(I2));
    %affiche_image_grise(abs(images(1)));
    n;
    e = ErreurQuadratique(I, abs(I2))
    erreurs(n) = e;
    n = n + 1;
end

%%

size(zeros(5, 1))
affiche_image_grise(abs(I2));

max(erreurs)
erreurs = erreurs / max(erreurs) * 100;
nbZeross = nbZeross / max(nbZeross) * 100;
figure;
plot(erreurs);
hold on;
plot(nbZeross);
