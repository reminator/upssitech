function [ImgBin] = bin(img)
taille = size(img);
ImgBin = img;
for i=1:taille(1)
    for j=1:taille(2)
        if img(i, j) == 0
            ImgBin(i, j) = 1;
        else
            ImgBin(i, j) = 0;
        end
    end
end

end

