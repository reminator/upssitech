clear;
close all;

carre = double(imread('carre.png'));
triangle = double(imread('triangle.png'));
cercle = double(imread('cercle.png'));

carre = carre(:, :, 1);
triangle = triangle(:, :, 1);
cercle = cercle(:, :, 1);

carreBin = bin(carre);
triangleBin = bin(triangle);
cercleBin = bin(cercle);

figure;
affiche_image_grise(carreBin);
figure;
affiche_image_grise(triangleBin);
figure;
affiche_image_grise(cercleBin);

% Masque de chanfrein utilis� : 
% 4 3 4
% 3 0 3
% 4 3 4

masque = [4 3 4 ; 3 0 3 ; 4 3 4];
tdCarre = transformeDist(carreBin, masque);
figure;
affiche_image_grise(tdCarre);

tdTriangle = transformeDist(triangleBin, masque);
figure;
affiche_image_grise(tdTriangle);

tdCercle = transformeDist(cercleBin, masque);
figure;
affiche_image_grise(tdCercle);







carre_main = double(imread('carre_main.png'));
triangle_main = double(imread('triangle_main.png'));
cercle_main = double(imread('cercle_main.png'));

carre_main = carre_main(:, :, 1);
triangle_main = triangle_main(:, :, 1);
cercle_main = cercle_main(:, :, 1);

carre_mainBin = bin(carre_main);
triangle_mainBin = bin(triangle_main);
cercle_mainBin = bin(cercle_main);

figure;
affiche_image_grise(carre_mainBin);
figure;
affiche_image_grise(triangle_mainBin);
figure;
affiche_image_grise(cercle_mainBin);

tds = {tdCarre, tdTriangle, tdCercle};
figures = {carre_mainBin, triangle_mainBin, cercle_mainBin};

res = Inf(3, 3);

for i=1:3
    for j=1:3
        res(i, j) = score(figures{i}, tds{j});
    end
end
res