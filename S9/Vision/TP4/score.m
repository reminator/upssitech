function [res] = score(img, td)
res = 0;

tailleImg = size(img);

for i=1:tailleImg(1)
    for j=1:tailleImg(2)
        res = res + img(i, j) * td(i + 1, j + 1);
    end
end

end

