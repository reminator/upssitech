function [imgRes] = transformeDist(img, masque)

tailleImg = size(img);

imgRes = zeros(tailleImg(1) + 2, tailleImg(2) + 2);
tailleRes = size(imgRes);

imgRes(1, :) = zeros(1, tailleRes(2));
imgRes(tailleRes(1), :) = zeros(1, tailleRes(2));

imgRes(:, 1) = zeros(tailleRes(1), 1);
imgRes(:, tailleRes(2)) = zeros(tailleRes(1), 1);

imgRes(2:tailleRes(1) - 1, 2:tailleRes(2) - 1) = img;

for i=1:tailleRes(1)
    for j=1:tailleRes(2)
        if imgRes(i, j) == 0
            imgRes(i, j) = Inf;
        else
            imgRes(i, j) = 0;
        end
    end
end

% Balayage descendant
for i=2:tailleRes(1) - 1
    for j=2:tailleRes(2) - 1
        val = Inf;
        for x=1:2
            for y=1:3
                if x ~= 2 || y ~= 3
                    val = min(val, masque(x, y) + imgRes(i - 2 + x, j - 2 + y));
                end
            end
        end
        
        imgRes(i, j) = val;
    end
end

% Balayage ascendant
for i=tailleRes(1) - 1:-1:2
    for j=tailleRes(2) - 1:-1:2
        val = Inf;
        for x=2:3
            for y=1:3
                if x ~= 2 || y ~= 1
                    val = min(val, masque(x, y) + imgRes(i - 2 + x, j - 2 + y));
                end
            end
        end
        
        imgRes(i, j) = val;
    end
end


end

