clear
close all

A_REMPLIR = nan;

vR = 1, omegaR = 0.2, Te = 0.25

Ac = [0 omegaR 0 ; -omegaR 0 vR ; 0 0 0]; Bc = [-1 0 ; 0 0 ; 0 -1];
Ad = ...
[  cos(Te*omegaR), sin(Te*omegaR), (vR - vR*cos(Te*omegaR))/omegaR;
  -sin(Te*omegaR), cos(Te*omegaR),     (vR*sin(Te*omegaR))/omegaR;
              0,            0,                          1];
Bd = ...
[          -sin(Te*omegaR)/omegaR, (vR*(sin(Te*omegaR) - Te*omegaR))/omegaR^2;
  (2*sin((Te*omegaR)/2)^2)/omegaR,    -(2*vR*sin((Te*omegaR)/2)^2)/omegaR^2;
                             0,                                   -Te];
Gc = ss(Ac,Bc,eye(3),0);
B0Gd = ss(Ad,Bd,eye(3),0,Te); B0Gd_bis = c2d(Gc,Te,'zoh');
     
%% Section IV - DLQ Riccati
fprintf('= Section IV - DLQ Riccati =====================================\n');
N = 100; % Attention, horizon de taille N => indices de 1 � N+1 !
nb_cas = 3;
S = {100*eye(3),100*eye(3),diag([1 1 100])};
Q = {100*eye(3),1*eye(3),diag([100 100 1])};
R = {1*eye(2),100*eye(2),diag([1 10])};
E0 = [4 2 pi/3].';
TIME = [0:N]*Te;

%
% R�solution de l'�quation r�currente r�trograde de Riccati
P = {nan(3,3,N+1),nan(3,3,N+1),nan(3,3,N+1)};
for i_cas = 1:nb_cas
    P{i_cas}(:,:,N+1) = S{i_cas} ; % remplissage de P[N]
    for i_time = N+1:-1:2
        % depuis les instants k=N jusqu'� k=1 afin de remplir P[N-1] � P[0]
        Pk = P{i_cas}(:, :, i_time);
        Rcas = R{i_cas};
        Qcas = Q{i_cas};
        P{i_cas}(:,:,i_time-1) = Ad.' * Pk * Ad + Qcas - Ad.' * Pk * ...
        (Bd / (Rcas + Bd.' * Pk * Bd)) * Bd.' * Pk * Ad;
    end
end
%
% Gain de retour d'�tat u[k] = -K[k] x[k] depuis k=N-1 jusqu'� k=0
K = {nan(2,3,N+1),nan(2,3,N+1),nan(2,3,N+1)}; % derni�re page = NaN's
for i_cas = 1:nb_cas
    for i_time = 1:N
        Rcas = R{i_cas};
        Qcas = Q{i_cas};
        Pk = P{i_cas}(:, :, i_time+1);
        K{i_cas}(:,:,i_time) = (Rcas + Bd.' * Pk * Bd) \ Bd.' * Pk * Ad;
    end
end
%
% Simulation des �tats depuis k=0 jusqu'� k=N
U = {nan(2,1,N+1),nan(2,1,N+1),nan(2,1,N+1)};
X = {nan(3,1,N+1),nan(3,1,N+1),nan(3,1,N+1)};
for i_cas = 1:nb_cas
    X{i_cas}(:,:,1) = E0;
        Rcas = R{i_cas};
        Qcas = Q{i_cas};
    for i_time = 1:N
        Pk = P{i_cas}(:, :, i_time+1);
        U{i_cas}(:,:,i_time) = - K{i_cas}(:, :, i_time) * X{i_cas}(:, :, i_time);
        X{i_cas}(:,:,i_time+1) = (Ad - Bd / (Rcas + Bd.' * Pk * Bd) * Bd.' * Pk * Ad) ...
            * X{i_cas}(:,:,i_time);
    end
end
%
% Plot
for i_cas = 1:nb_cas
    plotX{i_cas}=squeeze(X{i_cas});
    plotU{i_cas}=squeeze(U{i_cas});
    figure(i_cas); title(sprintf('== DLQR -- Cas %d ==',i_cas));
    subplot(2,1,1); title('x');
    plot(TIME,plotX{i_cas}(1,:),'o-',...
         TIME,plotX{i_cas}(2,:),'o-.',...
         TIME,plotX{i_cas}(3,:),'o:');
    legend('x1','x2','x3');
    subplot(2,1,2); title('u');
    plot(TIME,plotU{i_cas}(1,:),'o-',...
         TIME,plotU{i_cas}(2,:),'o-.');
    legend('u1','u2');
end

%% Section VI - DLQR
fprintf('= Section VI - DLQR =====================================\n');
for i_cas = 1:nb_cas
    fprintf('Cas %d\n',i_cas);
    fprintf('--> solutions DLQR\n');
    P_DLQR{i_cas} = dare(Ad,Bd,Q{i_cas},R{i_cas});
    K_DLQR{i_cas} =  (R{i_cas}+Bd.'*P_DLQR{i_cas}*Bd) \ Bd.'*P_DLQR{i_cas}*Ad;
    P_DLQR{i_cas}, K_DLQR{i_cas}
    fprintf('--> "r�gimes permanents" P et  K du probl�me DLQ\n');
    P{i_cas}(:, :, N), K{i_cas}(:, :, N)
end

%% Section V - DLQ par Programmation Quadratique
fprintf('= Section V - DLQ par Programmation Quadratique =====================================\n');
for i_cas = 1:nb_cas
    Qr = repmat({Q{i_cas}}, 1, N);
    Rr = repmat({R{i_cas}}, 1, N);
    
    barQ{i_cas} = blkdiag(Qr{:});
    barQ{i_cas}(end-2:end, end-2:end) = S{i_cas};
    barR{i_cas} = blkdiag(Rr{:});
    %
    barS = zeros(N*3, N*2);
    barT = zeros(N*3, 3);
    for i_barST = 1:N
        for j_barS = 1:N
            if i_barST == j_barS
                barS(1 + 3*(i_barST-1):1 + 3*(i_barST-1)+2, 1+ 2*(j_barS-1):1+ 2*(j_barS-1)+1) = Bd;
            else
                if j_barS < i_barST
                    barS(1 + 3*(i_barST-1):1 + 3*(i_barST-1)+2, 1+ 2*(j_barS-1):1+ 2*(j_barS-1)+1) = Ad^(i_barST-j_barS)* Bd;
                end
            end
        end
        barT(1 + 3*(i_barST-1):1 + 3*(i_barST-1)+2, :) = Ad^i_barST;
    end
    H = 2 * (barR{i_cas} + barS.' * barQ{i_cas} * barS);
    f = 2 * barS.' * barQ{i_cas} * barT * E0;
    Y = 2 * (Q{i_cas} + barT.' * barQ{i_cas} * barT);
    %
    xiU{i_cas} = -H \ f;
    fprintf('Cas %d\n',i_cas);
    fprintf('--> u_B^*[0:N-1] solution DLQ Riccati\n');
    plotU{i_cas}(:,1:(end-1))
    fprintf('--> u_B^*[0:N-1] solution Prog Quad\n');
    reshape(xiU{i_cas},2,N)
    fprintf('--> �cart absolu maxi\n');
    max(max(abs(plotU{i_cas}(:,1:(end-1))-reshape(xiU{i_cas},2,N))))
end

%% Section VII - DLQ avec contraintes par Programmation Quadratique
fprintf('= Section V - DLQ avec contraintes par Programmation Quadratique =====================================\n');
i_cas = 1; % On choisit le Cas 1
%
% Crit�re 1/2 xi^T H xi o� xi = [E^T U^T]^T
% avec E = [e[1];...;e[N]] et U = [uB[1];...;uB[N-1]]
H = A_REMPLIR;
f = A_REMPLIR;
%
% Contraintes �galit�s (ind�pendantes du cas consid�r�)
Ae = A_REMPLIR;
Be = A_REMPLIR;
Ae1 = A_REMPLIR;
Ae2 = A_REMPLIR;
Ae = [Ae1 Ae2];
clear Ae1 Ae2
Be(1:3,:) = A_REMPLIR;
%
% Contraintes in�galit�s -- on consid�rera nb_cas_ineg cas diff�rents
nb_cas_ineg = 1; % on peut aller jusqu'� 3 par exemple
xiEU = cell(nb_cas_ineg);
fval = cell(nb_cas_ineg);
exitflag = cell(nb_cas_ineg);
Ai = cell(nb_cas_ineg);
Bi = cell(nb_cas_ineg);
uMIN = cell(nb_cas_ineg);
uMAX = cell(nb_cas_ineg);
L = cell(nb_cas_ineg); % sorties critiques z = L e
zMIN = cell(nb_cas_ineg);
zMAX = cell(nb_cas_ineg);
uMIN{1} = [-3;-0.5]; uMAX{1} = [3;2];
zMIN{1} = -0.7; zMAX{1} = pi/3; L{1} = [0 0 1]; %contrainte sur e3
for i_cas_ineg = 1:nb_cas_ineg
    Ai{i_cas_ineg} = zeros(2*N+4*N,3*N+2*N);
    Bi{i_cas_ineg} = zeros(2*N+4*N,1);
    Ai{i_cas_ineg} = A_REMPLIR;
    Bi{i_cas_ineg} = A_REMPLIR;
    [xiEU{i_cas_ineg},fval{i_cas_ineg},exitflag{i_cas_ineg}] = ...
        quadprog(H,f,Ai{i_cas_ineg},Bi{i_cas_ineg},Ae,Be);
    if (exitflag{i_cas_ineg} ~= 1), error('Revoir programme optim'); end
    %
    U_in_xiEU = xiEU{i_cas_ineg}((3*N+1):(5*N),:);
    plotUcontraint{i_cas_ineg} = reshape(U_in_xiEU,2,N); clear U_in_xiEU
    plotUcontraint{i_cas_ineg} = [plotUcontraint{i_cas_ineg} nan(2,1)];
    %
    X_in_xiEU = xiEU{i_cas_ineg}(1:(3*N),:);
    plotXcontraint{i_cas_ineg} = reshape(X_in_xiEU,3,N); clear X_in_xiEU
    plotXcontraint{i_cas_ineg} = [X{i_cas}(:,:,1) plotXcontraint{i_cas_ineg}];
end
%
% Plot
for i_cas_ineg = 1:nb_cas_ineg
    figure(10+i_cas_ineg);
    title(sprintf('== DLQR CONTRAINT -- Cas %d -- Cas\\_ineg %d ==',i_cas,i_cas_ineg));
    subplot(2,1,1); title('x');
    plot(TIME,plotXcontraint{i_cas}(1,:),'o-',...
         TIME,plotXcontraint{i_cas}(2,:),'o-.',...
         TIME,plotXcontraint{i_cas}(3,:),'o:');
    legend('x1','x2','x3');
    subplot(2,1,2); title('u');
    plot(TIME,plotUcontraint{i_cas}(1,:),'o-',...
         TIME,plotUcontraint{i_cas}(2,:),'o-.');
    legend('u1','u2');
end
