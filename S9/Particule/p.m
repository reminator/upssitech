function [y] = p(x, mu, sigma)

y = 1 / sqrt(det(2 * pi * sigma)) * exp(- 1 / 2 * (x - mu)' * inv(sigma)* (x - mu));

end

