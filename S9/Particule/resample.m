function [Xki,Wki] = resample(Xki, Wki, k, N, nbParticules)


c = zeros(nbParticules, 1);
u = zeros(nbParticules, 1);
c(1) = Wki(k, 1, :);

for i=2:nbParticules
    c(i) = c(i - 1) + Wki(k, i, :);
end

i=1;
u(1) = rand * 1 / nbParticules;

for j=1:nbParticules
    u(j) = u(1) + (j - 1) * 1/nbParticules;
    while u(j) > c(i)
        i = i + 1;
    end
    Xki(k, j, :) = Xki(k, i, :);
    Wki(k, j, :) = 1/nbParticules;
end
end

