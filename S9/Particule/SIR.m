function [Moy, Cov, Neff, Wki, Xki] = SIR(N,T,Z,F,Hfull,mX0,PX0,Qw,Rv,X, nbParticules, Ns)

Xki = zeros(N, nbParticules, 6);
Wki = zeros(N, nbParticules, 1);

Moy = zeros(N, 6);
Cov = zeros(N, 6, 6);
Neff = zeros(N, 1);

W = chol(Qw)'*randn(6,N);

for k=1:N
    
    if k==1
        for i=1:nbParticules
            % On initialise les premi�res particules selon la loi p(x0)
            Xki(k, i, :) = mX0 + chol(PX0)' * randn(6,1);
            Wki(k, i, :) = 1/nbParticules;
        end
    else
        for i=1:nbParticules
            % Propagation de la particule k-1
            Xki(k, i, :) = F * reshape(Xki(k-1, i, :), 6, 1) + chol(Qw)' * rand(6,1);
            
            % �valuation du poid wk
            % A1 et A2 visibles
            if (~isnan(Z(1, k)) && ~isnan(Z(3, k)))
                Wki(k, i) = Wki(k-1, i) * p(Z(:, k), Hfull*reshape(Xki(k, i, :), 6, 1), Rv);
            % A1 visible
            elseif (~isnan(Z(1, k)) && isnan(Z(3, k)))
                Wki(k, i) = Wki(k-1, i) * p(Z(1:2, k), Hfull(1:2, :)*reshape(Xki(k, i, :), 6, 1), Rv(1:2, 1:2));
            % A2 visible
            elseif (isnan(Z(1, k)) && ~isnan(Z(3, k)))
                Wki(k, i) = Wki(k-1, i) * p(Z(3:4, k), Hfull(3:4, :)*reshape(Xki(k, i, :), 6, 1), Rv(3:4, 3:4));
            % Ancun visible
            else
                Wki(k, i) = Wki(k-1, i);
            end
        end
        % Normalisation des poids
        somme = sum(Wki(k, :));
            for i=1:nbParticules
                Wki(k, i) = Wki(k, i) / somme;
            end
        
        % Estimation de l'�tat
        if k==1
            Wki(k, :)
        end
        
        for i=1:nbParticules
            Moy(k, :) = Moy(k, :) + Wki(k, i) * reshape(Xki(k, i, :), 1, 6);
        end
        
        % Estimation de la covariance
        for i=1:nbParticules
            Cov(k, :, :) = Cov(k, :, :) + reshape(Wki(k, i) * (reshape(Xki(k, i, :), 1, 6) - Moy(k, :))' * (reshape(Xki(k, i, :), 1, 6) - Moy(k, :)), 1, 6, 6);
        end
        
    end
    
    for i=1:nbParticules
        Neff(k) = Neff(k) + Wki(k, i)^2;
    end
    Neff(k) = 1 / Neff(k);
    
    if Neff(k) < Ns
        [Xki,Wki] = resample(Xki, Wki, k, N, nbParticules);
    end
end



end

