close all;
clear all;

[N,T,Z,F,Hfull,mX0,PX0,Qw,Rv,X] = simulationDonnees(0);


% [Moy, Cov, Neff, Wki] = SIS(N,T,Z,F,Hfull,mX0,PX0,Qw,Rv,X, 100);
[Moy, Cov, Neff, Wki, Xki] = SIR(N,T,Z,F,Hfull,mX0,PX0,Qw,Rv,X, 100, 0.5*100);
size(Cov)

%%

for i=1:50
    hold on;
    h(1)=ellipse(Moy(i, 1:2)', reshape(Cov(i, 1:2, 1:2), 2, 2), 'r');
    h(2)=plot(X(1, i)', X(2, i)', "+");
    h(3)=ellipse(Moy(i, 3:4)', reshape(Cov(i, 3:4, 3:4), 2, 2), 'g');
    h(4)=plot(X(3, i)', X(4, i)', "+");
    h(5)=ellipse(Moy(i, 5:6)', reshape(Cov(i, 5:6, 5:6), 2, 2), 'y');
    h(6)=plot(X(5, i)', X(6, i)', "+");
    h(7)=plot(Moy(i, 1:2:6)', Moy(i, 2:2:6)', "+");
    xlim([-5 5]);
    ylim([-5 5]);
    pause(0.5);
    delete(h)
end