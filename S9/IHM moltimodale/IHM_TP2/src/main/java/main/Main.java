package main;

import processing.core.PApplet;

import java.util.ArrayList;
import java.util.List;

public class Main extends PApplet {

    public static void main(String[] args) {
        String[] processingArgs = {"MySketch"};
        Main mySketch = new Main();
        PApplet.runSketch(processingArgs, mySketch);
    }

    public void generateUnistrokePermutations(List<Stroke> strokes) {
        for (int i = 0; i < strokes.size(); i++) {
            int order = i;
            List<Integer> orders = heapPermute(strokes.size(), order);
        }
        List<UniStroke> m = makeUnistrokes(strokes, orders);

    }

    public List<Integer> heapPermute(int n, int order) {

    }

    public List<UniStroke> makeUnistrokes(List<Stroke> strokes, List<Integer> orders) {

    }

    public float indicativeAngle(List<Point> points) {
        Point centroid = centroid(points);
        return atan2(centroid.Y - points.get(0).Y, centroid.X - points.get(0).X);
    }

    public List<Point> rotateBy(List<Point> points, float omega) {
        Point c = centroid(points);
        List<Point> newPoints = new ArrayList<>();
        for (Point p : points) {
            float qx = ((p.X - c.X) * cos(omega) - (p.Y - c.Y) * sin(omega) + c.X);
            float qy = ((p.X - c.X) * sin(omega) + (p.Y - c.Y) * cos(omega) + c.X);
            newPoints.add(new Point(qx, qy));
        }
        return newPoints;
    }

    public List<Point> checl

    public List<Point> translateTo(List<Point> points, Point k) {
        Point c = centroid(points);
        List<Point> newPoints = new ArrayList<>();
        points.forEach(p -> {
            float qx = p.X + k.X - c.X;
            float qy = p.Y + k.Y - c.Y;
            newPoints.add(new Point(qx, qy));
        });
        return newPoints;
    }

    private Point centroid(List<Point> points) {
        float x = 0;
        for (Point p : points) {
            x += p.X;
        }
        x = x / points.size();

        float y = 0;
        for (Point p : points) {
            y += p.Y;
        }
        y = y / points.size();

        return new Point(x, y);
    }
}
