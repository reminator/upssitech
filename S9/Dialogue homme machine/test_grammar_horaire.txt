PS C:\Users\remil\Documents\UPSSITECH\S9\Dialogue homme machine\Optimtalk> .\ot_grammar_tester.exe ..\AGENDA_V1\horaire.grxml

OptimTalk Grammar Tester (version 1.10)
========================
Copyright (c) 2004-2010 OptimSys, s.r.o., Czech Republic
  http://www.optimsys.cz

Testing grammar file:/C:/Users/remil/Documents/UPSSITECH/S9/Dialogue homme machine/AGENDA_V1/horaire.grxml
Enter an utternace or press Enter to finish:
une heure
Semantic result: {
  NTMN: 60,
  H: 1,
  M: 0,
  text: "une heure"
}
Enter an utternace or press Enter to finish:
une heure dix
Semantic result: {
  NTMN: 70,
  H: 1,
  M: 10,
  text: "une heure dix"
}
Enter an utternace or press Enter to finish:
deux heures et quart
Semantic result: {
  NTMN: 135,
  H: 2,
  M: 15,
  text: "deux heures et quart"
}
Enter an utternace or press Enter to finish:
trois heures vingt cinq
Semantic result: {
  NTMN: 205,
  H: 3,
  M: 25,
  text: "trois heures vingt cinq"
}
Enter an utternace or press Enter to finish:
quatre heures trente
Semantic result: {
  NTMN: 270,
  H: 4,
  M: 30,
  text: "quatre heures trente"
}
Enter an utternace or press Enter to finish:
cinq heures moins vingt cinq
Semantic result: {
  NTMN: 275,
  H: 4,
  M: 35,
  text: "cinq heures moins vingt cinq"
}
Enter an utternace or press Enter to finish:
six heures moins le quart
Semantic result: {
  NTMN: 345,
  H: 5,
  M: 45,
  text: "six heures moins le quart"
}
Enter an utternace or press Enter to finish:
sept heures quarante cinq
Semantic result: {
  NTMN: 465,
  H: 7,
  M: 45,
  text: "sept heures quarante cinq"
}
Enter an utternace or press Enter to finish:
huit heures moins douze
Semantic result: {
  NTMN: 468,
  H: 7,
  M: 48,
  text: "huit heures moins douze"
}
Enter an utternace or press Enter to finish:
neuf heures moins cinq
Semantic result: {
  NTMN: 535,
  H: 8,
  M: 55,
  text: "neuf heures moins cinq"
}
Enter an utternace or press Enter to finish:
midi
Semantic result: {
  NTMN: 720,
  H: 12,
  M: 0,
  text: "midi"
}
Enter an utternace or press Enter to finish:
midi et quart
Semantic result: {
  NTMN: 735,
  H: 12,
  M: 15,
  text: "midi et quart"
}
Enter an utternace or press Enter to finish: 
midi et demi
Semantic result: {
  NTMN: 750,
  H: 12,
  M: 30,
  text: "midi et demi"
}
Enter an utternace or press Enter to finish:
midi moins le quart
Semantic result: {
  NTMN: 705,
  H: 11,
  M: 45,
  text: "midi moins le quart"
}
Enter an utternace or press Enter to finish:
dix heure trente
Semantic result: {
  NTMN: 630,
  H: 10,
  M: 30,
  text: "dix heure trente"
}
Enter an utternace or press Enter to finish:
onze heures cinq
Semantic result: {
  NTMN: 665,
  H: 11,
  M: 5,
  text: "onze heures cinq"
}
Enter an utternace or press Enter to finish:
douze heures cinquante cinq
Semantic result: {
  NTMN: 775,
  H: 12,
  M: 55,
  text: "douze heures cinquante cinq"
}
Enter an utternace or press Enter to finish:
treize heures trente
Semantic result: {
  NTMN: 810,
  H: 13,
  M: 30,
  text: "treize heures trente"
}
Enter an utternace or press Enter to finish:
quatorze heures
Semantic result: {
  NTMN: 840,
  H: 14,
  M: 0,
  text: "quatorze heures"
}
Enter an utternace or press Enter to finish:
quinze heures quarante
Semantic result: {
  NTMN: 940,
  H: 15,
  M: 40,
  text: "quinze heures quarante"
}
Enter an utternace or press Enter to finish:
seize heures cinq
Semantic result: {
  NTMN: 965,
  H: 16,
  M: 5,
  text: "seize heures cinq"
}
Enter an utternace or press Enter to finish:
dix sept heures dix sept
Semantic result: {
  NTMN: 1037,
  H: 17,
  M: 17,
  text: "dix sept heures dix sept"
}
Enter an utternace or press Enter to finish:
dix huit heure
Semantic result: {
  NTMN: 1080,
  H: 18,
  M: 0,
  text: "dix huit heure"
}
Enter an utternace or press Enter to finish:
vingt heures vingt deux
Semantic result: {
  NTMN: 1222,
  H: 20,
  M: 22,
  text: "vingt heures vingt deux"
}
Enter an utternace or press Enter to finish:
vingt et une heures onze
Semantic result: {
  NTMN: 1271,
  H: 21,
  M: 11,
  text: "vingt et une heures onze"
}
Enter an utternace or press Enter to finish:
vingt deux heures seize
Semantic result: {
  NTMN: 1336,
  H: 22,
  M: 16,
  text: "vingt deux heures seize"
}
Enter an utternace or press Enter to finish:
vingt trois heures moins dix
Semantic result: {
  NTMN: 1370,
  H: 22,
  M: 50,
  text: "vingt trois heures moins dix"
}
Enter an utternace or press Enter to finish:
vingt quatre heures dix
Semantic result: {
  NTMN: 1450,
  H: 24,
  M: 10,
  text: "vingt quatre heures dix"
}
Enter an utternace or press Enter to finish:
minuit
Semantic result: {
  NTMN: 0,
  H: 0,
  M: 0,
  text: "minuit"
}
Enter an utternace or press Enter to finish:
minuit et quart
Semantic result: {
  NTMN: 15,
  H: 0,
  M: 15,
  text: "minuit et quart"
}
Enter an utternace or press Enter to finish:
minuit moins trois
Semantic result: {
  NTMN: 1437,
  H: 23,
  M: 57,
  text: "minuit moins trois"
}
Enter an utternace or press Enter to finish: 
minuit vingt cinq
Semantic result: {
  NTMN: 25,
  H: 0,
  M: 25,
  text: "minuit vingt cinq"
}
Enter an utternace or press Enter to finish:
trente heures dix
Semantic result: {
  NTMN: 1810,
  H: 30,
  M: 10,
  text: "trente heures dix"
}
Enter an utternace or press Enter to finish: 
soixante quinze heures cent cinquante huit
Semantic result: {
  NTMN: 4658,
  H: 75,
  M: 158,
  text: "soixante quinze heures cent cinquante huit"
}
Enter an utternace or press Enter to finish:
dans une heure
Semantic result: {
  NTMN: 60,
  H: 1,
  M: 0,
  text: "dans une heure"
}
Enter an utternace or press Enter to finish:
trente heures et quarante minutes
Semantic result: {
  NTMN: 1840,
  H: 30,
  M: 40,
  text: "trente heures et quarante minutes"
}
Enter an utternace or press Enter to finish:
a dix-neuf heures moins cinquante-deux
Semantic result: {
  NTMN: 1088,
  H: 18,
  M: 8,
  text: "a dix-neuf heures moins cinquante-deux"
}
Enter an utternace or press Enter to finish:
a dix-neuf heures cinquante-deux
Semantic result: {
  NTMN: 1192,
  H: 19,
  M: 52,
  text: "a dix-neuf heures cinquante-deux"