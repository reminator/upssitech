from hpp.corbaserver.manipulation import ConstraintGraph
from hpp.corbaserver import Client
from manipulation import (
    robot,
    vf,
    ps,
    Ground,
    Box,
    Pokeball,
    gripperName,
    ballName,
)
from manipulation import PathPlayer  # noqa: F401

vf.loadEnvironmentModel(Ground, "ground")
vf.loadEnvironmentModel(Box, "box")
vf.moveObstacle("box/base_link_0", [0.3 + 0.04, 0, 0.04, 0, 0, 0, 1])
vf.moveObstacle("box/base_link_1", [0.3 - 0.04, 0, 0.04, 0, 0, 0, 1])
vf.moveObstacle("box/base_link_2", [0.3, 0.04, 0.04, 0, 0, 0, 1])
vf.moveObstacle("box/base_link_3", [0.3, -0.04, 0.04, 0, 0, 0, 1])

vf.loadObjectModel(Pokeball, "pokeball")
robot.setJointBounds(
    "pokeball/root_joint",
    [
        -0.4,
        0.4,
        -0.4,
        0.4,
        -0.1,
        1.0,
        -1.0001,
        1.0001,
        -1.0001,
        1.0001,
        -1.0001,
        1.0001,
        -1.0001,
        1.0001,
    ],
)

q1 = [0, -1.57, 1.57, 0, 0, 0, 0.3, 0, 0.025, 0, 0, 0, 1]

# Create constraint of relative position of the ball in the gripper when ball
# is grasped
ballInGripper = [0, 0.137, 0, 0.5, 0.5, -0.5, 0.5]
ps.createTransformationConstraint(
    "grasp",
    gripperName,
    ballName,
    ballInGripper,
    6
    * [
        True,
    ],
)
# Create transformation constraint : ball is in horizontal plane with free
# rotation around z
ps.createTransformationConstraint(
    "placement",
    "",
    ballName,
    [0, 0, 0.025, 0, 0, 0, 1],
    [
        False,
        False,
        True,
        True,
        True,
        False,
    ],
)
#  Create complement constraint
ps.createTransformationConstraint(
    "placement/complement",
    "",
    ballName,
    [0, 0, 0.025, 0, 0, 0, 1],
    [
        True,
        True,
        False,
        False,
        False,
        True,
    ],
)

ps.setConstantRightHandSide("placement", True)
ps.setConstantRightHandSide("placement/complement", False)

# Create graph
graph = ConstraintGraph(robot, "graph")


ps.selectPathValidation("Discretized", 0.01)
ps.selectPathProjector("Progressive", 0.1)
graph.initialize()

res, q_init, error = graph.applyNodeConstraints("placement", q1)
q2 = q1[::]
q2[7] = 0.2

res, q_goal, error = graph.applyNodeConstraints("placement", q2)

ps.setInitialConfig(q_init)
ps.addGoalConfig(q_goal)

# v = vf.createViewer ()
# pp = PathPlayer (v)
# v (q1)
