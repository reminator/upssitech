package annuaire_interface;

public interface ContactInterface extends java.rmi.Remote {

    String getNom() throws java.rmi.RemoteException;;

    String getNumero_telephone() throws java.rmi.RemoteException;;

    String getPrenom() throws java.rmi.RemoteException;;

    void setIdentifiant(int identifiant) throws java.rmi.RemoteException;;
}
