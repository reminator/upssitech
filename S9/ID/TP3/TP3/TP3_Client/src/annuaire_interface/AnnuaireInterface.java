package annuaire_interface;

public interface AnnuaireInterface extends java.rmi.Remote {


    void addContact(ContactInterface contact) throws java.rmi.RemoteException;;

    ContactInterface addContact(String prenom, String nom, String numero) throws java.rmi.RemoteException;;

    ContactInterface getContactById(int id) throws java.rmi.RemoteException;;

    ContactInterface removeContactById(int id) throws java.rmi.RemoteException;;

    ContactInterface getContactByNom(String nom) throws java.rmi.RemoteException;;

    ContactInterface removeContactByNom(String nom) throws java.rmi.RemoteException;;

}
