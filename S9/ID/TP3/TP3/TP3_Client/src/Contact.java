import annuaire_interface.ContactInterface;

import java.io.Serializable;
import java.rmi.RemoteException;

public class Contact implements Serializable, ContactInterface {

    private final String prenom;
    private final String nom;
    private final String numero_telephone;

    private int identifiant;

    public Contact(String prenom, String nom, String numero_telephone) {
        this.prenom = prenom;
        this.nom = nom;
        this.numero_telephone = numero_telephone;
    }

    public String getNom() throws RemoteException {
        return nom;
    }

    public String getNumero_telephone() throws RemoteException {
        return numero_telephone;
    }

    public String getPrenom() throws RemoteException {
        return prenom;
    }

    public void setIdentifiant(int identifiant) throws RemoteException {
        this.identifiant = identifiant;
    }

    @Override
    public String toString() {
        return prenom + " " + nom + " : " + numero_telephone;
    }
}
