import annuaire_interface.AnnuaireInterface;
import annuaire_interface.ContactInterface;

import java.rmi.*;

public class HelloClient {

    public static void main(String args[]) {
        try {
            // rechercher une instance de type "HelloInterface" � l'adresse indiqu�e
            AnnuaireInterface obj = (AnnuaireInterface) Naming.lookup("rmi://localhost:12345/mon_serveur_annuaire");

            // appel des m�thodes
            Contact contact = (Contact) obj.addContact("Kahina", "Chalabi", "0635488456");
            System.out.println(contact);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
