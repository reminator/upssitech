import annuaire_interface.AnnuaireInterface;
import annuaire_interface.ContactInterface;

import java.io.Serializable;
import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.util.HashMap;
import java.util.Map;

public class BD_Contact extends UnicastRemoteObject implements Serializable, AnnuaireInterface {

    private final Map<Integer, ContactInterface> contacts = new HashMap<>();
    private int identifiant = 0;

    public BD_Contact() throws RemoteException {
        super();
    }

    public void addContact(ContactInterface contact) throws RemoteException {
        contact.setIdentifiant(identifiant);
        contacts.put(identifiant, contact);
        identifiant++;
    }

    @Override
    public ContactInterface addContact(String prenom, String nom, String numero) throws RemoteException {
        ContactInterface contact = new Contact(prenom, nom, numero);
        contact.setIdentifiant(identifiant);
        contacts.put(identifiant, contact);
        identifiant++;
        return contact;
    }

    public ContactInterface getContactById(int id) throws RemoteException {
        return contacts.get(id);
    }

    public ContactInterface removeContactById(int id) throws RemoteException {
        return contacts.remove(id);
    }

    public ContactInterface getContactByNom(String nom) throws RemoteException {
        for (ContactInterface contact : contacts.values()) {
            if (contact.getNom().equalsIgnoreCase(nom)) {
                return contact;
            }
        }
        return null;
    }

    public ContactInterface removeContactByNom(String nom) throws RemoteException {
        for (Map.Entry<Integer, ContactInterface> entry : contacts.entrySet()) {
            ContactInterface contact = entry.getValue();
            int id = entry.getKey();
            if (contact.getNom().equalsIgnoreCase(nom)) {
                return contacts.remove(id);
            }
        }
        return null;
    }
}
