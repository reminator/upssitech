import java.rmi.Naming;

public class Main {

    public static void main(String args[]) {
        try {

            BD_Contact bd = new BD_Contact();

            // lancer automatiquement un annuaire RMIRegistry sur le port 12345
            // il existe une autre possibilité en lançant rmiregistry depuis une ligne de commande !
            java.rmi.registry.LocateRegistry.createRegistry(12345);

            // enregistrer la classe dans l'annuaire
            Naming.rebind("rmi://localhost:12345/mon_serveur_annuaire", bd);

            System.out.println("Annuaire bound in registry");

            while(true) {
                System.out.println(bd.getContactByNom("Chalabi"));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}