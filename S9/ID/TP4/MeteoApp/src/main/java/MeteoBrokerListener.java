import org.eclipse.paho.client.mqttv3.*;

import java.util.Arrays;
import java.util.UUID;

public class MeteoBrokerListener implements MqttCallback {

    public static void main(String[] args) throws MqttException {
        new MeteoBrokerListener().runClient();

    }

    public void runClient() throws MqttException {


        MqttConnectOptions mqttConnectOptions = new MqttConnectOptions();
        mqttConnectOptions.setUserName("upssitech");
        mqttConnectOptions.setPassword("2011".toCharArray());

        MqttClient mqttClient = new MqttClient("tcp://localhost:1883", UUID.randomUUID().toString());
        mqttClient.connect();
        mqttClient.setCallback(this);
        MqttTopic topic = mqttClient.getTopic("/meteo/toulouse/temperature");

        mqttClient.subscribe(String.valueOf(topic));

    }

    @Override
    public void connectionLost(Throwable cause) {

    }

    @Override
    public void messageArrived(String topic, MqttMessage message) throws Exception {
        System.out.println(new String(message.getPayload()));
    }

    @Override
    public void deliveryComplete(IMqttDeliveryToken token) {

    }
}
