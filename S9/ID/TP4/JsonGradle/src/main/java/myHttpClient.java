
import java.io.*;
import java.util.Timer;
import java.util.TimerTask;

import netscape.javascript.JSObject;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.HttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.params.CoreProtocolPNames;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.json.JSONObject;


public class myHttpClient {

    public static void main(String[] args) throws Exception {
		Timer timer = new Timer();
		timer.schedule(new TimerTask() {
			@Override
			public void run() {
				String url = "https://openexchangerates.org/api/latest.json?app_id=3e529440970a45569050a477277e9019"; // FAKE REST API pour le test -> users : 10 items r�cup�r�s

				HttpClient client = HttpClientBuilder.create().build();
				HttpGet request = new HttpGet(url);

				// Ajout d'une ent�te
				request.addHeader("User-Agent", org.apache.http.params.CoreProtocolPNames.USER_AGENT);
				HttpResponse response = null;
				try {
					response = client.execute(request);
				} catch (IOException e) {
					e.printStackTrace();
				}

				System.out.println("Response Code : "
						+ response.getStatusLine().getStatusCode());

				BufferedReader rd = null;
				try {
					rd = new BufferedReader(
							new InputStreamReader(response.getEntity().getContent()));
				} catch (IOException e) {
					e.printStackTrace();
				}

				// lecture de la r�ponse
				StringBuffer result = new StringBuffer();
				String line = "";
				while (true) {
					try {
						if (!((line = rd.readLine()) != null)) break;
					} catch (IOException e) {
						e.printStackTrace();
					}
					result.append(line);
				}

				JSONObject res_json = new JSONObject(result.toString());
				JSONObject rates = res_json.getJSONObject("rates");
				float eur = rates.getFloat("EUR");
				float usd = rates.getFloat("USD");
				// Affichage non structur�
				System.out.println("eur " + eur);
				System.out.println("usd " + usd);
			}
		}, 0, 1000);
    }



}