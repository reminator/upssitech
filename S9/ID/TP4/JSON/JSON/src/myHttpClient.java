/* Exemple d'usage d'un client HTTP avec le code retour
 *
 */ 
 
import java.io.*;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.HttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.params.CoreProtocolPNames;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;


public class myHttpClient {

    public final static void main(String[] args) throws Exception {
        String url = "http://jsonplaceholder.typicode.com/users"; // FAKE REST API pour le test -> users : 10 items r�cup�r�s

		HttpClient client = HttpClientBuilder.create().build();
		HttpGet request = new HttpGet(url);

		// Ajout d'une ent�te
		request.addHeader("User-Agent", org.apache.http.params.CoreProtocolPNames.USER_AGENT);
		HttpResponse response = client.execute(request);

		System.out.println("Response Code : " 
                + response.getStatusLine().getStatusCode());

		BufferedReader rd = new BufferedReader(
			new InputStreamReader(response.getEntity().getContent()));
		
		// lecture de la r�ponse
		StringBuffer result = new StringBuffer();
		String line = "";
		while ((line = rd.readLine()) != null) {
			result.append(line);
		}
		// Affichage non structur�
		System.out.println(result);
    }

}