import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.net.ServerSocket;
import java.net.Socket;

import javax.print.event.PrintEvent;

public class ServerEcho {

    public static void main(String[] args) {

        while (true) {

            try {
                ServerSocket serverSocket = new ServerSocket(7);
                Socket socket = serverSocket.accept();
                
                BufferedReader fluxEntreeSocket = new BufferedReader(new InputStreamReader(socket.getInputStream()));
                String message = fluxEntreeSocket.readLine(); // lit dans la socket jusqu'� trouver un "\n" (nouvelle ligne)

                
                PrintStream printStream = new PrintStream(socket.getOutputStream());
                printStream.println("J'ai bien reçu le message : " + message);
                
                socket.close();
                serverSocket.close();
            } catch (IOException e) {

            }
        }
        
    }
}
