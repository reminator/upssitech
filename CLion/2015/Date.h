//
// Created by remil on 16/12/2021.
//

#ifndef INC_2015_DATE_H
#define INC_2015_DATE_H


class Date {
    int jour, mois, an, heure, minute ;
public:
    Date (int j= 1, int m=1, int an=2016, int heure=8, int minute=0);

    int getJour() const;

    void setJour(int jour);

    int getMois() const;

    void setMois(int mois);

    int getAn() const;

    void setAn(int an);

    int getHeure() const;

    void setHeure(int heure);

    int getMinute() const;

    void setMinute(int minute);

    void afficher();

};


#endif //INC_2015_DATE_H
