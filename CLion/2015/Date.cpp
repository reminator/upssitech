//
// Created by remil on 16/12/2021.
//

#include "Date.h"

int Date::getJour() const{
    return jour;
}

void Date:: setJour(int jour) {
    this->jour = jour;
}

int Date::getMois() const {
    return mois;
}

void Date::setMois(int mois) {
    this->mois = mois;
}

int Date::getAn() const {
    return an;
}

void Date::setAn(int an) {
    this->an = an;
}

int Date::getHeure() const {
    return heure;
}

void Date::setHeure(int heure) {
    this->heure = heure;
}

int Date::getMinute() const {
    return minute;
}

void Date::setMinute(int minute) {
    this->minute = minute;
}

Date::Date(int j, int m, int an, int heure, int minute) : jour(j), mois(m), an(an), heure(heure), minute(minute) {

}
