//
// Created by remil on 15/12/2021.
//

#include "CarteAction.h"

CarteAction::CarteAction(TypeAttaque type) : type(type) {

}

CarteAction::CarteAction() {

}

TypeAttaque CarteAction::getType() const {
    return this->type;
}