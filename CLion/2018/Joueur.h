//
// Created by remil on 15/12/2021.
//

#ifndef INC_2018_JOUEUR_H
#define INC_2018_JOUEUR_H

#include "CarteAction.h"
#include "Carte.h"
#include "Jeu.h"
#include <iostream>
using namespace std;

#define TAILLE_MAX 100

class Joueur {

    string nom;
    int nbPoint = 0;

    int nbElemCarte;
    int nbElemAttaques;

    int tailleAttaques;
    int tailleCartes;

    Jeu & refJeu;
    Carte ** cartes;
    CarteAction ** attaques;

public:
    Joueur(string name, Jeu &, int tailleAttaques, int tailleCartes;
    Joueur(const Joueur & copie);
    Joueur & operator=(const Joueur & joueur);
    ~Joueur();
    void operator+(Carte * ca);
    void ajouterCarteAction(CarteAction * ca);
    void retirerAttaque(const TypeAttaque & ta);
    void afficherCartes() const;
    friend ostream& operator<<(ostream& os, const Joueur & joueur);

    CarteAction ** getAttaques() const;
    int getNbAttaques() const;
};


#endif //INC_2018_JOUEUR_H
