%1 Analyse spectrale d�une matrice

%Matrice A
A = [-1 2 0 ; 0 -2 1 ; 0 2 -1];

%Calcul des valeurs propres
ValProps = eig(A);


%Calcul des vecteurs propres(T) valeurs propres(D)
[T, D] = eig(A);

%Le rank de la famille des vecteurs propres
rank(T);
%Le r�sultat est 3. Donc la dimension de l'espace vectoriel engendr� par
%les vecteurs propres est 3. Or l'endomorphisme �tudi� est de dimension 3.
%Ainsi cet espace vectoriel forme une base de ce dernier

%2 Polyn�me caract�ristique

%Polyn�me caract�ristique de A
PolyCar = poly(A);

%Vecteur contenant toutes les valeurs contenues entre?5 et 2 avec un pas de 0.1
Vect = (-5:0.1:2);

%Valeur prise par le polyn�me caract�ristique deAen ces points.
Result = polyval(PolyCar, Vect);


%Tracer ce polyn�me sur l�horizon [?5; 2] et afficher le quadrillage.
plot(Vect, Result), grid;

%V�rifier que les valeurs propres annulent le polyn�me caract�ristique de A
polyval(PolyCar, diag(D))
%Les r�sultats sont bien 0

%Calculer les racines du polyn�me caract�ristique de A avec la fonction appropri�e
Racines = roots(PolyCar);

%V�rifier queAest solution de son polyn�me caract�ristique.
polyvalm(PolyCar,A);
%Le r�sultat est bien une matrice nulle


%3 Analyse de commandabilit�
%Saisir ces matrices sous Matlab.
A = [-1 2 0 ; 0 -2 1 ; 0 2 -1];
B1 = [0 ; 0 ; 1];
B2 = [1 ; 0 ; 0];

%Quelle est la dimension du vecteur d��tat ?
%La dimension de ce vecteur doit �tre : 3

%Quelle est la dimension du vecteur de commande U si B=B1? Et si B= [B1B2] ?
%Si B = B1, la dimension de ce vecteur doit �tre : 1
%Si B = [B1B2], la dimension de ce vecteur doit �tre : 2

%calculer les matrices de commandabilit� correspondantes.
CoB1 = ctrb(A, B1);
%rank(CoB1) = 3
CoB2 = ctrb(A, B2);
%rank(CoB2) = 1
CoB1B2 = ctrb(A, [B1, B2]);
%rank(CoB1B2) = 3
%Or dim(X) = 3
%Donc les matrices de commande rendant le mod�le commandable sont CoB1 et
%CoB1B2

%4 Analyse d�observabilit�
%Saisir ces matrices sous Matlab.
C1 = [1 0 -2];
C2 = [0 0 1];

%Si C = C1 : dim(Y) = 1
%dim(X) = 3
%Si C = [C1 ; C2] : dim(Y) = 2
%dim(X) = 3

%Calculer les matrices d�observabilit� correspondantes.
ObC1 = obsv(A, C1);
%rank(ObC1) = 3
ObC2 = obsv(A, C2);
%rank(ObC2) = 2
ObC1C2 = obsv(A, [C1 ; C2]);
%rank(ObC1C2) = 3
%Or dim(X) = 3
%Donc les matrices d'observabilit� rendant le mod�le observable sont ObC1 et
%ObC1C2

%5 Mod�le dans l�espace d��tat
%Qu�ont ces 4 mod�les en commun ? Quelles sont les hypoth�ses qui ont �t� retenues ?
%Ces mod�les ont les m�mes variables d'�tat et les m�mes sorties

%Quelles sont les diff�rences entre ces 4 mod�les ? Comment interpr�ter ces diff�rences ?
%Les mod�les ont les matrices d'observation et de commandes qui diff�rent

%Cr�er dans Matlab ces 4 mod�les lin�aires invariants dans l�espace d��tat.
M1 = ss(A, B1, C1, 0);
M2 = ss(A, B1, C2, 0);
M3 = ss(A, B2, C1, 0);
M4 = ss(A, B1, C2, 0);

%Calculer les p�les et les z�ros de ces mod�les.
pole1 = pole(M1);
pole2 = pole(M2);
pole3 = pole(M3);
pole4 = pole(M4);
zero1 = zero(M1);
zero2 = zero(M2);
zero3 = zero(M3);
zero4 = zero(M4);

%Quels sont les mod�les asymptotiquement stables ?
%Les mod�les M1, M2 et M4 ont leurs z�ros


%6 Fonction de transfert
%Cr�er les 4 fonctions de transfert associ�es aux 4 mod�les pr�c�dents.
tf1 = tf(M1);
tf2 = tf(M2);
tf3 = tf(M3);
tf4 = tf(M4);

%Quels sont les ordres de ces fonctions de transfert ? Comment interpr�ter ce r�sultat ?
%ordre de tf1 : 3
%ordre de tf2 : 2
%ordre de tf3 : 1
%ordre de tf4 : 2
%Plus l'ordre est �lev�, moins on a de perte d'information.

%Calculer les p�les et les z�ros de ces fonctions de transfert.
poletf1 = pole(tf1);
poletf2 = pole(tf2);
poletf3 = pole(tf3);
poletf4 = pole(tf4);
zerotf1 = zero(tf1);
zerotf2 = zero(tf2);
zerotf3 = zero(tf3);
zerotf4 = zero(tf4);

%Quelles sont les fonctions de transfert stables entr�e born�e / sortie born�e ?
%tf3 est la seule fonction de transfert qui a tous ses p�les n�gatifs donc
%elle est stable entr�e born�e / sortie born�e (stable EBSB).