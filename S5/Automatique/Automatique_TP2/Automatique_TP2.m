Km = 10;
Tm = 0.3;
Kr = 1/9;
Ks = 10;
Kg = 0.105;
K1 = 2.4;
K2 = 0.76;
K3 = 2;

%Transfer Fcn
num = Km;
den = [Tm 1];
tf1 = tf(num,den);

%Integrateur
num1 = 1;
den1 = [1 0];
integrateur = tf(num1, den1);

sys1 = feedback(tf1, Kg*K2);
sys2 = feedback(sys1*Kr*integrateur*Ks, K1);

sys=K3*sys2;

bode(sys),grid;