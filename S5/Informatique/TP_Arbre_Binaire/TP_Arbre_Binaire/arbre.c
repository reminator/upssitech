#include <stdio.h>
#include <stdlib.h>
#include "file_dynamique.h"

/**
 * 7. (a) On ne peux pas transformer la fonction en fonction itérative directement.
 * (b) Je propose d'utiliser une file dynamique qui va permettre de stocker tous les éléments dans un tableau puis de les afficher.
 **/
void affiche_ARBRE(ARBRE arbre) {

    if (ARBRE_vide(arbre)) {
        return;
    }

    int nbElement = 0;
    CELLULE_FILE * ptr_c = malloc(sizeof(CELLULE_FILE));

    MA_FILE f = init_FILE();
    MA_FILE f2 = init_FILE();
    f = enFILE(f, arbre);
    f2 = enFILE(f2, arbre);

    while (!FILE_estVide(f)) {
        f = deFILE(f, &arbre);
        if (arbre->gauche != NULL) {
            f = enFILE(f, arbre->gauche);
            f2 = enFILE(f2, arbre->gauche);
        }
        if (arbre->droite != NULL) {
            f = enFILE(f, arbre->droite);
            f2 = enFILE(f2, arbre->droite);
        }
        nbElement++;
    }

    ELEMENT dernier = 0;
    int flag_dernier = 0;
    while(nbElement > 0) {
        ptr_c = f2.tete;
        ELEMENT min = 0;
        int flag_min = 0;

        while(ptr_c != NULL) {
            if (( !flag_min || compare_ELEMENT(min, ptr_c->element->element) > 0 ) && ( !flag_dernier || compare_ELEMENT(ptr_c->element->element, dernier) > 0 )) {
                min = ptr_c->element->element;
                flag_min = 1;
            }
            ptr_c = ptr_c->suivant;
        }
        affiche_ELEMENT(min);
        printf(" ");
        dernier = min;
        flag_dernier = 1;
        nbElement--;
    }

}

/* Question 3
void affiche_ARBRE(ARBRE arbre) {
    if (!ARBRE_vide(arbre)) {
        affiche_ARBRE(arbre->gauche);
        affiche_ELEMENT(arbre->element);
        printf(" ");
        affiche_ARBRE(arbre->droite);
    }
}*/

ARBRE init_ARBRE() {
    return NULL;
}

int ARBRE_vide(ARBRE arbre) {
    return arbre == NULL;
}

void ajout_ds_ARBRE(ARBRE * ptr_arbre, ELEMENT element) {

    if (ARBRE_vide(*ptr_arbre)) {
        *ptr_arbre = malloc(sizeof(CELLULE));
        (*ptr_arbre)->gauche = NULL;
        (*ptr_arbre)->droite = NULL;
        affect_ELEMENT(&((*ptr_arbre)->element), element);

    } else {
        CELLULE * ptr_cellule_prec = NULL;
        CELLULE * ptr_cellule_emplacement = *ptr_arbre;
        int plus_grand;

        while(!ARBRE_vide(ptr_cellule_emplacement)) {
            ptr_cellule_prec = ptr_cellule_emplacement;
            int compare = compare_ELEMENT(ptr_cellule_emplacement->element, element);

            if (compare > 0) {
                ptr_cellule_emplacement = ptr_cellule_emplacement->gauche;
                plus_grand = 0;
            } else if (compare < 0) {
                ptr_cellule_emplacement = ptr_cellule_emplacement->droite;
                plus_grand = 1;
            } else {
                printf("L'élément est déjà dans l'arbre.\n");
                return;
            }
        }
        
        ptr_cellule_emplacement = malloc(sizeof(CELLULE));
        if (plus_grand) {
            ptr_cellule_prec->droite = ptr_cellule_emplacement;
        } else {
            ptr_cellule_prec->gauche = ptr_cellule_emplacement;
        }

        ptr_cellule_emplacement->prec = ptr_cellule_prec;
        ptr_cellule_emplacement->gauche = NULL;
        ptr_cellule_emplacement->droite = NULL;
        affect_ELEMENT(&(ptr_cellule_emplacement->element), element);
    }
}

ELEMENT enlever_min_de_ARBRE(ARBRE * ptr_arbre) {
    if (ARBRE_vide(*ptr_arbre)) {
        printf("Arbre vide.\n");
    } else {
        CELLULE * ptr_cellule_prec = NULL;
        CELLULE * ptr_cellule_emplacement = *ptr_arbre;
        ELEMENT e;
        
        while (ptr_cellule_emplacement->gauche != NULL) {
            ptr_cellule_prec = ptr_cellule_emplacement;
            ptr_cellule_emplacement = ptr_cellule_emplacement->gauche;
        }
        e = ptr_cellule_emplacement->element;
        if (ptr_cellule_prec != NULL) {
            ptr_cellule_prec->gauche = ptr_cellule_emplacement->droite;
        } else {
            *ptr_arbre = (*ptr_arbre)->droite;
        }
        free(ptr_cellule_emplacement);
        return e;
    }
}