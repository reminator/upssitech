#include <stdio.h>
#include <stdlib.h>
#include "file_dynamique.h"

MA_FILE init_FILE() {
    MA_FILE f;
    f.tete = NULL;
    f.queue = NULL;
}
/*
void affiche_FILE(MA_FILE f) {
        printf("[");
        CELLULE_FILE * ptr_cellule = f.tete;
        while (ptr_cellule != NULL) {
                affiche_ELEMENT(ptr_cellule->element);
                if (ptr_cellule->suivant != NULL) {
                        printf(" ; ");
                }
                ptr_cellule = ptr_cellule->suivant;
        }
        printf("]\n");
}*/

int FILE_estVide(MA_FILE f) {
    return f.tete == NULL;
}

MA_FILE enFILE(MA_FILE f, ARBRE arbre) {
    CELLULE_FILE * ptr_newQueue = malloc(sizeof(CELLULE_FILE));
    ptr_newQueue->element = arbre;

    if (FILE_estVide(f)) {
        f.tete = ptr_newQueue;
        f.queue = ptr_newQueue;
    } else {
        f.queue->suivant = ptr_newQueue;
        f.queue = ptr_newQueue;
    }
    return f;
}

MA_FILE deFILE(MA_FILE f, ARBRE * arbre) {
    if (FILE_estVide(f)) {
        printf("Séance vide.\n");
        return f;
    }
    CELLULE_FILE * ptr_oldCell = f.tete;
    *arbre = f.tete->element;
    f.tete = f.tete->suivant;
    if (f.tete == NULL) {
        f.queue = NULL;
    }
    free(ptr_oldCell);
    return f;
}

/*
MA_FILE saisir_FILE() {
        MA_FILE f = init_FILE();
        ELEMENT e;
        char c2;
        int nbElements;
        do {
                printf("Combien de clients dans la séance ?\n");
                scanf("%d", &nbElements);
                while ((c2 = getchar()) != '\n' && c2 != EOF) { }
        } while (nbElements <= 0);

        for (int i=0; i<nbElements; i++) {
                printf("\n\n***Saisir un client. Nombre restant : %d***\n", nbElements - i);
                e = saisir_ELEMENT();
                f = enFILE(f, e);
        }
        return f;
}
*/