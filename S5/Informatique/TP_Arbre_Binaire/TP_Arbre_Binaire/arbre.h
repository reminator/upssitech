#include <stdio.h>
#include <stdlib.h>
#include "element.h"

typedef struct Arbre {
    ELEMENT element;
    struct Arbre * prec;
    struct Arbre * gauche;
    struct Arbre * droite;
} CELLULE, * ARBRE;

ARBRE init_ARBRE();
void affiche_ARBRE(ARBRE arbre);
int ARBRE_vide(ARBRE arbre);
void ajout_ds_ARBRE(ARBRE * arbre, ELEMENT element);
ELEMENT enlever_min_de_ARBRE(ARBRE * arbre);