#include <stdio.h>
#include <stdlib.h>
#include "arbre.h"

typedef struct C {
    ARBRE element;
    struct C * suivant;
} CELLULE_FILE;

typedef struct F {
    struct C * tete;
    struct C * queue;
} MA_FILE;

typedef MA_FILE SEANCE;

MA_FILE init_FILE();
void affiche_FILE(MA_FILE f);
int FILE_estVide(MA_FILE f);
MA_FILE enFILE(MA_FILE f, ARBRE arbre);
MA_FILE deFILE(MA_FILE f, ARBRE * arbre);
MA_FILE saisir_FILE();