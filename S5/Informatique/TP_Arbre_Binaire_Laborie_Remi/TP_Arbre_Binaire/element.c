#include <stdio.h>
#include <stdlib.h>
#include "element.h"

void affiche_ELEMENT(ELEMENT e) {
    printf("%d", e);
}

ELEMENT saisir_ELEMENT() {
    ELEMENT e;
    printf("Veuillez saisir la valeur de l'élément :\n");
    scanf("%d", &e);
    return e;
}

void affect_ELEMENT(ELEMENT * e1, ELEMENT e2) {
    *e1 = e2;
}

int compare_ELEMENT(ELEMENT e, ELEMENT e2) {
    return (e - e2);
}