#include <stdio.h>
#include <stdlib.h>

typedef int ELEMENT;

void affiche_ELEMENT(ELEMENT e);
ELEMENT saisir_ELEMENT();
void affect_ELEMENT(ELEMENT * e1, ELEMENT e2);
int compare_ELEMENT(ELEMENT e, ELEMENT e2);