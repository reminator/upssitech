#include <stdio.h>
#include <stdlib.h>
#include "arbre.h"

int main(void) {
    printf("Init de l'arbre\n");
    ARBRE arbre = init_ARBRE();

    printf("Affichage de l'arbre\n");
    printf("[ ");
    affiche_ARBRE(arbre);
    printf("]\n");

    printf("Ajout d'un élement\n");
    ELEMENT e1 = saisir_ELEMENT();
    ajout_ds_ARBRE(&arbre, e1);

    printf("Affichage de l'arbre\n");
    printf("[ ");
    affiche_ARBRE(arbre);
    printf("]\n");

    printf("Ajout d'un élement\n");
    ELEMENT e2 = saisir_ELEMENT();
    ajout_ds_ARBRE(&arbre, e2);
    

    printf("Affichage de l'arbre\n");
    printf("[ ");
    affiche_ARBRE(arbre);
    printf("]\n");

    printf("Ajout d'un élement\n");
    ELEMENT e3 = saisir_ELEMENT();
    ajout_ds_ARBRE(&arbre, e3);

    printf("Affichage de l'arbre\n");
    printf("[ ");
    affiche_ARBRE(arbre);
    printf("]\n");

    printf("Ajout d'un élement\n");
    ELEMENT e4 = saisir_ELEMENT();
    ajout_ds_ARBRE(&arbre, e4);

    printf("Affichage de l'arbre\n");
    printf("[ ");
    affiche_ARBRE(arbre);
    printf("]\n");

    printf("Ajout d'un élement\n");
    ELEMENT e6 = saisir_ELEMENT();
    ajout_ds_ARBRE(&arbre, e6);

    printf("Affichage de l'arbre\n");
    printf("[ ");
    affiche_ARBRE(arbre);
    printf("]\n");

    printf("Ajout d'un élement\n");
    ELEMENT e7 = saisir_ELEMENT();
    ajout_ds_ARBRE(&arbre, e7);

    printf("Affichage de l'arbre\n");
    printf("[ ");
    affiche_ARBRE(arbre);
    printf("]\n");

    printf("Ajout d'un élement\n");
    ELEMENT e8 = saisir_ELEMENT();
    ajout_ds_ARBRE(&arbre, e8);

    printf("Affichage de l'arbre\n");
    printf("[ ");
    affiche_ARBRE(arbre);
    printf("]\n");

    printf("Enlever min\n");
    e1 = enlever_min_de_ARBRE(&arbre);
    printf("Element enlevé : ");
    affiche_ELEMENT(e1);
    printf("\n");

    printf("Affichage de l'arbre\n");
    printf("[ ");
    affiche_ARBRE(arbre);
    printf("]\n");

    printf("Enlever min\n");
    e1 = enlever_min_de_ARBRE(&arbre);
    printf("Element enlevé : ");
    affiche_ELEMENT(e1);
    printf("\n");

    printf("Affichage de l'arbre\n");
    printf("[ ");
    affiche_ARBRE(arbre);
    printf("]\n");

    printf("Enlever min\n");
    e1 = enlever_min_de_ARBRE(&arbre);
    printf("Element enlevé : ");
    affiche_ELEMENT(e1);
    printf("\n");

    printf("Affichage de l'arbre\n");
    printf("[ ");
    affiche_ARBRE(arbre);
    printf("]\n");

    printf("Enlever min\n");
    e1 = enlever_min_de_ARBRE(&arbre);
    printf("Element enlevé : ");
    affiche_ELEMENT(e1);
    printf("\n");

    printf("Affichage de l'arbre\n");
    printf("[ ");
    affiche_ARBRE(arbre);
    printf("]\n");

    printf("Enlever min\n");
    e1 = enlever_min_de_ARBRE(&arbre);
    printf("Element enlevé : ");
    affiche_ELEMENT(e1);
    printf("\n");

    printf("Affichage de l'arbre\n");
    printf("[ ");
    affiche_ARBRE(arbre);
    printf("]\n");

    printf("Enlever min\n");
    e1 = enlever_min_de_ARBRE(&arbre);
    printf("Element enlevé : ");
    affiche_ELEMENT(e1);
    printf("\n");

    printf("Affichage de l'arbre\n");
    printf("[ ");
    affiche_ARBRE(arbre);
    printf("]\n");

    printf("Enlever min\n");
    e1 = enlever_min_de_ARBRE(&arbre);
    printf("Element enlevé : ");
    affiche_ELEMENT(e1);
    printf("\n");

    printf("Affichage de l'arbre\n");
    printf("[ ");
    affiche_ARBRE(arbre);
    printf("]\n");

    printf("Enlever min\n");
    e1 = enlever_min_de_ARBRE(&arbre);
    printf("Element enlevé : ");
    affiche_ELEMENT(e1);
    printf("\n");

    printf("Affichage de l'arbre\n");
    printf("[ ");
    affiche_ARBRE(arbre);
    printf("]\n");
}