% % %Récupération du signal audio
% [signal,fs] = audioread("essai.wav") ;
% info = audioinfo("essai.wav") ;
% % %fs contient la fréquence d'échantillonnage du signal
% % 
% % %Affichage des 10 premières variables du signal 
% signal(1:10);
% % 
% % %Détermination de l'intervalle de variation du signal
% valmin = min(signal);
% valmax = max(signal);
% % 
% % %Affichage du signal audio %
% %plot(signal);
% %On multiplie les valeurs par 2 puissance 15 pour avoir des valuers
% %entières (bits sur lesquels c'est codé -1)
% 
% % %Calcul de la durée du signal en secondes : nombre de points divisés par
% % la fréquence d'échantillonnage
% 
% duree = length(signal)/fs;
% 
% % %Affichage du signal avec la durée en secondes en abscisse
% plot((1 : length(signal))/fs , signal);


[signalEssai,fsEssai] = audioread('essai.wav') ;
info = audioinfo('essai.wav') ;
parole = signalEssai(11300:12323) ;

figure;
subplot(411);
plot(signalEssai);
subplot(412);
plot((1:length(signalEssai))/fsEssai, signalEssai);

[signalNote,fsNote] = audioread('note.wav') ;
info = audioinfo('note.wav') ;

spectreNote = abs(fft(signalNote));

figure;
subplot(211);
plot((1:length(signalNote))/fsEssai, signalNote);
subplot(212);
plot(((1:length(signalNote))/length(signalNote))*fsNote, spectreNote);



spectreParole = abs(fft(parole));
cepstreParole = abs(fft(log(spectreParole)));

figure;
subplot(311);
% Extrait en secondes
plot((1:length(parole))/fsEssai, parole);
subplot(312);
% Demi spectre en Hertz
plot(((1:length(parole)/2)/length(parole))*fsEssai, spectreParole(1:length(spectreParole)/2));
subplot(313);
% Demi cepstre en Hertz
plot((1:length(parole))/fsEssai, cepstreParole);

figure ;
specgram(signalEssai,1024,fsEssai,hamming(1024)) ;



% [signal, fs] = audioread('Source\essai.wav');
% subplot(311);% = plot(3,1,1);
% plot((1:length(signal))/fs, signal); 
% title('signal');
% ylabel('hertz');
% xlabel('time');
% 
% signal2 = fft(signal);
% spectre = abs(signal2);
% %plot(signal2);
% subplot(312);
% plot((1:length(signal))/length(signal)*fs,spectre);
% title('spectre');
% ylabel('puissance');
% xlabel('frequence');
% extrait = signal(11300:12323) ;
% h=real(fft(log(abs(fft(extrait.*hamming(1024)))))) ;
% subplot(313);
% plot(h);
% title('cepstre');
% ylabel('puissance');
% xlabel('mel');
% 

