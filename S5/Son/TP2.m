fe = 8000; % nb points par secondes
Nnote = 8000; % nb points pour une note
Nsilence = 800; % nb points pour un silence

tNote = (1:Nnote)/fe;
tSilence = (1:Nsilence)/fe;

fDo = 264;
fMi = 330;
fSol = 396;
fDo4 = 528;
fSilence = 0;

do = sin(2*pi*fDo*tNote);
mi = sin(2*pi*fMi*tNote);
sol = sin(2*pi*fSol*tNote);
do4 = sin(2*pi*fDo4*tNote);
silence = sin(2*pi*fSilence*tSilence);

signal = [silence, do, silence, mi, silence, sol, silence, do4, silence];
soundview(signal, fe);
%audiowrite('signal.wav', signal, fe);

s = spectro(signal, 1024, 512);
image(0:0.1:(length(signal)/fe), 0:fe/2, s);
disp(length(signal/fe));
set(gca, 'Ydir', 'normal');