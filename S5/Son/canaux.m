% en=canaux(Signal,n1,lgsig,Fs,nfilt,titre)
% - Signal est le vecteur total de signal echantillonne a Fs KHz
% - n1 est l'echantillon de debut de trame, de longueur lgsig
% - nfilt est le nombre de canaux visualises
% - titre est le titre de la fenetre (et oui !)
% - en correspond a l'energie
%
% Exemple : en=canaux(sig,1,512,16,24,'titre1')

function en=canaux(Signal,n1,lgsig,Fs,nfilt,titre)

n2=n1+lgsig-1;
signal = Signal(n1:n2);
% presignal=conv(signal,[1,-1]);
presignal = signal;
%figure;
%plot(presignal);
fen=hamming(lgsig);
fensignal=presignal(1:lgsig).*fen;
spect=fft(fensignal,lgsig);

nspect=abs(spect);
lgsig1=lgsig/2;

%calcul des nfilt filtres triangulaires a partir des frequences de coupure (echelle Mel)
%

fcoup=[0,100,200,300,400,500,600,700,800,900,1000,1150,1300,1500,1700,2000,2350,2700,3100,3550,4000,4500,5050,5600,6200,6850,7500];

x2=zeros(1,lgsig1);
x1=zeros(1,lgsig1);

for i=1:lgsig1
	nm(i)=0;
	x1(i)=0;
	f=Fs*500.*i/lgsig1;
	for n=1:(nfilt+1)
		if((f>=fcoup(n)) && (f<fcoup(n+1)))
		nm(i)=n;
		x1(i)=(f-fcoup(n))/(fcoup(n+1)-fcoup(n));
		x2(i)=1-x1(i);
		end
	end
end

x2sig=x2.*nspect(1:lgsig1)';
x1sig=x1.*nspect(1:lgsig1)';
figure;
subplot(311);
plot([11300:11811],signal);
title(titre);
subplot(312)
plot([1:length(nspect)/2]/(length(nspect)/2)*(Fs*1000/2),nspect(1:length(nspect)/2));
subplot(313)

en=zeros(1,nfilt);

for i=1:(lgsig1-1)
i2=nm(i);
i1=i2-1;
if(i1 > 0)
	en(i1)=en(i1)+x2sig(i);
end
if ((i2 > 0) && (i2 <= nfilt))
	en(i2)=en(i2)+x1sig(i);
end
end

% plot(fcoup(1:nfilt),en);
stairs(fcoup(1:nfilt),en);
