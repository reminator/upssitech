function mat_spectro = spectro( signal, taille_fenetre, recouvrement )

    mat_spectro = [];
    for x=1 : recouvrement:length(signal)-taille_fenetre+1
        spectre = abs(fft(signal(x:x+taille_fenetre-1)));
        mat_spectro = [mat_spectro, spectre(1:taille_fenetre/2)'];
    end

end

