[signal,fs] = audioread('son.wav') ;
info = audioinfo('son.wav') ;
extrait = signal(47500:49547) ;
%disp(fs);


spectre = abs(fft(extrait));
cepstre = abs(fft(log(spectre)));

figure;
subplot(311);
plot((1:length(extrait))/fs, extrait);
title('extrait');
ylabel('valeur');
xlabel('ms');
subplot(312);
plot(((1:length(extrait)/2)/length(extrait))*fs, spectre(1:length(spectre)/2));
title('spectre');
ylabel('fr�quence');
xlabel('Hertz');
subplot(313);
plot((1:length(extrait)/2)/fs, cepstre(1:length(cepstre)/2));
title('cepstre');
ylabel('valeur');
xlabel('ms');



fe = 8000; % nb points par secondes
Nnote = 4000; % nb points pour une note
Nsilence = 800; % nb points pour un silence

tNote = (1:Nnote)/fe;
tSilence = (1:Nsilence)/fe;

VAL_DO3 = 262;
VAL_RE3 = 294;
VAL_MI3 = 330;
VAL_SI2 = 466/2;
VAL_LA2 = 440/2;
VAL_SOL2 = 392/2;
VAL_SILENCE = 0;

do = sin(2*pi*VAL_DO3*tNote);
re = sin(2*pi*VAL_RE3*tNote);
mi = sin(2*pi*VAL_MI3*tNote);
si = sin(2*pi*VAL_SI2*tNote);
la = sin(2*pi*VAL_LA2*tNote);
sol = sin(2*pi*VAL_SOL2*tNote);
silence = sin(2*pi*VAL_SILENCE*tSilence);

signal = [silence, do, silence, do, silence, do, silence, re, silence, mi, mi, silence, re, re, silence, do, silence mi, silence, re, silence, re, silence, do, do, silence, re, silence, re, silence, re, silence, re, silence, la, la, silence, la, la, silence, re, silence, do, silence, si, silence, la, silence, sol, sol, silence, do, silence, do, silence, do, silence, re, silence, mi, mi, silence, re, re, silence, do, silence, mi, silence, re, silence, re, silence, do, do, silence];
soundview(signal, fe);
audiowrite('melodie.wav', signal, fe);

figure;
s = spectro(signal, 10000, 512);
image((0:0.1:length(signal))*fe, 0:fe/2, s);
disp(length(signal/fe));
set(gca, 'Ydir', 'normal');