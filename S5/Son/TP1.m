[signal,fs] = audioread('essai.wav') ;
signal1 = signal;
fs1 = fs;
info = audioinfo('essai.wav') ;
parole = signal(11300:12323) ;

disp("La variable signal contient un tableau de 31743 double");
disp("La valeur de fs est 16000 et correspond au nombre de valeurs par secondes");
disp("La variable info contient plusieurs informations en rapport avec le fichier essai.wav. On peut retrouver la valeur de fs : info.SampleRate");

disp("Dix premi�res valeurs du signal: ");
for i=1:10
    disp(signal(i));
end

valMin = min(signal(1:length(signal)));
valMax = max(signal(1:length(signal)));

disp("Intervale de variation des �chantillons du sigal : [ " + valMin + " ; " + valMax + " ]");
disp("Pour ramener le vecteur � des valeurs enti�res, il faut multiplier les valeurs par 2^15");

disp("Dur�e du signal : " + (length(signal)/fs));
% Affichage du signal avec l'axe des abscisses en secondes
subplot(411);
plot((1:length(signal))/fs, signal);

[signal,fs] = audioread('note.wav') ;
info = audioinfo('note.wav') ;

%Signal note
subplot(412);
plot((1:length(signal))/fs, abs(signal));
%Spectre note
subplot(413);
plot(((1:length(signal))/length(signal))*fs, fft(signal));
% On trouve la premi�re harmonique � 440 Hz. C'est donc un la3.


spectreParole = fft(parole);    
subplot(414);
plot(((1:length(spectreParole)/2)/length(spectreParole))*fs, spectreParole(1:length(spectreParole)/2));

figure ;
subplot(511) ;
plot(parole) ;
subplot(512) ;
plot(hamming(1024)) ;
subplot(513) ;
plot(hamming(1024).*parole) ;
subplot(514) ;
f=abs(fft(parole));
plot(f(1:512)) ;
subplot(515) ;
h=abs(fft(parole.*hamming(1024))) ;
plot(h(1:512)) ;

% Le premier trac� est le signal de l'extrait
% Le deuxi�me es le fen�trage de Hamming
% Le troisi�me correspond au signal convolu� avec le fen�trage de Hamming
% Le quatri�me est la TTF du signal
% Le cinqui�me est la TTF du signal convolu� avec le fen�trage de Hamming

% la fen�tre de Hamming permet de r�duire les discontinuit� en pond�rant le
% signal par cette fen�tre, applatit aux extr�mit�s
% Il existe comme fen�tre : Hamming, Hanning, Triangulaire, Rectangulaire

figure ;
specgram(signal1,1024,fs,hamming(1024)) ;
% Ce fichier est compos� d�un bip, suivi de 4 chiffres : 0, 3, 8 et 0

en = canaux(signal1,11300,512,16,24,'Voyelle o') ;
% 1er trac� : signal
% 2eme trac� : spectre sur une demi-p�riode, avec les abscisses en Hertz
% 3eme trac� : �chelle MEL

figure;
subplot(311);
plot((1:length(parole))/fs1, parole);
subplot(312);
spectre = abs(fft(parole.*hamming(1024)));
plot(((1:length(spectre)/2)/length(spectre))*fs1, spectre(1:length(spectre)/2))  ;
subplot(313);
cepstre = abs(fft(log(spectre)));
plot(((1:length(cepstre)/2)/length(cepstre))*fs1, cepstre(1:length(cepstre)/2));

