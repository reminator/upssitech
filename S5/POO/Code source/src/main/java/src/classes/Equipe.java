package src.classes;

import src.enumerations.Action;
import src.enumerations.Direction;
import src.enumerations.Nature;
import src.enumerations.Status;
import src.interfaces.JHumain;
import src.interfaces.JIA;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Random;
import java.util.Scanner;

public abstract class Equipe implements JIA, JHumain {
    protected Status myStatut;
    protected final Nature myNature;
    protected int ident;

    private final Scanner sc;
    private final Random rd;
    protected final ArrayList<Navire> listeNavire;
    protected Commande myCommande;

    public Equipe(Nature nature) {
        this.listeNavire = new ArrayList<>();
        this.myNature = nature;
        this.rd = new Random();
        this.sc = new Scanner(System.in);
    }

    @Override
    public void tirageAleatoire() {
        int idNavire = rd.nextInt(listeNavire.size());
        idNavire = listeNavire.get(idNavire).getIdent();

        int idAction = rd.nextInt(2);
        if (myStatut == Status.NEUTRE && idAction == 1)
            idAction++;

        int direction = -1;
        if (idAction < 2)
            direction = rd.nextInt(4);
        System.out.println("Navire : " + idNavire);
        System.out.println("Action : " + Action.values()[idAction]);
        if (!Action.values()[idAction].equals(Action.PECHE))
            System.out.println("Direction : " + Direction.values()[direction]);
        this.myCommande = new Commande(this, idNavire, idAction, direction);
    }

    @Override
    public void interrogationParClavier() {
        String strNavire;
        String strAction;
        String strDirection = "";

        System.out.print("Veuillez saisir le numéro du navire qui va agir (de 0 à " + (this.listeNavire.size() - 1) + ") : ");
        int idNavire;
        try {
            idNavire = sc.nextInt();
        } catch (Exception e) {
            sc.next();
            idNavire = -1;
        }
        while (idNavire < 0 || idNavire > this.listeNavire.size()-1) {
            System.out.print("ERREUR DE SAISIE: Veuillez saisir le numéro du navire qui va agir (de 0 à " + (this.listeNavire.size() - 1) + ") : ");
            try {
                idNavire = sc.nextInt();
            } catch (Exception e) {
                sc.next();
                idNavire = -1;
            }
        }
        strNavire = String.valueOf(idNavire);

        Action action1 = Action.DEPLACEMENT;
        Action action2;
        if (this.myStatut == Status.MILITAIRE) {
            action2 = Action.TIR;
        } else {
            action2 = Action.PECHE;
        }
        System.out.print("Veuillez saisir l'action à faire (" + action1 + " ou " + action2 + ") : ");
        strAction = sc.next();
        if ("déplacement".equalsIgnoreCase(strAction)) {
            strAction = "deplacement";
        }
        if ("pêche".equalsIgnoreCase(strAction)) {
            strAction = "peche";
        }
        while (!action1.toString().equalsIgnoreCase(strAction) && !action2.toString().equalsIgnoreCase(strAction)) {
            System.out.print("ERREUR DE SAISIE : Veuillez saisir l'action à faire (" + action1 + " ou " + action2 + ") : ");
            strAction = sc.next();
            if ("déplacement".equalsIgnoreCase(strAction)) {
                strAction = "deplacement";
            }
            if ("pêche".equalsIgnoreCase(strAction)) {
                strAction = "peche";
            }
        }

        if (Action.TIR.toString().equalsIgnoreCase(strAction) || Action.DEPLACEMENT.toString().equalsIgnoreCase(strAction)) {
            ArrayList<Direction> directions = new ArrayList<>(Arrays.asList(Direction.values()));
            Direction direction1 = directions.get(0);
            Direction direction2 = directions.get(1);
            Direction direction3 = directions.get(2);
            Direction direction4 = directions.get(3);

            System.out.print("Veuillez saisir la direction dans laquelle agir (" + direction1 + ", " + direction2 + ", " + direction3 + ", " + direction4 + ") : ");
            strDirection = sc.next();
            while (true) {
                try {
                    Direction.valueOf(strDirection.toUpperCase());
                    break;
                } catch (IllegalArgumentException e) {
                    System.out.print("ERREUR DE SAISIE : Veuillez saisir la direction dans laquelle agir (" + direction1 + ", " + direction2 + ", " + direction3 + ", " + direction4 + ") : ");
                    strDirection = sc.next();
                }
            }
        }
        this.myCommande = new Commande(this, strNavire, strAction, strDirection);
    }

    @Override
    public Commande getMyCommande() {
        switch (this.myNature) {
            case HUMAIN -> this.interrogationParClavier();
            case IA -> this.tirageAleatoire();
        }
        return this.myCommande;
    }

    @Override
    public Status getStatus() {
        return this.myStatut;
    }

    @Override
    public void addNavire(Navire navire) {
        this.listeNavire.add(navire);
    }

    @Override
    public ArrayList<Navire> getListeNavire() {
        return new ArrayList<>(this.listeNavire);
    }

    @Override
    public int getId() {
        return this.ident;
    }

    @Override
    public Nature getNature() {
        return this.myNature;
    }

    @Override
    public String toString() {
        return "Équipe " + this.ident + " (" + this.myStatut + ", " + this.myNature + "), avec " + this.listeNavire;
    }
}
