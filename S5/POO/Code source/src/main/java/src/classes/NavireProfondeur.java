package src.classes;

import src.enumerations.TypeNav;

public abstract class NavireProfondeur extends Navire {
    public NavireProfondeur(int rang, TypeNav typeNav, int numEquipe) {
        super(rang, typeNav, numEquipe);
    }
}
