package src.classes;

import src.enumerations.TypeNav;

import java.util.ArrayList;

public class CasePlateau {

    private final ArrayList<Navire> lesOccupants;

    public CasePlateau() {
        this.lesOccupants = new ArrayList<>();
    }

    boolean estPleine() {
        return this.lesOccupants.size() >= 2;
    }

    boolean estOccupee(TypeNav typeNav) {
        if (this.estPleine()) {
            return true;
        }
        if (this.lesOccupants.isEmpty()) {
            return false;
        }
        if (typeNav == TypeNav.SOUSMARIN) {
            return this.lesOccupants.get(0).getType() == TypeNav.SOUSMARIN;
        }
        return this.lesOccupants.get(0).getType() != TypeNav.SOUSMARIN;
    }

    public ArrayList<Navire> getLesOccupants() {
        return new ArrayList<>(this.lesOccupants);
    }

    public void addUnOccupant(Navire nouvelOccupant) {
        // TODO Exceptions
        this.lesOccupants.add(nouvelOccupant);
    }

    public void removeUnOccupant(Navire n) {
        this.lesOccupants.remove(n);
    }

    @Override
    public String toString() {
        String strTop = "       ";
        String strBot = "       ";
        for (Navire navire : this.getLesOccupants()) {
            if (navire.getType().equals(TypeNav.SOUSMARIN)) {
                strBot = "Eq." + navire.getNumEq() + ",N" + navire.getIdent();
            } else {
                strTop = "Eq." + navire.getNumEq() + ",N" + navire.getIdent();
            }
        }
        return strTop + "|" + strBot;
    }
}
