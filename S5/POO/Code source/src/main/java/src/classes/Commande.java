package src.classes;

import src.enumerations.Action;
import src.enumerations.Direction;

public class Commande {

    private int idNavire;
    private Action action;
    private Direction direction;
    private final Equipe equipe;

    public Commande(Equipe equipe, int idNavire, int idAction, int idDirection) {
        this.equipe = equipe;
        this.setIdNavire(idNavire);
        this.setActionChoisie(Action.values()[idAction]);
        if (!this.action.equals(Action.PECHE))
            this.setDirectionChoisie(Direction.values()[idDirection]);
    }

    public Commande(Equipe equipe, String strNavire, String strAction, String strDirection) {
        this.equipe = equipe;
        this.idNavire = Integer.parseInt(strNavire.toUpperCase());
        this.action = Action.valueOf(strAction.toUpperCase());
        if (!this.action.equals(Action.PECHE))
            this.direction = Direction.valueOf(strDirection.toUpperCase());
    }

    public Equipe getEquipe() {
        return equipe;
    }

    public int getIdNavire() {
        return idNavire;
    }

    public void setIdNavire(int idNavire) {
        this.idNavire = idNavire;
    }

    public Action getActionChoisie() {
        return action;
    }

    public void setActionChoisie(Action action) {
        this.action = action;
    }

    public Direction getDirectionChoisie() {
        return direction;
    }

    public void setDirectionChoisie(Direction direction) {
        this.direction = direction;
    }

    @Override
    public String toString() {
        return "Commande{" +
                "idNavire=" + idNavire +
                ", action=" + action +
                ", direction=" + direction +
                ", equipe=" + equipe +
                '}';
    }
}
