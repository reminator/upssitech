package src.classes;

import src.enumerations.TypeNav;

public class Destroyer extends NavireSurface {

    public Destroyer(int rang, int numEquipe) {
        super(rang, TypeNav.DESTROYER, numEquipe);
        this.vitesse = 1;
        this.portee = 2;
    }

    @Override
    public String toString() {
        return "Destroyer{" +
                "ident=" + ident +
                ", myType=" + myType +
                ", strAffichage='" + strAffichage + '\'' +
                ", numEq=" + numEq +
                ", etat=" + etat +
                ", portee=" + portee +
                ", vitesse=" + vitesse +
                ", position=" + position +
                '}';
    }
}
