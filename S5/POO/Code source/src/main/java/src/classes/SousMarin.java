package src.classes;

import src.enumerations.TypeNav;

public class SousMarin extends NavireProfondeur {
    public SousMarin(int rang, int numEquipe) {
        super(rang, TypeNav.SOUSMARIN, numEquipe);
        this.vitesse = 2;
        this.portee = 1;
    }

    public void setEndommage() {
        this.etat = 1;
    }

    @Override
    public String toString() {
        return "SousMarin{" +
                "ident=" + ident +
                ", myType=" + myType +
                ", strAffichage='" + strAffichage + '\'' +
                ", numEq=" + numEq +
                ", etat=" + etat +
                ", portee=" + portee +
                ", vitesse=" + vitesse +
                ", position=" + position +
                '}';
    }
}
