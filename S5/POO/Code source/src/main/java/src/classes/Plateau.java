package src.classes;

import java.awt.*;

public class Plateau {

    private final CasePlateau[][] matrice;
    private final int myTaille;

    public Plateau(int taille) {
        this.myTaille = taille;
        matrice = new CasePlateau[taille][taille];
        for (int i = 0; i < taille; i++) {
            for (int j = 0; j < taille; j++) {
                matrice[i][j] = new CasePlateau();
            }
        }
    }

    public int getTaille() {
        return this.myTaille;
    }

    public CasePlateau getCasePlateau(int ligne, int colonne) {
        return this.matrice[ligne][colonne];
    }

    public CasePlateau getCasePlateau(Point point) {
        return this.matrice[point.x][point.y];
    }

    @Override
    public String toString() {
        StringBuilder affichage = new StringBuilder();

        affichage.append("|");
        affichage.append("-------|".repeat(Math.max(0, this.myTaille)));
        affichage.append("\n");

        for (int i = 0; i < this.myTaille; i++) {
            affichage.append("|");
            for (int j = 0; j < this.myTaille; j++) {
                affichage.append(this.getCasePlateau(i, j).toString().split("\\|")[0]);
                affichage.append("|");
            }
            affichage.append("\n");

            affichage.append("|");
            for (int j = 0; j < this.myTaille; j++) {
                affichage.append(this.getCasePlateau(i, j).toString().split("\\|")[1]);
                affichage.append("|");
            }
            affichage.append("\n");

            affichage.append("|");
            affichage.append("-------|".repeat(this.myTaille));
            affichage.append("\n");
        }

        return affichage.toString();
    }

    public void affichage() {
        System.out.println(this.toString());
    }
}
