package src.classes;

import src.enumerations.Nature;
import src.enumerations.Status;

public class EqPecheur extends Equipe {

    public EqPecheur(int idEquipe, Nature nature) {
        super(nature);
        this.ident = idEquipe;
        this.myStatut = Status.NEUTRE;
    }
}
