package src.classes;

import src.enumerations.Nature;
import src.enumerations.Status;

public class EqBataillon extends Equipe {

    public EqBataillon(int idEquipe, Nature nature) {
        super(nature);
        this.ident = idEquipe;
        this.myStatut = Status.MILITAIRE;
    }
}
