package src.classes;

import src.enumerations.TypeNav;

public abstract class NavireSurface extends Navire {

    public NavireSurface(int rang, TypeNav t, int numEquipe) {
        super(rang, t, numEquipe);
    }
}
