package src.interfaces;

import src.classes.Commande;
import src.classes.Navire;
import src.enumerations.Nature;
import src.enumerations.Status;

import java.util.ArrayList;

public interface Joueur {

    Commande getMyCommande();
    Status getStatus();
    void addNavire(Navire navire);
    ArrayList<Navire> getListeNavire();
    int getId();
    Nature getNature();
}
