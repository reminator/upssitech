package src.classes;

import src.enumerations.Direction;
import src.enumerations.Nature;
import src.enumerations.Status;
import src.enumerations.TypeNav;
import src.exceptions.LimiteException;
import src.exceptions.OccupException;
import src.interfaces.Joueur;

import java.awt.*;
import java.util.ArrayList;
import java.util.Random;
import java.util.Scanner;

public class Jeu {

    public static void main(String[] args) {
        try {
            Jeu jeu = new Jeu(5);
            jeu.jouer();
        } catch (Exception e) {
            System.out.println(e);
            System.out.println("******************************************\nErreur imprévu, contactez le créateur du jeu");
        }
        System.out.println("<Appuyez sur Entrée pour quitter>");
        Scanner sc = new Scanner(System.in);
        sc.nextLine();
    }

    private boolean fini;
    private final ArrayList<Joueur> listeJoueurs;
    private final Random alea;
    private final Plateau lePlateau;
    private final Scanner sc;

    public Jeu(int taille) {
        this.fini = false;
        this.listeJoueurs = new ArrayList<>();
        this.alea = new Random();
        this.lePlateau = new Plateau(taille);
        this.sc = new Scanner(System.in);
    }

    public void jouer() {
        afficheRegles();
        System.out.println("<Appuyez sur Entrée pour jouer>");
        sc.nextLine();
        System.out.println("Veuillez patienter svp");
        for (int i = 0; i < 50; ++i) System.out.println();
        choixJoueur();
        attributionNavire();
        positionnementNavire();
        while (!fini) {
            for (Joueur joueur : this.listeJoueurs) {
                for (Joueur joueur2 : this.listeJoueurs) {
                    System.out.println("L'équipe " + joueur2.getId() + " est " + joueur2.getStatus() + " de nature " + joueur2.getNature());
                }
                this.lePlateau.affichage();
                System.out.println("Équipe " + joueur.getId() + ". Choisis ton action.");
                majJeuAvCommande(joueur.getMyCommande());
                System.out.println("\n");
                fini = majListeNavire();
                if (fini) {
                    break;
                }
                System.out.println("<Appuyez sur Entrée pour passer au tour suivant>");
                sc.nextLine();
                System.out.println("Veuillez patienter svp");
                for (int i = 0; i < 50; ++i) System.out.println();
            }
        }
        finDePartie();
    }

    private void afficheRegles() {
        System.out.println("""
                *****************
                * Règles du jeu *
                *****************

***************
* Les Équipes *
***************

Le jeu est composé de 3 équipes.
Chaque équipe est composée de navires identifiables par leur identifiant.
Il y a 2 équipes militaires et une neutre.
Les équipes militaires sont équipées de destroyers.
ainsi que de sous-marins.
L'équipe neutre aura à sa disposition des chalutiers.

Deux équipes sont contrôlées par des humains,
tandis que la troisième est contrôlée par une Intelligence Artificielle.

Chaque équipe joue à son tour une action parmi 2 :
- Se déplacer
- Tirer/Pêcher (en fonction du status de l'équipe)

***************
* Les actions *
***************

- Se déplacer
    Le joueur indique la direction dans laquelle se déplacer.
    Le navire se déplace ainsi du nombre de case qu'indique sa vitesse.
    Le navire s'arrête en cours de déplacement s'il se retrouve bloqué contre un autre navire ou les limites du plateau.

- Tirer
    Le joueur indique la direction dans laquelle tirer.
    Le navire tire sur le navire le plus proche (surface ou profondeur) dans la direction souhaité.
    Le navire touché coule directement.
    Si aucun navire ne se trouve dans sa porté de tir, le tir ne touche personne.
    Si un navire de surface est touché, il coule et emporte avec lui un navire de profondeur s'il y en a un en dessous.

- Pêcher
    Le navire utilise un chalut pour pêcher.
    Si un navire de profondeur se trouve en dessous, celui-ci ne peux plus se déplacer jusqu'à la fin de la partie.

***************
* Les navires *
***************

- Chalutier
    Navire de surface
    Statut neutre (ne peut pas tirer)
    Vitesse : 2
    
- Destroyer
    Navire de surface
    Statut militaire (ne peut pas pêcher)
    Vitesse : 1
    Portée de tir : 2
    
- Sous-marin
    Navire de surface
    Statut militaire
    Vitesse : 2
    Porté de tir : 1
    

*********************
* Le plateau de jeu *
*********************

Le plateau de jeu est composé d'un certain nombre de cases.
Ses cases contiennent une partie haute et une partie basse.
La partie haute peut contenir un navire de surface et la partie basse, un navire de profondeur.

********************
* Fin de la partie *
********************

La partie se termine lorsqu'une équipe n'a plus de navire (tous les navires coulés).
Le gagnant est l'équipe aillant le plus de navires capables de se déplacer.
En cas d'égalité, il y a plusieurs gagnants.
""");
    }

    private void finDePartie() {
        System.out.println("Fin de la partie !!!");
        int[] scores = new int[this.listeJoueurs.size()];

        for (int i = 0; i < this.listeJoueurs.size(); i++) {
            for (Navire navire : this.listeJoueurs.get(i).getListeNavire()) {
                if (navire.estValide()) {
                    scores[i]++;
                }
            }
        }

        ArrayList<Joueur> gagnants = new ArrayList<>();
        for (Joueur joueur : this.listeJoueurs) {
            if (gagnants.isEmpty()) {
                gagnants.add(joueur);
            } else {
                if (scores[gagnants.get(0).getId()] < scores[joueur.getId()]) {
                    gagnants.clear();
                    gagnants.add(joueur);
                } else if (scores[gagnants.get(0).getId()] == scores[joueur.getId()]) {
                    gagnants.add(joueur);
                }
            }
        }
        if (gagnants.size() > 1) {
            System.out.println("Égalité !!");
            System.out.print("Les gagnants sont ");
            for (Joueur joueur : gagnants) {
                if (joueur.getId() == gagnants.get(gagnants.size()-1).getId()) {
                    System.out.println("et l'équipe " + joueur.getId() + " !!");
                } else {
                    System.out.print("l'équipe " + joueur.getId() + ", ");
                }
            }
        } else {
            System.out.println("Le gagnant est l'équipe " + gagnants.get(0).getId() + " !!");
        }
    }

    private boolean majListeNavire() {
        for (Joueur joueur : this.listeJoueurs) {
            boolean vivant = false;
            for (Navire navire : joueur.getListeNavire()) {
                if (!navire.estCoule()) {
                    vivant = true;
                    break;
                }
            }
            if (!vivant) {
                return true;
            }
        }
        return false;
    }

    private void majJeuAvCommande(Commande myCommande) {
        Joueur joueur = myCommande.getEquipe();
        int idNavire = myCommande.getIdNavire();
        Navire navire = joueur.getListeNavire().get(idNavire);

        if (!navire.estCoule()) {
            switch (myCommande.getActionChoisie()) {
                case DEPLACEMENT -> {
                    try {
                        majJeuCasDeplacement(myCommande);
                    } catch (OccupException | LimiteException ignored) {
                    }
                }
                case TIR -> majJeuCasTir(myCommande);
                case PECHE -> majJeuCasPeche(myCommande);
            }
        } else {
            System.out.println("Le navire sélectionné est hors jeu ! L'action ne peut pas être effectuée.");
        }
    }

    private void majJeuCasPeche(Commande myCommande) {
        Joueur joueur = myCommande.getEquipe();
        int idNavire = myCommande.getIdNavire();
        Point position = joueur.getListeNavire().get(idNavire).getPosition();
        CasePlateau casePlateau = this.lePlateau.getCasePlateau(position);

        if (casePlateau.estOccupee(TypeNav.SOUSMARIN)) {
            for (Navire navire : casePlateau.getLesOccupants()) {
                if (navire.getType().equals(TypeNav.SOUSMARIN)) {
                    ((SousMarin) navire).setEndommage();
                    System.out.println("Vous avez endommagé un navire de l'équipe " + navire.getNumEq() + " !");
                }
            }
        } else {
            System.out.println("Vous avez fait une bonne séance de pêche !");
        }
    }

    private void majJeuCasTir(Commande myCommande) {
        Joueur joueur = myCommande.getEquipe();
        int idNavire = myCommande.getIdNavire();
        Navire navire = joueur.getListeNavire().get(idNavire);
        Point position = navire.getPosition();
        Direction direction = myCommande.getDirectionChoisie();
        CasePlateau casePlateau;

        int taille = this.lePlateau.getTaille();
        boolean touche = false;
        for (int i = 0; i < navire.getPortee() && !touche; i++) {
            position = direction.getPointSuivant(position);
            if (position.x < taille && position.y < taille && position.x >= 0 && position.y >= 0) {
                casePlateau = this.lePlateau.getCasePlateau(position);
                for (Navire navire2 : casePlateau.getLesOccupants()) {
                    navire2.setCoule();
                    casePlateau.removeUnOccupant(navire2);
                    touche = true;
                    System.out.println("Bateau " + navire2.getIdent() + " de l'équipe " + navire2.getNumEq() + " coulé !");
                }
            }
        }
        if (!touche) {
            System.out.println("Personne n'a été touché !");
        }
    }

    private void majJeuCasDeplacement(Commande myCommande) throws OccupException, LimiteException {
        Joueur joueur = myCommande.getEquipe();
        int idNavire = myCommande.getIdNavire();
        Navire navire = joueur.getListeNavire().get(idNavire);
        Point position = navire.getPosition();
        Direction direction = myCommande.getDirectionChoisie();
        CasePlateau casePlateau = this.lePlateau.getCasePlateau(position);

        if (!navire.estValide()) {
            System.out.println("Le navire ne peut pas se déplacer ! Il n'est pas valide.");
            return;
        }

        int taille = this.lePlateau.getTaille();
        for (int i = 0; i < navire.getVitesse(); i++) {
            position = direction.getPointSuivant(position);
            if (position.x < taille && position.y < taille && position.x >= 0 && position.y >= 0) {
                CasePlateau casePlateau2 = this.lePlateau.getCasePlateau(position);
                if (!casePlateau2.estOccupee(navire.getType())) {
                    navire.seDeplacer(position);
                    casePlateau.removeUnOccupant(navire);
                    casePlateau2.addUnOccupant(navire);
                    casePlateau = casePlateau2;
                    System.out.println("Navire déplacé sur la case " + position.x + ";" + position.y);
                } else {
                    throw new OccupException("Navire stopé dans son déplacement par un autre, bloquant le chemin.");
                }
            } else {
                throw new LimiteException("Navire stopé dans son déplacement par les limites du plateau de jeu.");
            }
        }
    }

    private void positionnementNavire() {
        for (Joueur joueur : this.listeJoueurs) {
            for (Navire navire : joueur.getListeNavire()) {
                int taille = this.lePlateau.getTaille();
                int x;
                int y;
                do {
                    x = this.alea.nextInt(taille);
                    y = this.alea.nextInt(taille);
                } while (this.lePlateau.getCasePlateau(x, y).estOccupee(navire.getType()));
                navire.setPosition(new Point(x, y));
                this.lePlateau.getCasePlateau(x, y).addUnOccupant(navire);
            }
        }
    }

    private void attributionNavire() {
        for (Joueur joueur : listeJoueurs) {
            for (int i = 0; i < 3 ; i++) {
                Navire navire;
                if (joueur.getStatus() == Status.NEUTRE) {
                    navire = new Chalutier(i, joueur.getId());
                } else {
                    navire = i >= 2 ? new SousMarin(i, joueur.getId()) : new Destroyer(i, joueur.getId());
                }
                joueur.addNavire(navire);
            }
        }
    }

    private void choixJoueur() {
        int idEquipeIA = this.alea.nextInt(3);
        int idPecheur = this.alea.nextInt(3);
        for (int i = 0; i < 3; i++) {
            Nature nature = i == idEquipeIA ? Nature.IA : Nature.HUMAIN;
            Joueur joueur = i == idPecheur ? new EqPecheur(i, nature) : new EqBataillon(i, nature);
            this.listeJoueurs.add(joueur);
        }
    }
}
