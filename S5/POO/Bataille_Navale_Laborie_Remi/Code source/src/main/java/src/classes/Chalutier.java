package src.classes;

import src.enumerations.TypeNav;

public class Chalutier extends NavireSurface {
    public Chalutier(int rang, int numEquipe) {
        super(rang, TypeNav.CHALUTIER, numEquipe);
        this.vitesse = 2;
        this.portee = 0;
    }

    @Override
    public String toString() {
        return "Chalutier{" +
                "ident=" + ident +
                ", myType=" + myType +
                ", strAffichage='" + strAffichage + '\'' +
                ", numEq=" + numEq +
                ", etat=" + etat +
                ", portee=" + portee +
                ", vitesse=" + vitesse +
                ", position=" + position +
                '}';
    }
}
