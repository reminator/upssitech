package src.classes;

import src.enumerations.TypeNav;

import java.awt.*;

@SuppressWarnings("BooleanMethodIsAlwaysInverted")
public abstract class Navire {

    protected final int ident;
    protected final TypeNav myType;
    protected String strAffichage;
    protected final int numEq;
    protected int etat;
    protected int portee;
    protected int vitesse;

    protected Point position;

    public Navire(int rang, TypeNav typeNav, int numEquipe) {
        this.ident = rang;
        this.myType = typeNav;
        this.numEq = numEquipe;
        this.etat = typeNav == TypeNav.SOUSMARIN ? 2 : 1;
    }

    public int getIdent() {
        return ident;
    }

    public int getNumEq() {
        return numEq;
    }

    public Point getPosition() {
        return position;
    }

    public void setPosition(Point position) {
        this.position = position;
    }

    public TypeNav getType() {
        return myType;
    }

    public boolean estValide() {
        return this instanceof SousMarin ? this.etat == 2 : this.etat == 1;
    }

    public boolean estCoule() {
        return this.etat == 0;
    }

    public void seDeplacer(Point position) {
        this.setPosition(position);
    }

    public int getPortee() {
        return portee;
    }

    public int getVitesse() {
        return vitesse;
    }

    public void setCoule() {
        this.etat = 0;
    }

    @Override
    public String toString() {
        return "Navire{" +
                "ident=" + ident +
                ", myType=" + myType +
                ", numEq=" + numEq +
                ", etat=" + etat +
                ", portee=" + portee +
                ", vitesse=" + vitesse +
                ", position=" + position +
                '}';
    }
}
