package etape2 ;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import outils.* ;

/** Classe pour definir un etat pour le cas 1 de la tache 2
*/
public class EtatCas1 implements Etat {

    // attributs
    // //////////////////////////////////////////////
    /** le graphe representant le monde */
    private final GrapheDeLieux tg;
    private final int sommet;
    private double coutTotal;
    private final int depart;
    private final int arrivee;

    // constructeurs
    // //////////////////////////////////////////////
    /** constructeur d'un etat a partir du graphe representant le monde
    * @param tg graphe representant le monde
    */
    public EtatCas1(GrapheDeLieux tg) {
        this(tg, 0, tg.getNbSommets() - 1);
    }

    /** constructeur d'un etat a partir du graphe representant le monde
    * et du lieu de depart
    * @param tg graphe representant le monde
    * @param lieuDepart numero du lieu de depart
    */
    public EtatCas1(GrapheDeLieux tg, int lieuDepart) {
        this(tg, lieuDepart, tg.getNbSommets() - 1);
    }

    /** constructeur d'un etat a partir du graphe representant le monde,
    * et des lieux de depart et d'arrivee
    * @param tg graphe representant le monde
    * @param lieuDepart numero du lieu de depart
    * @param lieuArrivee numero du lieu d'arrivee
    */
    public EtatCas1(GrapheDeLieux tg, int lieuDepart, int lieuArrivee) {
        this.tg = tg;
        this.depart = lieuDepart;
        this.arrivee = lieuArrivee;
        this.sommet = lieuDepart;
        this.coutTotal = 0;
    }

    // methodes de l'interface Etat
    // //////////////////////////////////////////////
    /** methode detectant si l'etat est une solution
    * @return true si l'etat courant est une solution, false sinon
    */
    public boolean estSolution() {
        return this.sommet == this.arrivee;
    }

    /** methode permettant de recuperer la liste des etats successeurs de l'etat courant
    * @return liste des etats successeurs de l'etat courant
    */
    public List<Etat> successeurs() {
        List<Etat> successeurs = new ArrayList<>();
        for (int adjacent : tg.getAdjacents(sommet)) {
            EtatCas1 successeur = new EtatCas1(tg, adjacent, this.arrivee);
            successeur.coutTotal = this.coutTotal + tg.getCoutArete(sommet, adjacent);
            successeurs.add(successeur);
        }
        return successeurs;
    }

    /** methode permettant de recuperer l'heuristique de l'etat courant 
    * @return heuristique de l'etat courant
    */
    public double h() {
        Lieu lieuDepart = tg.getLesLieux().get(sommet);
        Lieu lieuArrivee = tg.getLesLieux().get(arrivee);

        int var3 = lieuDepart.getX();
        int var5 = lieuArrivee.getX();

        int var4 = lieuDepart.getY();
        int var6 = lieuArrivee.getY();

        double var7 = (var5 - var3) * (var5 - var3);
        double var8 = (var6 - var4) * (var6 - var4);

        return Math.pow(var7 + var8, 0.5);
    }

    /** methode permettant de recuperer le cout du passage de l'etat courant à l'etat e
    * @param e un etat
    * @return cout du passage de l'etat courant à l'etat e
    */
    public double k(Etat e) {
        if (!(e instanceof EtatCas1)) return 0;
        EtatCas1 e1 = (EtatCas1) e;
        return tg.getCoutArete(this.sommet, e1.sommet);
    }

    /** methode permettant d'afficher le chemin qui a mene a l'etat courant en utilisant la map des peres
    * @param pere map donnant pour chaque etat, son pere 
    */
    public void displayPath(Map<Etat, Etat> pere) {
        ArrayList<Etat> chemin = new ArrayList<>();
        chemin.add(this);

        Etat parent;

        do {
            parent = pere.get(chemin.get(chemin.size() - 1));
            if (parent != null) {
                chemin.add(parent);
            }
        } while (parent != null);

        System.out.println("Display path");
        System.out.print("[");
        for (int i = chemin.size() - 1; i >= 0; i--) {
            if (i == 0) {
                System.out.print(chemin.get(i));
            } else {
                System.out.print(chemin.get(i) + ", ");
            }
        }
        System.out.println("]");
        System.out.println("End Display path");
    }

    // methodes de l'interface Comparable
    // //////////////////////////////////////////////
    /** methode de comparaison de l'etat courant avec l'objet o
    * @param o l'objet avec lequel on compare
    * @return un entier qui donne le resultat de la comparaison 
    * (0 si egaux, negatif si inferieur, positif si superieur)
    */
    public int compareTo(Object o) {
        if (!(o instanceof EtatCas1)) return 0;
        EtatCas1 e = (EtatCas1) o;
        return (int) (this.coutTotal - e.coutTotal);
    }
    // methodes pour pouvoir utiliser cet objet dans des listes et des map
    // //////////////////////////////////////////////
    /** methode permettant de recuperer le code de hachage de l'etat courant
    *  pour une utilisation dans des tables de hachage
    * @return code de hachage
    */
    @Override
    public int hashCode() {
        return Objects.hash(tg, sommet, coutTotal, depart, arrivee);
    }

    /** methode de comparaison de l'etat courant avec l'objet o
    * @param o l'objet avec lequel on compare
    * @return true si l'etat courant et o sont egaux, false sinon
    */
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        EtatCas1 etatCas1 = (EtatCas1) o;
        return depart == etatCas1.depart && arrivee == etatCas1.arrivee
                && tg.getLesLieux().equals(etatCas1.tg.getLesLieux())
                && this.coutTotal == etatCas1.coutTotal;
    }

    // methode pour affichage futur (heritee d'Object)
    // //////////////////////////////////////////////
    /** methode mettant l'etat courant sous la forme d'une 
    * chaine de caracteres en prevision d'un futur affichage
    * @return representation de l'etat courant sous la forme d'une
    *         chaine de caracteres
    */
    public String toString() {
        return "" + this.sommet;
    }

}

