package etape2 ;

import java.util.*;

import outils.* ;

/** Classe pour definir un etat pour le cas 3 de la tache 2
*/
public class EtatCas3 implements Etat {

    // attributs
    // A COMPLETER
    // //////////////////////////////////////////////
    private final GrapheDeLieux tg;
    private final ArrayList<Integer> trajet;

    // constructeurs
    // A ECRIRE/COMPLETER
    // //////////////////////////////////////////////
    /** constructeur d'un etat a partir du graphe representant le monde
    * @param tg graphe representant le monde
    */
    public EtatCas3(GrapheDeLieux tg) {
        this(tg, tg.getSommets().get(0));
    }

    /** constructeur d'un etat a partir du graphe representant le monde
    * et du lieu de depart
    * @param tg graphe representant le monde
    * @param lieuDepart numero du lieu de depart
    */
    public EtatCas3(GrapheDeLieux tg, int lieuDepart) {
        this.trajet = new ArrayList<>();
        this.trajet.add(lieuDepart);
        this.tg = tg;
    }

    public EtatCas3(GrapheDeLieux tg, ArrayList<Integer> trajet) {
        this.tg = tg;
        this.trajet = trajet;
    }

    // methodes de l'interface Etat
    // //////////////////////////////////////////////
    /** methode detectant si l'etat est une solution
    * @return true si l'etat courant est une solution, false sinon
    */
    public boolean estSolution() {
        // On peut réduire la condition de solution ainsi au vu de la création des successeurs
        return this.trajet.size() > 1 && this.trajet.get(0).equals(this.trajet.get(this.trajet.size() - 1));
    }

    /** methode permettant de recuperer la liste des etats successeurs de l'etat courant
    * @return liste des etats successeurs de l'etat courant
    */
    public List<Etat> successeurs() {
        List<Etat> successeurs = new ArrayList<>();
        for (int adjacent : tg.getSommets()) {

            // Si on revient au point de départ...
            if (adjacent == this.trajet.get(0)) {
                // ... pour créer le successeur, il faut avoir visité tous les sommets
                if (!this.trajet.containsAll(this.tg.getSommets())) continue;
            } else {
                // Sinon, si on est déjà passé par ce sommet, on empêche de créer le successeur correspondant.
                if (this.trajet.contains(adjacent)) continue;
            }

            // Sinon on ajoute le successeur à la liste.
            ArrayList<Integer> trajet = new ArrayList<>(this.trajet);
            trajet.add(adjacent);
            EtatCas3 successeur = new EtatCas3(tg, trajet);

            successeurs.add(successeur);
        }
        return successeurs;
    }

    /** methode permettant de recuperer l'heuristique de l'etat courant 
    * @return heuristique de l'etat courant
    */
    public double h() {
        return (tg.getNbSommets() + 1 - trajet.size()) * tg.getPoidsMinAir();
    }

    /** methode permettant de recuperer le cout du passage de l'etat courant à l'etat e
    * @param e un etat
    * @return cout du passage de l'etat courant à l'etat e
    */
    public double k(Etat e) {
        return tg.dist(this.trajet.get(this.trajet.size() - 1), ((EtatCas3) e).trajet.get(((EtatCas3) e).trajet.size() - 1));
    }

    /** methode permettant d'afficher le chamin qui a mene a l'etat courant en utilisant la map des peres
    * @param pere map donnant pour chaque etat, son pere 
    */
    public void displayPath(Map<Etat, Etat> pere) {
        ArrayList<Etat> chemin = new ArrayList<>();
        chemin.add(this);

        Etat parent;

        do {
            parent = pere.get(chemin.get(chemin.size() - 1));
            if (parent != null) {
                chemin.add(parent);
            }
        } while (parent != null);

        System.out.println("Display path");
        System.out.print("[");
        for (int i = chemin.size() - 1; i >= 0; i--) {
            if (i == 0) {
                System.out.print(chemin.get(i));
            } else {
                System.out.print(chemin.get(i) + ", ");
            }
        }
        System.out.println("]");
        System.out.println("End Display path");
    }

    // methodes de l'interface Comparable
    // //////////////////////////////////////////////
    /** methode de comparaison de l'etat courant avec l'objet o
    * @param o l'objet avec lequel on compare
    * @return un entier qui donne le resultat de la comparaison 
    * (0 si egaux, negatif si inferieur, positif si superieur)
    */
    public int compareTo(Object o) {
        if (!(o instanceof EtatCas3)) return 0;
        EtatCas3 e = (EtatCas3) o;
        return this.trajet.equals(e.trajet) ? 0 : -1;
    }

    // methodes pour pouvoir utiliser cet objet dans des listes et des map
    // //////////////////////////////////////////////
    /** methode permettant de recuperer le code de hachage de l'etat courant
    *  pour une utilisation dans des tables de hachage
    * @return code de hachage
    */
    @Override
    public int hashCode() {
        return Objects.hash(tg, trajet);
    }

    /** methode de comparaison de l'etat courant avec l'objet o
    * @param o l'objet avec lequel on compare
    * @return true si l'etat courant et o sont egaux, false sinon
    */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        EtatCas3 etatCas3 = (EtatCas3) o;
        return trajet.equals(etatCas3.trajet);
    }
    

    // methode pour affichage futur (heritee d'Object)
    // //////////////////////////////////////////////
    /** methode mettant l'etat courant sous la forme d'une 
    * chaine de caracteres en prevision d'un futur affichage
    * @return representation de l'etat courant sour la forme d'une 
    *         chaine de caracteres
    */
    public String toString() {
        return "" + this.trajet.get(this.trajet.size() - 1);
    }
}

