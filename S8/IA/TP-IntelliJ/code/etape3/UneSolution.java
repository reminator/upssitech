package etape3 ;

import java.lang.reflect.Array;
import java.util.*;

import org.jgrapht.Graph;
import outils.* ;

/** Classe pour definir une solution pour le cas 3 de la tache 2
*/
public class UneSolution implements Solution {

  
    // attributs
    // A COMPLETER
    // //////////////////////////////////////////////
    /** le graphe representant le monde */
    private final GrapheDeLieux tg;
    private final ArrayList<Integer> trajet;
    private static final Random RAND = new Random();
    private static final int NB_VOISINS_MAX = 1000;
    
    // constructeurs
    // A ECRIRE/COMPLETER
    // //////////////////////////////////////////////
    /** constructeur d'une solution a partir du graphe representant le monde
     * @param tg graphe representant le monde
     */
    public UneSolution(GrapheDeLieux tg) {
        this(tg, tg.getSommets().get(0));
    }

    public UneSolution(GrapheDeLieux tg, int depart) {
        this(tg, new ArrayList<>(UneSolution.cheminAleatoire(tg, depart)));
    }

    public UneSolution(GrapheDeLieux tg, List<Integer> chemin) {
        this.tg = tg;
        this.trajet = new ArrayList<>(chemin);
    }

    public static List<Integer> cheminAleatoire(GrapheDeLieux tg, int depart) {
        List<Integer> chemin = new ArrayList<>();
        List<Integer> sommets = tg.getSommets();

        chemin.add(depart);
        sommets.remove(Integer.valueOf(depart));

        while(sommets.size() > 0) {
            int suivantIndex = RAND.nextInt(sommets.size());
            int suivant = sommets.get(suivantIndex);
            chemin.add(suivant);
            sommets.remove(Integer.valueOf(suivant));
        }
        chemin.add(depart);
        return chemin;
    }


    // methodes de l'interface Solution
    // //////////////////////////////////////////////
    /** methode recuperant la liste des voisins de la solution courante
    * @return liste des voisins de la solution courante
    */
    public List<Solution> lesVoisins() {

        List<Solution> solutions = new ArrayList<>();
        Solution solution;

        for (int n = 0; n < NB_VOISINS_MAX; n++) {
            solution = getVoisin();
            if (!solutions.contains(solution)) {
                solutions.add(solution);
            }
        }
    	return solutions;
    }

    /** methode recuperant un voisin de la solution courante
    * @return voisin de la solution courante
    */
    public List<Solution> unVoisin() {
        List<Solution> solutions = new ArrayList<>();        
        solutions.add(getVoisin());

    	return solutions;
    }

    private Solution getVoisin() {
        Solution solution;
        List<Integer> chemin = new ArrayList<>(this.trajet);

        int i = RAND.nextInt(chemin.size() - 2) + 1;
        int j;

        do {
            j =  RAND.nextInt(chemin.size() - 2) + 1;
        } while(i == j);

        int stock = chemin.get(i);
        chemin.set(i, chemin.get(j));
        chemin.set(j, stock);

        solution = new UneSolution(tg, chemin);
        return solution;
    }

    /** methode generant aleatoirement une nouvelle solution
     * a partir de la solution courante
     * @return nouvelle solution generee aleatoirement a partir de la solution courante
     */
    public Solution nelleSolution() {
        return new UneSolution(this.tg, this.trajet.get(0));
    }

    /** methode recuperant la valeur de la solution courante
    * @return valeur de la solution courante
    */
    public double eval() {
        double res = 0;
        for (int i = 1; i < this.trajet.size(); i++) {
            res += this.tg.dist(this.trajet.get(i - 1), this.trajet.get(i));
        }
        return res;
    }
    
    /** methode affichant la solution courante comme un chemin dans le graphe
    */
    public void displayPath() {

        System.out.println("Display path");
        System.out.print("[");
        for (int i = this.trajet.size() - 1; i >= 0; i--) {
            if (i == 0) {
                System.out.print(this.trajet.get(i));
            } else {
                System.out.print(this.trajet.get(i) + ", ");
            }
        }
        System.out.println("]");
        System.out.println("End Display path");
    }


    // methodes pour pouvoir utiliser cet objet dans des listes et des map
    // //////////////////////////////////////////////
    /** methode permettant de recuperer le code de hachage de la solution courante
    *  pour une utilisation dans des tables de hachage
    * @return code de hachage
    */
    public int hashCode() {
        return Objects.hash(tg, trajet);
    }

    /** methode de comparaison de la solution courante avec l'objet o
    * @param o l'objet avec lequel on compare
    * @return true si la solution courante et o sont egaux, false sinon
    */
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        UneSolution etatCas3 = (UneSolution) o;
        return trajet.equals(etatCas3.trajet);
    }
    

    // methode pour affichage futur (heritee d'Object)
    // //////////////////////////////////////////////
    /** methode mettant la solution courante sous la forme d'une 
    * chaine de caracteres en prevision d'un futur affichage
    * @return representation de la solution courante sour la forme d'une 
    *         chaine de caracteres
    */
    public String toString() {
        //return "" + this.trajet.get(this.trajet.size() - 1);
        StringBuilder res = new StringBuilder();
        res.append("\n[");
        for (int i = this.trajet.size() - 1; i >= 0; i--) {
            if (i == 0) {
                res.append(this.trajet.get(i));
            } else {
                res.append(this.trajet.get(i)).append(", ");
            }
        }
        res.append("]\nPoids : ").append(this.eval()).append("\n");
        return res.toString();
    }
}
