package etape5;

import org.chocosolver.solver.Model;
import org.chocosolver.solver.variables.IntVar;
import org.jgrapht.Graph;
import outils.Fils;
import outils.GrapheDeLieux;
import outils.Lieu;

import java.util.ArrayList;

public class Etape5 {

    Model model;
    IntVar[] variables;

    public Etape5(GrapheDeLieux tg, int nbCouleurs) {
        model = new Model("Coloration de graphe avec " + nbCouleurs + " couleurs");

        variables = model.intVarArray("sommets", tg.getNbSommets(), 1, nbCouleurs);

        for (Lieu lieu : tg.getLesLieux()) {
            int sommet = lieu.getNom();

            for (Fils fils : lieu.getFils()) {
                int sommet2 = fils.getNom();

                // Si le sommet2 < sommet, l'arête a déjà été traité car on parcourt les somments dans l'ordre croissant.
                if (sommet2 > sommet) {
                    model.allDifferent(variables[sommet], variables[sommet2]).post();
                }
            }
        }
    }

    public boolean solve() {
        return model.getSolver().solve();
    }
}
