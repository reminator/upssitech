package etape1 ;

import java.lang.* ;
import java.util.* ;
import outils.* ;
import solvers.* ;


/** classe pour realiser l'etape 1 du projet (tache 1) */
public class Etape1 {

    // attributs
    // //////////////////////////////////////////////
    /** le graphe representant le monde */
    private GrapheDeLieux g ;
    /** la base de clauses representant le probleme */
    private ArrayList<ArrayList<Integer>> base ; // maj par majBase
    /** le nb de variables utilisees pour representer le probleme */
    private int nbVariables ; // maj par majBase

    // constructeur
    // //////////////////////////////////////////////
    /** constructeur
     * @param fn le nom du fichier dans lequel sont donnes les sommets et les aretes
     * @param format le format permet de distinguer entre les differents types de fichier
     * (pour ceux contenant des poids et des coordonnees, format est true ;
     *  pour les autres, format est false)
     */
    public Etape1 (String fn, boolean format) {
        g = GrapheDeLieux.loadGraph(fn,format);
        base = new ArrayList<>() ;
    }

    // methodes
    // //////////////////////////////////////////////
    /** methode de maj de la base de clauses et du nb de variables en fonction du pb traite
     * @param nbCouleurs parametre servant pour definir la base qui representera le probleme
     */
    public void majBase(int nbCouleurs) {
        // ajout possible de parametre => modifier aussi l'appel dans le main

        this.base.clear();

        // Calcul des nombres utiles
        int nbSommets = g.getNbSommets();
        this.nbVariables = nbCouleurs * nbSommets;

        for (Lieu lieu : g.getLesLieux()) {
            int sommet = lieu.getNom();

            // 1) Clauses "Chaque sommet a une couleur".
            // Le sommet doit avoir au moins une couleur (phi11)
            ArrayList<Integer> clausePhi1 = new ArrayList<>();
            // Le sommet ne doit pas avoir plusieurs couleurs (phi12)
            ArrayList<Integer> clausePhi2;

            // Je vais jusqu'à nbCouleurs pour ajouter la dernière à clausePhi11. Mais cela n'est pas nécessaire pour clausePhi12.
            for (int couleur1 = 0; couleur1 < nbCouleurs; couleur1++) {
                int variable1 = getVariable(nbSommets, couleur1, sommet);
                clausePhi1.add(variable1);

                for (int couleur2 = couleur1 + 1; couleur2 < nbCouleurs; couleur2++) {
                    clausePhi2 = new ArrayList<>();
                    int variable2 = getVariable(nbSommets, couleur2, sommet);
                    clausePhi2.add(-variable1);
                    clausePhi2.add(-variable2);
                    base.add(clausePhi2);
                }
            }
            base.add(clausePhi1);

            // 2) Clauses "Deux sommets adjacents ne peuvent pas avoir la même couleur"
            ArrayList<Integer> clausePsy;
            for (Fils fils : lieu.getFils()) {
                int sommet2 = fils.getNom();

                // Si le sommet2 < sommet, l'arête a déjà été traité car on parcourt les somments dans l'ordre croissant.
                if (sommet2 > sommet) {
                    for (int couleur = 0; couleur < nbCouleurs; couleur++) {
                        clausePsy = new ArrayList<>();
                        int variable1 = getVariable(nbSommets, couleur, sommet);
                        int variable2 = getVariable(nbSommets, couleur, sommet2);
                        clausePsy.add(-variable1);
                        clausePsy.add(-variable2);
                        base.add(clausePsy);
                    }
                }
            }
        }
    }

    private int getVariable(int nbSommets, int couleur, int sommet) {
        return nbSommets * couleur + sommet + 1;
    }

    /** methode d'appel du solver sur la base de clauses representant le pb traite
     * @return true si la base de clauses representant le probleme est satisfiable,
     *         false sinon
     */
    public boolean execSolver() {
        return SolverSAT.isSAT(base,nbVariables) ;
    }

    /** affichage de la base de clauses representant le probleme */
    public void affBase(){
        System.out.println("Base de clause utilise "+nbVariables+
                " variables et contient les clauses suivantes : ") ;
        for (ArrayList<Integer> cl : base) {
            System.out.println(cl) ;
        }
    }

    // le main
    // //////////////////////////////////////////////
    /** methode de TESTS pour Etape1
     * @param args parametre non utilise
     */
    public static void main(String [] args) {

        // TEST 1 : town10.txt avec 3 couleurs
        System.out.println("Test sur fichier town10.txt avec 3 couleurs") ;
        Etape1 e = new Etape1("Data/town10.txt",true) ;
        e.majBase(3) ;
        e.affBase() ;
        System.out.println("Resultat obtenu (on attend true) :"+e.execSolver()) ;


        // TEST 2 : town10.txt avec 2 couleurs
        System.out.println("Test sur fichier town10.txt avec 2 couleurs") ;
        e.majBase(2) ;
        e.affBase() ;
        System.out.println("Resultat obtenu (on attend false) :"+e.execSolver()) ;


        // TEST 3 : town10.txt avec 4 couleurs
        System.out.println("Test sur fichier town10.txt avec 4 couleurs") ;
        e.majBase(4) ;
        //e.affBase() ;
        System.out.println("Resultat obtenu (on attend true) :"+e.execSolver()) ;


        // TEST 4 : flat20_3_0.col avec 4 couleurs
        System.out.println("Test sur fichier flat20_3_0.col avec 4 couleurs") ;
        e = new Etape1("Data/pb-etape1/flat20_3_0.col",false) ;
        e.majBase(4) ;
        //e.affBase() ;
        System.out.println("Resultat obtenu (on attend true) :"+e.execSolver()) ;

        // TEST 5 : flat20_3_0.col avec 3 couleurs
        System.out.println("Test sur fichier flat20_3_0.col avec 3 couleurs") ;
        e.majBase(3) ;
        //e.affBase() ;
        System.out.println("Resultat obtenu (on attend true) :"+e.execSolver()) ;

        // TEST 6 : flat20_3_0.col avec 2 couleurs
        System.out.println("Test sur fichier flat20_3_0.col avec 2 couleurs") ;
        e.majBase(2) ;
        //e.affBase() ;
        System.out.println("Resultat obtenu (on attend false) :"+e.execSolver()) ;



        // TEST 7 : jean.col avec 10 couleurs
        System.out.println("Test sur fichier jean.col avec 10 couleurs") ;
        e = new Etape1("Data/pb-etape1/jean.col",false) ;
        e.majBase(10) ;
        //e.affBase() ;
        System.out.println("Resultat obtenu (on attend true) :"+e.execSolver()) ;

        // TEST 9 : jean.col avec 9 couleurs
        System.out.println("Test sur fichier jean.col avec 9 couleurs") ;
        e.majBase(9) ;
        //e.affBase() ;
        System.out.println("Resultat obtenu (on attend false) :"+e.execSolver()) ;

        // TEST 8 : jean.col avec 3 couleurs
        System.out.println("Test sur fichier jean.col avec 3 couleurs") ;
        e.majBase(3) ;
        //e.affBase() ;
        System.out.println("Resultat obtenu (on attend false) :"+e.execSolver()) ;

    }
}
