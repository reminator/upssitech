param fichier := "../Data/pb-etape5/tsp19.txt";

set positions := { read fichier as "<1n, 2n>" comment "#"};

param taille := card(positions);

set sommets := { 0 to (taille - 1) };

set sous_ensembles[] := powerset(sommets);
set indices := indexset(sous_ensembles);

param positionsX[sommets] := read fichier as "1n" comment "#";
param positionsY[sommets] := read fichier as "2n" comment "#";


set aretes := { <i,j> in sommets * sommets with i < j };

defnumb dist(i, j) := sqrt((positionsX[j] - positionsX[i])^2 + (positionsY[j] - positionsY[i])^2);


var x[aretes] binary;

minimize cout: sum <i,j> in aretes : dist(i, j) * x[i, j];

subto connexion: forall <s> in sommets do (sum <i,s> in aretes : x[i,s]) + (sum <s,j> in aretes : x[s,j]) == 2;

subto chemin_unique: forall <i> in indices with card(sous_ensembles[i]) < taille - 2 and card(sous_ensembles[i]) > 2 do
    (sum <k,l> in aretes with <k> in sous_ensembles[i] and <l> in sous_ensembles[i] : x[k,l]) <= (card(sous_ensembles[i]) - 1);





