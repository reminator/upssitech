function T_RC_R = getTRCR(teta, Xm, Ym, r, qpl, Dx, h, a, b, c)


T_R_RM = [  cos(teta) -sin(teta) 0 Xm;
            sin(teta) cos(teta) 0 Ym;
            0 0 1 r;
            0 0 0 1 ];
        
T_RM_RP = [  cos(qpl) -sin(qpl) 0 Dx;
            sin(qpl) cos(qpl) 0 0;
            0 0 1 h;
            0 0 0 1 ];
        
T_RP_C = [  cos(pi/2) 0 sin(pi/2) a;
            0 1 0 b;
            -sin(pi/2) 0 cos(pi/2) c;
            0 0 0 1 ];

T_R_RC = T_R_RM * T_RM_RP * T_RP_C;

T_RC_R = inv(T_R_RC);

end

