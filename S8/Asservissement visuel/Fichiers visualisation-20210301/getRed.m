function Lred = getRed(X, Y, z)

Lred = [    0 X/z X*Y;
            -1/z Y/z 1+Y^2 ];
end

