function dq = commande(sStar, s, OP1, OP2, OP3, OP4, qpl, Dx, a, b, lambda)

e = s - sStar;

L1 = getRed(s(1), s(2), OP1(3));
L2 = getRed(s(3), s(4), OP2(3));
L3 = getRed(s(5), s(6), OP3(3));
L4 = getRed(s(7), s(8), OP4(3));

L = [ L1 ; L2 ; L3 ; L4 ];

J = [ -sin(qpl) a+Dx*cos(qpl) a ;
        cos(qpl) -b+Dx*sin(qpl) -b;
        0 -1 -1 ];

A = L * J;
B = -lambda * e;

PseudoA = inv(A' * A) * A';

dq = PseudoA * B;

end

