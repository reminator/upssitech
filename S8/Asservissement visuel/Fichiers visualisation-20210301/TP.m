close all;
clear;

Tes = [0.01 0.001];
Te = Tes(1)
lambda = 1;

Xm = 6.9;
Ym = 2;
teta = 0;
qpl = 0;

a = 0;
b = 0;
c = 0;
Dx = 0.1;



OP1_R = [8 ; 2.2 ; 1 ; 1];
OP2_R = [8 ; 1.8 ; 1 ; 1];
OP3_R = [8 ; 1.8 ; 0.6 ; 1];
OP4_R = [8 ; 2.2 ; 0.6 ; 1];

sStar = visu(OP1_R, OP2_R, OP3_R, OP4_R, teta, Xm, Ym, qpl)

X1 = sStar(1);
Y1 = sStar(2);

X2 = sStar(3);
Y2 = sStar(4);

X3 = sStar(5);
Y3 = sStar(6);

X4 = sStar(7);
Y4 = sStar(8);

figure;
hold on;
% hold on;
% drawImage(X1,Y1,X2,Y2,X3,Y3,X4,Y4);
drawRobotPlatine(Xm, Ym, teta, qpl);
drawTarget(OP1_R,OP2_R,OP3_R,OP4_R);

    Xm = 2;
    Ym = 0;
    teta = -2*pi/3;
    qpl = 5*pi/6;
    
   
s = visu(OP1_R, OP2_R, OP3_R, OP4_R, teta, Xm, Ym, qpl)
norm(s - sStar);

n = 10000;

Xms = zeros(n, 1);
Yms = zeros(n, 1);
tetas = zeros(n, 1);
qpls = zeros(n, 1);
ts = zeros(n, 1);
ss = zeros(n, 8);
t = 1;

vs = zeros(n, 1);
ws = zeros(n, 1);
wpl = zeros(n, 1);


        drawRobotPlatine(Xm, Ym, teta, qpl);

while (norm(s - sStar) > 0.000000001) && t < n
    
    [s, OP1_RC, OP2_RC, OP3_RC, OP4_RC] = visu(OP1_R, OP2_R, OP3_R, OP4_R, teta, Xm, Ym, qpl);
    ss(t, :) = s;


    dq = commande(sStar, s, OP1_RC, OP2_RC, OP3_RC, OP4_RC, qpl, Dx, a, b, lambda);
    
vs(t) = dq(1);
ws(t) = dq(2);
wpl(t) = dq(3);
    
    [Xm, Ym, teta, qpl] = etat(Xm, Ym, teta, qpl, dq, Te);
    
    Xms(t) = Xm;
    Yms(t) = Ym;
    tetas(t) = teta;
    qpls(t) = qpl;
    ts(t) = t;
    t = t + 1;
    
    
    

    if mod(t, 100) == 0 || t < 5
        drawRobotPlatine(Xm, Ym, teta, qpl);
    end
end

figure;
subplot(411);
plot(Xms(1:t));
legend("X");
subplot(412);
plot(Yms(1:t));
legend("Y");
subplot(413);
plot(tetas(1:t));
legend("Teta");
subplot(414);
plot(qpls(1:t));
legend("Qpl");

figure;
subplot(311);
plot(vs(1:t));
legend("vs");
subplot(312);
plot(ws(1:t));
legend("ws");
subplot(313);
plot(wpl(1:t));
legend("wpl");

drawImage(ss(:, 1), ss(:, 2), ss(:, 3), ss(:, 4), ss(:, 5), ss(:, 6), ss(:, 7), ss(:, 8));




