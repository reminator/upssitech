function [Xm, Ym, teta, qpl] = etat(Xm, Ym, teta, qpl, dq, Te)

    Xm = Xm + dq(1) * Te * cos(teta);
    Ym = Ym + dq(1) * Te * sin(teta);
    
    teta = teta + dq(2) * Te;
    qpl = qpl + dq(3) * Te;
end

