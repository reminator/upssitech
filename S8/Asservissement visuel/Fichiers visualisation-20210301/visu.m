function [s, OP1_RC, OP2_RC, OP3_RC, OP4_RC] = visu(OP1_R, OP2_R, OP3_R, OP4_R, teta, Xm, Ym, qpl)
f = 1;
Dx = 0.1;
h = 0.4;
r = 0.4;
a = 0;
b = 0;
c = 0;


% Question a
T_RC_R = getTRCR(teta, Xm, Ym, r, qpl, Dx, h, a, b, c);

% Question b
OP1_RC = RtoRC(T_RC_R, OP1_R);
OP2_RC = RtoRC(T_RC_R, OP2_R);
OP3_RC = RtoRC(T_RC_R, OP3_R);
OP4_RC = RtoRC(T_RC_R, OP4_R);

%Question c
XP1 = OP1_RC * f / OP1_RC(3);
XP2 = OP2_RC * f / OP2_RC(3);
XP3 = OP3_RC * f / OP3_RC(3);
XP4 = OP4_RC * f / OP4_RC(3);

s = [ XP1(1) ; XP1(2) ; XP2(1) ; XP2(2) ; XP3(1) ; XP3(2) ; XP4(1) ; XP4(2) ];

end

