import numpy as np


def triangle_pascal(n: int):
    if n == 0: return np.array([1])
    if n == 1: return np.array([1, 1])

    res = np.array([1, 2, 1])
    for i in range(2, n):
        suivant = np.ones(res.size + 1)
        suivant[1:-1] = res[:-1] + res[1:]
        res = suivant
    return res
