from trianglePascal import *
from Point import *
import matplotlib.pyplot as plt


class BSpline:
    def __init__(self, polygone: Polygone, nodal: list, k: int):
        self.k = k
        self.m = k - 1
        self.n = len(polygone) - 1
        self.polygone = polygone
        self.nodal = nodal

    def p(self, u):
        nodal = self.nodal
        k = self.k
        n = self.n
        polygone = self.polygone

        if u < nodal[k - 1] or u > nodal[n + 1]:
            return 0

        j = k
        dec = 0
        while u > nodal[j]:
            j += 1
            dec += 1

        Pcalc = []
        for i in range(k):
            Pcalc.append(polygone[dec + i])
        # print(u)
        # print(Pcalc)
        for j in range(k - 1):
            for i in range(k - j - 1):
                umin = nodal[dec + 1 + i + j]
                umax = nodal[dec + k + i]
                Pcalc[i] = (umax - u) / (umax - umin) * Pcalc[i] + (u - umin) / (umax - umin) * Pcalc[i + 1]
        return Pcalc[0]

    def plot(self, pas):
        xs = []
        ys = []
        for u in np.arange(self.nodal[self.k - 1], self.nodal[self.n + 1], pas):
            #print(u)
            res = self.p(u)
            xs.append(res.x)
            ys.append(res.y)

        plt.plot(xs, ys, "-m")
