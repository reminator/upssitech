from trianglePascal import *
from Point import *
import matplotlib.pyplot as plt


class Bezier:
    def __init__(self, polygone: Polygone):
        self.degres = len(polygone) - 1
        self.polygone = polygone

        self.pascal = triangle_pascal(self.degres)

        def B(i, u):
            return self.pascal[i] * u**i * (1 - u)**(self.degres - i)
        self.B = B

    def p(self, u):
        res = Point(0, 0)
        for i in range(self.degres + 1):
            res = res + self.B(i, u) * self.polygone[i]
        return res

    def plot(self, pas):
        xs = []
        ys = []
        for u in np.arange(0, 1 + pas, pas):
            res = self.p(u)
            xs.append(res.x)
            ys.append(res.y)

        plt.plot(xs, ys, "-m")


class BezierCubique:
    def __init__(self):
        self.beziers = []

    def addBezier(self, bezier):
        self.beziers.append(bezier)

    def plot(self, pas):
        for bezier in self.beziers:
            xs = []
            ys = []
            for u in np.arange(0, 1 + pas, pas):
                res = bezier.p(u)
                xs.append(res.x)
                ys.append(res.y)

            plt.plot(xs, ys, "-m")


class BezierCubiqueC1:
    def __init__(self, polygone: Polygone):
        self.beziers = []

        pts = []
        for i, point in enumerate(polygone):
            if len(pts) == 3 and len(polygone) > i:
                p = Point(polygone[i - 1].x + polygone[i].x, polygone[i - 1].y + polygone[i].y)
                p = Point(p.x / 2.0, p.y / 2.0)
                pts.append(p)
                self.beziers.append(Bezier(Polygone(pts)))
                pts = [p]
            pts.append(point)

    def plot(self, pas):
        for bezier in self.beziers:
            xs = []
            ys = []
            for u in np.arange(0, 1 + pas, pas):
                res = bezier.p(u)
                xs.append(res.x)
                ys.append(res.y)

            plt.plot(xs, ys, "-m")
