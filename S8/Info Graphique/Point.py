import matplotlib.pyplot as plt


class Point:
    def __init__(self, x, y):
        self.x = x
        self.y = y

    def __rmul__(self, r):
        return Point(self.x * r, self.y * r)

    def __add__(self, point):
        return Point(self.x + point.x, self.y + point.y)

    def __radd__(self, r):
        return Point(self.x + r, self.y + r)

    def __repr__(self):
        return "Point [" + str(self.x) + ", " + str(self.y) + "]"

    def __div__(self, other):
        return Point(self.x / other, self.y / other)


class Polygone:
    def __init__(self, points):
        self.points = points

    def __getitem__(self, item):
        return self.points[item]

    def __len__(self):
        return len(self.points)

    def plot(self):
        xs = []
        ys = []
        for point in self.points:
            xs.append(point.x)
            ys.append(point.y)
        plt.plot(xs, ys, ".r")
        plt.plot(xs, ys, "-c")

