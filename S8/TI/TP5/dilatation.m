function Img2 = dilatation(Img)

    Img2 = Img;
    tailleImg1 = size(Img);
    for x = 2:tailleImg1(1) - 1
        for y = 2:tailleImg1(2) - 1
            resultat = false;
            voisins = reshape(Img(x-1:x+1, y-1:y+1), 1, []);
            voisins = [voisins(2) voisins(4:6) voisins(8)];
            for voisin = voisins
                resultat = resultat || voisin;
            end
            Img2(x, y) = resultat;
        end
    end
end

