function Squelette = squelette(Img)

Img1 = Img(:, :, 1) >= 1;


tailleImg1 = size(Img1);

ImgPrec = Img1(:, :, 1);
Img1Erode = Img1(:, :, 1);

test = true;
Squelette = zeros(tailleImg1(1), tailleImg1(2));
while test
    Img1Erode = erosion(ImgPrec);
    Img1Dilate = dilatation(Img1Erode);

    Squelette = somme(Squelette, soustraction(ImgPrec, Img1Dilate));
    
    ImgPrec = Img1Erode;
    
    test = false;
    for x = 1:tailleImg1(1)
        for y = 1:tailleImg1(2)
            test = test || Img1Erode(x, y);
            if test
                break;
            end
        end
        if test
            break;
        end
    end
end
end

