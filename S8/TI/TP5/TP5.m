clear;
close all;

Img1 = double(imread('BE06I01.bmp'));
ImgRes1 = squelette(Img1);
extremites1 = extremites(ImgRes1);
noeuds1 = noeuds(ImgRes1);

Img2 = double(imread('Lapin.png'));
ImgRes2 = squelette(Img2);
extremites2 = extremites(ImgRes2);
noeuds2 = noeuds(ImgRes2);


figure();
res1 = ImgRes1;
res1(:, :, 1) = ImgRes1;
res1(:, :, 2) = extremites1;
res1(:, :, 3) = noeuds1;

imagesc(res1);
colormap(gray(256));


figure();
res2 = ImgRes2;
res2(:, :, 1) = ImgRes2;
res2(:, :, 2) = extremites2;
res2(:, :, 3) = noeuds2;

imagesc(res2);
colormap(gray(256));

%%


Img3 = double(imread('image (1).png'));
Img3 = Img3(:, :, 1);

Img3ErodePrec = Img3;
Img3DilatePrec = Img3;

for i = 1:10
    Img3Erode = erosionG(Img3ErodePrec);
    Img3Dilate = dilatationG(Img3DilatePrec);
    Img3ErodePrec = Img3Erode;
    Img3DilatePrec = Img3Dilate;
end
figure();
image(uint8(Img3Erode));
colormap(gray(256));

figure();
image(uint8(Img3Dilate));
colormap(gray(256));


%%

Img3 = double(imread('image (1).png'));
Img3 = Img3(:, :, 1);

Img3ErodePrec = Img3;
Img3DilatePrec = Img3;

for i = 1:3
    Img3Erode = erosionG(Img3ErodePrec);
    Img3Dilate = dilatationG(Img3DilatePrec);
    Img3ErodePrec = Img3Erode;
    Img3DilatePrec = Img3Dilate;
end

Gi = Img3 - Img3Erode;
Ge = Img3Dilate - Img3;
G = Gi + Ge

figure();
imagesc(G);
colormap(gray(256));
