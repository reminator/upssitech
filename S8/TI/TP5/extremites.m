function points = extremites(Img)

    tailleImg = size(Img);
    points = zeros(tailleImg(1), tailleImg(2));
    
    taille = size(Img);
    for x = 2:taille(1)-1
        for y = 2:taille(2)-1
            if Img(x, y) 
                voisins = reshape(Img(x-1:x+1, y-1:y+1), 1, []);
                voisins = [voisins(1:4) voisins(6:9)];
                nbVoisins = 0;
                for voisin=voisins
                    if voisin
                        nbVoisins = nbVoisins + 1;
                    end
                end
                if nbVoisins == 1
                    points(x, y) = 1;
                end
            end
        end
    end
end

