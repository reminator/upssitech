function Img = soustraction(Img1, Img2)
    
    tailleImg = size(Img1);
    Img = zeros(tailleImg(1), tailleImg(2));
    for x = 1:tailleImg(1)
        for y = 1:tailleImg(2)
            Img(x, y) = Img1(x, y) ~= Img2(x, y);
        end
    end
end

