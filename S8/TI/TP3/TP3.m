clear;
close all;

Img1 = double(imread('TP03I01.jpg'));

%% I - 1

nmin = 2500;
nmax = 3500;
pas = 100;

x_plt = zeros((nmax-nmin)/pas, 1);
y_bruit = zeros((nmax-nmin)/pas, 1);
y_filtre = zeros((nmax-nmin)/pas, 1);

i = 1;
for N = nmin:pas:nmax

    tailleImg = size(Img1);

    Img_bruitee = Img1;

    for n = 1:N
        x_blanc = floor(rand() * (tailleImg(1) - 1) + 1);
        y_blanc = floor(rand() * (tailleImg(2) - 1) + 1);

        x_noir = floor(rand() * (tailleImg(1) - 1) + 1);
        y_noir = floor(rand() * (tailleImg(2) - 1) + 1);

        Img_bruitee(x_blanc, y_blanc) = 255;
        Img_bruitee(x_noir, y_noir) = 0;
    end


    Img_filtre = Img_bruitee;

    for x = 3:tailleImg(1) - 3
        for y = 3:tailleImg(2) - 3
            tri = sort(reshape(Img_bruitee(x-2:x+2, y-2:y+2), [], 1));
            tailleTri = size(tri);
            median = 1 + floor(tailleTri(1) / 2);
            Img_filtre(x, y) = tri(median);
        end
    end



    erreur_bruit = ErreurQuadratique(Img1, Img_bruitee);
    erreur_filtre = ErreurQuadratique(Img1, Img_filtre);

    x_plt(i) = N;
    y_bruit(i) = erreur_bruit;
    y_filtre(i) = erreur_filtre;
    i = i +1;
end

    figure();

    subplot(131);
    image(uint8(Img_bruitee));
    colormap(gray(256));
    
    subplot(132);
    image(uint8(Img_filtre));
    colormap(gray(256));
    
    subplot(133);
    hold on;
    plot(x_plt, y_bruit);
    plot(x_plt, y_filtre);
    hold off;

%% II


Img2 = double(imread('TP03I02.jpg'));

coef = 20;

%%%%%%%%%%%%%%%% ALGO MASQUE FLOU %%%%%%%%%%%%%%%%%%%%
I1 = Img2;
I2 = floutage(I1);
I3 = I1 - I2;
I4 = I3 * coef;
I5 = I1 + I4;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

tailleImg = size(I5);

for x = 1:tailleImg(1)
    for y = 1:tailleImg(2)
        
        if I5(x, y) < 0
            I5(x, y) = 0;
        end
        
        if I5(x, y) > 255
                I5(x, y) = 255;
        end
    end
end

    figure();

    image(uint8(I5));
    colormap(gray(256));


    
    
%% III



Img3 = double(imread('TP03I04.jpg'));
size(Img3)


coef = 30;

%%%%%%%%%%%%%%%% ALGO MASQUE FLOU %%%%%%%%%%%%%%%%%%%%
I1 = Img3;
I2 = floutage(I1);
I3 = I1 - I2;
I4 = I3 * coef;
I5 = I1 + I4;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

tailleImg = size(I5);

for x = 1:tailleImg(1)
    for y = 1:tailleImg(2)
        for couleur = 1:3

            if I5(x, y, couleur) < 0
                I5(x, y, couleur) = 0;
            end

            if I5(x, y, couleur) > 255
                    I5(x, y, couleur) = 255;
            end
        end
    end
end

    figure();

    image(uint8(I5));
    colormap(gray(256));



