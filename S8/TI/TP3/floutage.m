function I2 = floutage(I1)

tailleImg = size(I1);
dim = size(tailleImg);

if dim(2) == 3
    for i = 1:3
        I2(:, :, i) = conv2(I1(:, :, i), [1 1 1; 1 1 1; 1 1 1] / 9, 'same');
    end
else
    I2 = conv2(I1, [1 1 1; 1 1 1; 1 1 1] / 9, 'same');
end

end

