function Img_dy = Expansion(Img)

N = 255;

Img_dy = Img;
tailleImg = size(Img_dy);
Hmin = min(Img(:));
Hmax = max(Img(:));

for x = 1:tailleImg(1)
    for y = 1:tailleImg(2)
        Img_dy(x, y) = floor(N * (Img_dy(x, y) - Hmin) / (Hmax - Hmin));
    end
end