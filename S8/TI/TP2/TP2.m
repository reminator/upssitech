clear;
close all;

N = 255;

Img1 = double(imread('TP02I01.jpg'));
Img2 = double(imread('TP02I02.bmp'));
Img3 = double(imread('TP02I03.bmp'));


%% Question 1

hist1 = Histogram(Img1);
hist2 = Histogram(Img2);
hist3 = Histogram(Img3);

figure();

subplot(221);
image(uint8(Img1));
colormap(gray(256));
subplot(222);
image(uint8(Img2));
colormap(gray(256));
subplot(223);
image(uint8(Img3));
colormap(gray(256));

figure();

subplot(221);
bar(hist1, 'r');
subplot(222);
bar(hist2, 'r');
subplot(223);
bar(hist3, 'r');



%% Question 2

Img_dy1 = Expansion(Img1);
Img_dy2 = Expansion(Img2);
Img_dy3 = Expansion(Img3);

hist_dy1 = Histogram(Img_dy1);
hist_dy2 = Histogram(Img_dy2);
hist_dy3 = Histogram(Img_dy3);

figure();

subplot(221);
image(uint8(Img_dy1));
colormap(gray(256));
subplot(222);
image(uint8(Img_dy2));
colormap(gray(256));
subplot(223);
image(uint8(Img_dy3));
colormap(gray(256));

figure();

subplot(221);
bar(hist_dy1, 'r');
subplot(222);
bar(hist_dy2, 'r');
subplot(223);
bar(hist_dy3, 'r');


%% Question 3

histC1 = HistogramC(hist1);
histC2 = HistogramC(hist1);
histC3 = HistogramC(hist1);

Img_eg1 = Egalisation(Img1);
Img_eg2 = Egalisation(Img2);
Img_eg3 = Egalisation(Img3);

hist_eg1 = Histogram(Img_eg1);
hist_eg2 = Histogram(Img_eg2);
hist_eg3 = Histogram(Img_eg3);

figure();

subplot(221);
image(uint8(Img_eg1));
colormap(gray(256));
subplot(222);
image(uint8(Img_eg2));
colormap(gray(256));
subplot(223);
image(uint8(Img_eg3));
colormap(gray(256));

figure();

subplot(221);
bar(hist_eg1, 'r');
subplot(222);
bar(hist_eg2, 'r');
subplot(223);
bar(hist_eg3, 'r');



%% Partie II

%% Question 1
% 
% figure();
% a=3.5;
% palette=uint8(((double([0:255]')./255).^(1/a)).*255)
% image(Img2);
% transfert(palette);

figure();

hist = hist2;
Hmin = min(Img2(:));
Hmax = max(Img2(:));
palette=uint8(((double([0:255]')./255).*(N / Hmax)).*255);
image(uint8(Img2));
transfert(palette);



