function Img_eg = Egalisation(Img)

N = 255;

Img_eg = Img;

hist = Histogram(Img);
histC = HistogramC(hist);


tailleImg = size(Img_eg);
tailleHist = size(hist);

for x = 1:tailleImg(1)
    for y = 1:tailleImg(2)
        Img_eg(x, y) = floor((N * histC(Img_eg(x, y) + 1)) / histC(tailleHist(1)));
    end
end

