function hist = Histogram(Img)

tailleImg = size(Img);
hist = zeros(256, 1);
for x = 1:tailleImg(1)
    for y = 1:tailleImg(2)
        pixel = Img(x, y) + 1;
        hist(pixel) = hist(pixel) + 1;
    end
end
