function histC = HistogramC(hist)

tailleHist = size(hist);
histC = zeros(tailleHist(1), 1);

histC(1) = hist(1);
for i = 2:tailleHist(1)
    histC(i) = histC(i-1) + hist(i);
end