clear;
close all;

Img1 = imread('TP05I01.bmp');

Img1Gris = double(rgb2gray(Img1));


tailleImg = size(Img1Gris);

Gx = conv2(Img1Gris, [1 1 1 ; 0 0 0 ; -1 -1 -1], 'same');
Gy = conv2(Img1Gris, [1 0 -1 ; 1 0 -1 ; 1 0 -1], 'same');
G = abs(Gx) + abs(Gy);

figure();
image(uint8(G));
colormap(gray(256));

test = ones(3, 10);
test(2, 5) = 0;
test
mod(14, 3)
floor((14 / 3) + 1)


%%
labels = ones(tailleImg(1) + 2, tailleImg(2) + 2);
tailleLabels = size(labels);
labelSuivant = 2;

for N = 0:max(max(G))
    [xs, ys] = find(G == N);
    for indice = 1:size(xs)
        x = xs(indice) + 1;
        y = ys(indice) + 1;
        
        voisins = reshape(labels(x-1:x+1, y-1:y+1), 1, []);
        voisins = [voisins(1:4) voisins(6:9)];
        
        label = 1;
        plusieursVoisins = false;
        for i = 1:8
            voisin = voisins(i);
            if voisin > 1
                if label == 1 || label == voisin
                    label = voisin;
                else
                    plusieursVoisins = true;
                    break;
                end
            end
        end
        
        if label == 1
            labels(x, y) = labelSuivant;
            labelSuivant = labelSuivant + 1;
        else 
            if plusieursVoisins
                labels(x, y) = 0;
            else
                labels(x, y) = label;
            end
        end
    end
end

figure();
image(mod(labels,256));
colormap(colorcube(256));

nbRegions = labelSuivant - 2;

labelsFusion = labels;
test = true;
while test
    test = false;
    for y = 2:tailleLabels(2) - 1
        for x = 3:tailleLabels(1) - 2
            p1 = labelsFusion(x-1, y);
            p2 = labelsFusion(x, y);
            p3 = labelsFusion(x+1, y);

            if (p1 ~= 0 && p3 ~= 0) && (p1 ~= p3)
                [xsP1, ysP1] = find(labelsFusion == p1);
                [xsP3, ysP3] = find(labelsFusion == p3);

                xsP1 = xsP1 - 1;
                ysP1 = ysP1 - 1;

                xsP3 = xsP3 - 1;
                ysP3 = ysP3 - 1;

                moyenneP1 = 0;
                moyenneP3 = 0;
                for indiceP1 = 1:size(xsP1)
                    xP1 = xsP1(indiceP1);
                    yP1 = ysP1(indiceP1);
                    moyenneP1 = moyenneP1 + Img1Gris(xP1, yP1);
                end
                moyenneP1;
                taille = size(xsP1);
                moyenneP1 = moyenneP1 / taille(1);

                for indiceP3 = 1:size(xsP3)
                    xP3 = xsP3(indiceP3);
                    yP3 = ysP3(indiceP3);
                    moyenneP3 = moyenneP3 + Img1Gris(xP3, yP3);
                end

                taille = size(xsP3);
                moyenneP3 = moyenneP3 / taille(1);

                if abs(moyenneP1 - moyenneP3) < 20
                    test = true;
                    nbRegions = nbRegions - 1;
                    for indiceP3 = 1:size(xsP3)
                        xP3 = xsP3(indiceP3) + 1;
                        yP3 = ysP3(indiceP3) + 1;
                        labelsFusion(xP3, yP3) = p1;
                    end
                end
            end

        end
    end

    nbRegions
    figure();
    image(mod(labelsFusion,256));
    colormap(colorcube(256));

    for x = 2:tailleLabels(1) - 1
        for y = 3:tailleLabels(2) - 2
            p1 = labelsFusion(x, y-1);
            p2 = labelsFusion(x, y);
            p3 = labelsFusion(x, y+1);

            if (p1 ~= 0 && p3 ~= 0) && (p1 ~= p3)
                [xsP1, ysP1] = find(labelsFusion == p1);
                [xsP3, ysP3] = find(labelsFusion == p3);

                xsP1 = xsP1 - 1;
                ysP1 = ysP1 - 1;

                xsP3 = xsP3 - 1;
                ysP3 = ysP3 - 1;

                moyenneP1 = 0;
                moyenneP3 = 0;
                for indiceP1 = 1:size(xsP1)
                    xP1 = xsP1(indiceP1);
                    yP1 = ysP1(indiceP1);
                    moyenneP1 = moyenneP1 + Img1Gris(xP1, yP1);
                end
                moyenneP1;
                taille = size(xsP1);
                moyenneP1 = moyenneP1 / taille(1);

                for indiceP3 = 1:size(xsP3)
                    xP3 = xsP3(indiceP3);
                    yP3 = ysP3(indiceP3);
                    moyenneP3 = moyenneP3 + Img1Gris(xP3, yP3);
                end

                taille = size(xsP3);
                moyenneP3 = moyenneP3 / taille(1);

                if abs(moyenneP1 - moyenneP3) < 20
                    test = true;
                    nbRegions = nbRegions - 1;
                    for indiceP3 = 1:size(xsP3)
                        xP3 = xsP3(indiceP3) + 1;
                        yP3 = ysP3(indiceP3) + 1;
                        labelsFusion(xP3, yP3) = p1;
                    end
                end
            end

        end
    end

    nbRegions
    figure();
    image(mod(labelsFusion,256));
    colormap(colorcube(256));
end