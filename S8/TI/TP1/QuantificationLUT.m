function Img_Q = QuantificationLUT(Img_, LUT)

% Calcul des tailles
tailleImg = size(Img_);
tailleLUT = size(LUT);

dim = size(tailleImg);

if dim(2) == 2
    Img(:, :, 1) = Img_(:, :);
    Img(:, :, 2) = Img_(:, :);
    Img(:, :, 3) = Img_(:, :);
else
    Img = Img_;
end


% Cr�ation de l'image quantifi�e
Img_Q = zeros(tailleImg(1), tailleImg(2), 3);


% Parcours sur tous les pixels de l'image
for x = 1:tailleImg(1)
    for y = 1:tailleImg(2)

        % Initialisation de la diff�rence minimale avec la premi�re valeur
        % du LUT
        differenceMin = 0;
        refMin = 1;


        for couleur = 1:3
            lut_val = LUT(refMin, couleur);
            differenceMin = differenceMin + abs(Img(x, y, couleur) - lut_val);
        end

        % Recherche de la r�elle valeur de LUT la plus proche
        for ref = 2:tailleLUT(1)

            % Calcul valeur suivante
            d = 0;
            for couleur = 1:3
                lut_val = LUT(ref, couleur);
                d = d + abs(Img(x, y, couleur) - lut_val);
            end

            % MAJ diff�rence
            if differenceMin > d
                differenceMin = d;
                refMin = ref;
            end
        end

        % Affectation de la valeur minimale trouv� dans l'image quantifi�e
        Img_Q(x, y, :) = LUT(refMin, :);
    end
end