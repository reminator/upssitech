clear;
close all;

LUT = LutSubSamp(1)
Img1 = double(imread('TP01I01.bmp'));
Img2 = double(imread('TP01I02.jpg'));
Img3 = double(imread('TP01I03.jpg'));

figure();

subplot(221);
image(uint8(Img1));
subplot(222);
image(uint8(Img2));
colormap(gray(256));
subplot(223);
image(uint8(Img3));

%% Question 2

tic
Img_Q1 = QuantificationLUT(Img1, LUT);
Img_Q2 = QuantificationLUT(Img2, LUT);
Img_Q3 = QuantificationLUT(Img3, LUT);

figure();

subplot(221);
image(uint8(Img_Q1));
subplot(222);
image(uint8(Img_Q2));
colormap(gray(256));
subplot(223);
image(uint8(Img_Q3));


%% Question 3

erreur1 = ErreurQuadratique(Img1, Img_Q1)
erreur2 = ErreurQuadratique(Img2, Img_Q2)
erreur3 = ErreurQuadratique(Img3, Img_Q3)

toc





%% Exercice 2
%% Question 1
tic

Img_FS1 = FS(Img1, LUT);
Img_FS2 = FS(Img2, LUT);
Img_FS3 = FS(Img3, LUT);

figure();

subplot(221);
image(uint8(Img_FS1));
subplot(222);
image(uint8(Img_FS2));
colormap(gray(256));
subplot(223);
image(uint8(Img_FS3));



%% Question 2

erreur1 = ErreurQuadratique(Img1, Img_FS1)
erreur2 = ErreurQuadratique(Img2, Img_FS2)
erreur3 = ErreurQuadratique(Img3, Img_FS3)



toc
