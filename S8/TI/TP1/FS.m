function Img_FS = FS(Img_, LUT)

% Calcul des tailles
tailleImg = size(Img_);
tailleLUT = size(LUT);

dim = size(tailleImg);

if dim(2) == 2
    Img_FS(:, :, 1) = Img_(:, :);
    Img_FS(:, :, 2) = Img_(:, :);
    Img_FS(:, :, 3) = Img_(:, :);
else
    Img_FS = Img_;
end


% Parcours sur tous les pixels de l'image (Les bords sont ignor�s)
for x = 1:tailleImg(1)-1
    for y = 2:tailleImg(2)-1

        % Initialisation de la diff�rence minimale avec la premi�re valeur
        % du LUT
        differenceMin = 0;
        refMin = 1;


        for couleur = 1:3
            lut_val = LUT(refMin, couleur);
            differenceMin = differenceMin + abs(Img_FS(x, y, couleur) - lut_val);
        end
        
        
        % Recherche de la r�elle valeur de LUT la plus proche
        for ref = 2:tailleLUT(1)

            % Calcul valeur suivante
            d = 0;
            for couleur = 1:3
                lut_val = LUT(ref, couleur);
                d = d + abs(Img_FS(x, y, couleur) - lut_val);
            end

            % MAJ diff�rence
            if differenceMin > d
                differenceMin = d;
                refMin = ref;
            end
        end
        
        for couleur = 1:3
            c = LUT(refMin, couleur);
            erreur = Img_FS(x, y, couleur) - c;
           
            Img_FS(x, y, couleur) = c;
            Img_FS(x, y+1, couleur) = Img_FS(x, y+1, couleur) + erreur * 7/16;
            Img_FS(x+1, y-1, couleur) = Img_FS(x+1, y-1, couleur) + erreur * 3/16;
            Img_FS(x+1, y, couleur) = Img_FS(x+1, y, couleur) + erreur * 5/16;
            Img_FS(x+1, y+1, couleur) = Img_FS(x+1, y+1, couleur) + erreur * 1/16;
        end        
    end
end

