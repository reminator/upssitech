function erreur = ErreurQuadratique(Img1_, Img2_)

% Calcul des tailles
tailleImg1 = size(Img1_);
tailleImg2 = size(Img2_);

dim1 = size(tailleImg1);
dim2 = size(tailleImg2);

if dim1(2) == 2
    Img1(:, :, 1) = Img1_(:, :);
    Img1(:, :, 2) = Img1_(:, :);
    Img1(:, :, 3) = Img1_(:, :);
else
    Img1 = Img1_;
end

if dim2(2) == 2
    Img2(:, :, 1) = Img2_(:, :);
    Img2(:, :, 2) = Img2_(:, :);
    Img2(:, :, 3) = Img2_(:, :);
else
    Img2 = Img2_;
end
tailleImg1 = size(Img1);
tailleImg2 = size(Img2);

erreur = -1;

if tailleImg1 == tailleImg2
    erreur = 0;
    N = tailleImg1(1) * tailleImg1(2) * 3;

    for x = 1:tailleImg1(1)
        for y = 1:tailleImg1(2)
            for couleur = 1:3
                erreur = erreur + (Img2(x, y, couleur) - Img1(x, y, couleur))^2;
            end
        end
    end

    erreur = erreur / N;
end

