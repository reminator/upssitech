clear;
close all;

Img1 = double(imread('BE07I01.jpg'));

Img1Binarise = binarisation(Img1, 120);

figure();
imagesc(Img1Binarise);
colormap(gray(256));

ImgOuverture = dilatation(dilatation(erosion(erosion(Img1Binarise))));


figure();
imagesc(ImgOuverture);
colormap(gray(256));


I = ImgOuverture;
NCC1 = bweuler(I);
t = 0;
N = [];
while NCC1 > 0
    Filtre = ones(t);
    E = imerode(I, Filtre);
    I = imreconstruct(E, I);
    NCC2 = bweuler(I);
    t = t + 1;
    N = [N NCC1-NCC2];
    NCC1 = NCC2;
end

figure;
bar(N, 'r');


%%
indicesMax = [];
indicesMin = [0];
taille = size(N)
for i = 2:taille(2)-1
    if N(i-1) < N(i) && N(i+1) < N(i)
        indicesMax = [indicesMax ; i];
    end
    if N(i-1) > N(i) && N(i+1) > N(i)
        indicesMin = [indicesMin ; i];
    end
end
indicesMax
indicesMin = [indicesMin ; i+1];
indicesMin

ImgPrec = Img1Binarise;
ImgSuiv = Img1Binarise;

for i = 2:size(indicesMin)
    Filtre = ones(indicesMin(i));
    E = imerode(Img1Binarise, Filtre);
    ImgSuiv = imreconstruct(E, Img1Binarise);
    figure();
    imagesc(imreconstruct((ImgPrec - ImgSuiv)*255, Img1));
    colormap(gray(256));
    ImgPrec = ImgSuiv;
end


