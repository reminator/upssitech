function Img2 = binarisation(Img, seuil)

    taille = size(Img);
    Img2 = zeros(taille(1), taille(2));
    for x = 1:taille(1)
        for y = 1:taille(2)
            Img2(x, y) = Img(x, y) > seuil;
        end
    end
end

