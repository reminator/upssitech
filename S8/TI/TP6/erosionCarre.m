function Img2 = erosionCarre(Img, tailleStruct, centreX, centreY)

    Img2 = Img;
    tailleImg = size(Img);
    
    
    for x = centreX:tailleImg(1) - tailleStruct - centreX
        for y = centreY:tailleImg(2) - tailleStruct - centreY
            resultat = true;
            
            voisins = reshape(Img((x-(centreX-1)):(x+tailleStruct-centreX), (y-(centreY-1)):(y+tailleStruct-centreY)), 1, []);
            
            for voisin = voisins
                resultat = resultat && voisin;
            end
            Img2(x, y) = resultat;
        end
    end
end

